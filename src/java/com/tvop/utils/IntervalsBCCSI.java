package com.tvop.utils;

public enum IntervalsBCCSI {

    DAY("D"),
    WEEK("W"),
    MONTH("M");

    private final String value;

    IntervalsBCCSI(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
