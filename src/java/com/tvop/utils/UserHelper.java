/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tvop.utils;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import static com.tvop.utils.AuthorizationListener.LOGGER;

/**
 *
 * @author mauro
 */
public class UserHelper {

    public static void main(String args[]) {

        System.out.println(encrypt("Giuliani" + "RGiuliani01"));
        System.out.println(encrypt("Pratico" + "GPratico01"));
        System.out.println(encrypt("Radaelli" + "ARadaelli01"));
    }

    private static String encrypt(String input) {

        String md5 = null;

        if (null == input) {
            return "";
        }
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.update(input.getBytes(), 0, input.length());
            md5 = new BigInteger(1, digest.digest()).toString(16);
        } catch (NoSuchAlgorithmException e) {
            LOGGER.error(e.getMessage());
        }
        return md5;
    }

}
