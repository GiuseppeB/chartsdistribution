package com.tvop.utils;

import org.apache.log4j.Logger;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;

public class AuthorizationListener implements PhaseListener {
    final static Logger LOGGER = Logger.getLogger(AuthorizationListener.class);
    /**
     *
     * @param event
     */
    @Override
    public void afterPhase(PhaseEvent event) {
        FacesContext context = event.getFacesContext();

        if (!userExists(context)) {
            if (requestingSecureView(context)) {
                context.responseComplete();
                 //context.getApplication().getNavigationHandler().handleNavigation(context, null, "login");
                try {
                    ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
                    externalContext.redirect("login.xhtml"); //faces/???
                } catch (Exception ioe) {
                    LOGGER.error(ioe.getMessage());
                }
            }
        }
    }

    public void beforePhase(PhaseEvent event) {
//Do nothing
    }

    public PhaseId getPhaseId() {
        return PhaseId.RESTORE_VIEW;
    }

    private boolean userExists(FacesContext context) {
        ExternalContext extContext = context.getExternalContext();
        return (extContext.getSessionMap().containsKey("user"));
    }

    private boolean requestingSecureView(FacesContext context) {
        ExternalContext extContext = context.getExternalContext();
        String path = extContext.getRequestPathInfo();
        return (!"/login.xhtml".equals(path));
    }

}
