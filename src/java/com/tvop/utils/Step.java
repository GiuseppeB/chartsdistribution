package com.tvop.utils;

public enum Step {
    GIORNO("Giorno"),
    MESE("Mese");

    private final String value;

    Step(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
