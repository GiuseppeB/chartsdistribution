/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tvop.utils;

import java.io.Serializable;

/**
 *
 * @author o            
 */
public class MenuItem implements Serializable{

        private String tooltip;
        private String image;
        private String outcome;
        private String title;
        private boolean rendered;

        /**
         * @return the tooltip
         */
        public String getTooltip() {
            return tooltip;
        }

        /**
         * @param tooltip the tooltip to set
         */
        public void setTooltip(String tooltip) {
            this.tooltip = tooltip;
        }

        /**
         * @return the image
         */
        public String getImage() {
            return image;
        }

        /**
         * @param image the image to set
         */
        public void setImage(String image) {
            this.image = image;
        }

        /**
         * @return the outcome
         */
        public String getOutcome() {
            return outcome;
        }

        /**
         * @param outcome the outcome to set
         */
        public void setOutcome(String outcome) {
            this.outcome = outcome;
        }

        /**
         * @return the title
         */
        public String getTitle() {
            return title;
        }

        /**
         * @param title the title to set
         */
        public void setTitle(String title) {
            this.title = title;
        }

        /**
         * @return the rendered
         */
        public boolean isRendered() {
            return rendered;
        }

        /**
         * @param rendered the rendered to set
         */
        public void setRendered(boolean rendered) {
            this.rendered = rendered;
        }

    
}