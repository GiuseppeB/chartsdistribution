/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tvop.utils;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ResourceBundle;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.tomcat.jdbc.pool.DataSource;
import org.apache.tomcat.jdbc.pool.PoolProperties;

public class COSMODatasourceConnection {

    private static DataSource datasource = null;
    private static final Logger logger = LogManager.getLogger(DatasourceConnection.class.getName());
    private static final String fileCosmo="com.tvop.cosmoResource";
    private static String separatoreTavolaId="-";       
    private COSMODatasourceConnection() {
    }

    public static Connection getConnection() {

        try {
            if (datasource == null) {
                COSMODatasourceConnection.initDatasource();
            }
            Connection conn = null;
            conn = datasource.getConnection();
            return conn;
        } catch (SQLException e) {
            logger.error("Errore in connessione COSMO! " + e.getMessage());
            return null;
        }
    }

    public static void initDatasource() {

        //spostare su properties esterne
        ResourceBundle risorsa;
        risorsa = ResourceBundle.getBundle(fileCosmo);
        
        String url=risorsa.getString("urlCosmo");
        String driverName=risorsa.getString("driverName");
        String username=risorsa.getString("username");
        String password=risorsa.getString("password");
                
        PoolProperties p = new PoolProperties();

        p.setUrl(url);
        p.setDriverClassName(driverName);
        p.setUsername(username);
        p.setPassword(password);

        p.setJmxEnabled(true);
        p.setTestWhileIdle(false);
        p.setTestOnBorrow(true);
        p.setValidationQuery("select 1");//TODO
        p.setTestOnReturn(false);
        p.setValidationInterval(30000);
        p.setTimeBetweenEvictionRunsMillis(30000);

        p.setMaxActive(10);
        p.setMaxIdle(10);
        p.setInitialSize(4);
        p.setMaxWait(10000);
        p.setRemoveAbandonedTimeout(60);
        p.setMinEvictableIdleTimeMillis(30000);
        p.setMinIdle(4);

        p.setLogAbandoned(true);
        p.setRemoveAbandoned(true);

        p.setJdbcInterceptors("org.apache.tomcat.jdbc.pool.interceptor.ConnectionState;"
                + "org.apache.tomcat.jdbc.pool.interceptor.StatementFinalizer");
        datasource = new DataSource();
        datasource.setPoolProperties(p);
        logger.info("Db connection COSMO estabilished");

    }

    public static void closeDatasource() {
        datasource.close();
        logger.info("Db connection COSMO closed");
    }
    public static String getTavola() {
//per poter gestire nome tavola di test, di esercizio, su altro db
        ResourceBundle risorsa;
        risorsa = ResourceBundle.getBundle(fileCosmo);
        String appoggio=risorsa.getString("TipoTable");
        separatoreTavolaId="-"; //usato per test su postgres
        if (appoggio==null || appoggio.equals("ESE"))
            return "ListEmergency";
        if (appoggio.equals("POST"))
        {   separatoreTavolaId="";
            return "listaprocemergenza";}
        return "ListEmergencyTest" ;
    }

    public static String getSeparatoreTavolaId() {
        return separatoreTavolaId;
    }

        
}