package com.tvop.utils;

public enum DatasPrivilegeEnum {
    FILTERED("FILTERED"),
    ALL("ALL");

    private final String value;

    DatasPrivilegeEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
