/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tvop.utils;

import com.tvop.persistence.dbentities.ThresholdEvaluationKpi;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.apache.logging.log4j.LogManager;
import org.joda.time.DateTime;

/**
 *
 * @author giuseppe
 */
public class Utils {

    static FacesContext context = FacesContext.getCurrentInstance();
    static ResourceBundle bundle = context.getApplication().getResourceBundle(context, "txt");
    static final transient org.apache.logging.log4j.Logger LOGGER = LogManager.getLogger(Utils.class.getName());

    public static String convertValue(Object o, int type) {
        String res = null;

        try {
            double db = (double) o;
            switch (type) {
                case 1:
                    res = String.valueOf((int) db);
                    break;
                case 2:
                case 3:
                    //funzione che controlla la cifra decimale
                    // da fare
                    res = String.format("%.1f", db);
                    break;
                case 4:
                    res = tFormat((int) db);
                    break;
                case 5:
                    res = String.format("%.1f", db) + "%";
                    break;
                default:
                    res = String.valueOf(db);
            }
        } catch (Exception e) {
            res = o.toString();
        }
        return res;
    }

    public static String evaluationKpi(String eval) {
        String result = "";
        switch (eval) {
            case "goodUp":
                result = "/resources/icons/ArrowUpGreen.png";
                break;
            case "badUp":
                result = "/resources/icons/ArrowUpRed.png";
                break;
            case "badDown":
                result = "/resources/icons/ArrowDownRed.png";
                break;
            case "goodDown":
                result = "/resources/icons/ArrowDownGreen.png";
                break;
            case "notVaried":
                result = "/resources/icons/NotVaried.png";
                break;
            case "noArrow":
                result = "/resources/icons/infoImage.png";
                break;
            case "simbolOk":
                result = "/resources/icons/SimbolOK.png";
                break;
            case "simbolDanger":
                result = "/resources/icons/SimbolDanger.png";
                break;
        }
        return result;
    }

    public static String evaluationKpiWithValues(ThresholdEvaluationKpi eval, double newValue, double oldValue) {
        String result = "/resources/icons/infoImage.png";
        if (eval.isEnable()) {
            if (eval.isGreatervalue()) {
                if (newValue > oldValue) {
                    result = "/resources/icons/ArrowUpGreen.png";
                } else {
                    result = "/resources/icons/ArrowDownRed.png";
                }
            } else {
                if (newValue > oldValue) {
                    result = "/resources/icons/ArrowUpRed.png";
                } else {
                    result = "/resources/icons/ArrowDownGreen.png";
                }
            }
        }
        if (eval.isEnabledalert()) {
            if (eval.getAlertformula().equals("<")) {
                if (newValue < eval.getAlertvalue()) {
                    result = "/resources/icons/SimbolDanger.png";
                }
            } else {
                if (newValue > eval.getAlertvalue()) {
                    result = "/resources/icons/SimbolDanger.png";
                }
            }
        }
        if (eval.isEnabledgood()) {
            if (eval.getGoodformula().equals("<")) {
                if (newValue < eval.getGoodvalue()) {
                    result = "/resources/icons/SimbolOK.png";
                }
            } else {
                if (newValue > eval.getGoodvalue()) {
                    result = "/resources/icons/SimbolOK.png";
                }
            }
        }
        return result;
    }

    public static String evaluationKpiWithValues(ThresholdEvaluationKpi eval, String newValue, String oldValue) {
        String result = "/resources/icons/infoImage.png";
        if (eval.isEnable()) {
            if (eval.isGreatervalue()) {
                if (stringCompare(newValue, oldValue) > 0) {
                    result = "/resources/icons/ArrowUpGreen.png";
                } else {
                    result = "/resources/icons/ArrowDownRed.png";
                }
            } else {
                if (stringCompare(newValue, oldValue) > 0) {
                    result = "/resources/icons/ArrowUpRed.png";
                } else {
                    result = "/resources/icons/ArrowDownGreen.png";
                }
            }
        }
        if (eval.isEnabledalert()) {
            if (eval.getAlertformula().equals("<")) {
                if (stringCompare(newValue, String.valueOf(eval.getAlertvalue())) < 0) {
                    result = "/resources/icons/SimbolDanger.png";
                }
            } else {
                if (stringCompare(newValue, String.valueOf(eval.getAlertvalue())) > 0) {
                    result = "/resources/icons/SimbolDanger.png";
                }
            }
        }
        if (eval.isEnabledgood()) {
            if (eval.getGoodformula().equals("<")) {
                if (stringCompare(newValue, String.valueOf(eval.getGoodvalue())) < 0) {
                    result = "/resources/icons/SimbolOK.png";
                }
            } else {
                if (stringCompare(newValue, String.valueOf(eval.getGoodvalue())) > 0) {
                    result = "/resources/icons/SimbolOK.png";
                }
            }
        }
        return result;
    }
    
    public static int stringCompare(String str1, String str2) {

        int l1 = str1.length();
        int l2 = str2.length();
        int lmin = Math.min(l1, l2);

        for (int i = 0; i < lmin; i++) {
            int str1_ch = (int) str1.charAt(i);
            int str2_ch = (int) str2.charAt(i);

            if (str1_ch != str2_ch) {
                return str1_ch - str2_ch;
            }
        }

        // Edge case for strings like 
        // String 1="Geeks" and String 2="Geeksforgeeks" 
        if (l1 != l2) {
            return l1 - l2;
        } // If none of the above conditions is true, 
        // it implies both the strings are equal 
        else {
            return 0;
        }
    }
    
    public static String getPercValue(double value) {
        String result;
//        float val = value.floatValue() * 100;
        if(value%1 == 0) {
            result = String.valueOf((int) value);  
        } else {
            result = String.format("%.2f", value);
        }
        return result + "%";
    }

    public static String tFormat(double t) {

        //fino a 24 ore formatto hh:mi:ss, oltre gg hh
        String formatted = null;

        int hours = (int) t / 3600;
        int remainder = (int) t - hours * 3600;
        int mins = remainder / 60;
        remainder = remainder - mins * 60;
        int secs = remainder;
        String sHours = String.valueOf(hours);
        String sMins = String.format("%02d", mins);
        String sSecs = String.format("%02d", secs);
        formatted = sHours + ":" + sMins + ":" + sSecs;

        return formatted;
    }

    public static boolean checkPassword(String newPassword, String confirmPassword) {
        if (!newPassword.equals(confirmPassword)) {
            messageError(bundle.getString("message.pwConfirmKO"));
            return false;
        }
        if (!pwMatches(newPassword)) {
            messageError(bundle.getString("message.invalidFormat"));
            return false;
        }
        return true;
    }

    public static boolean pwMatches(String pw) {
        FacesContext ctx = FacesContext.getCurrentInstance();
        String pattern = ctx.getExternalContext().getInitParameter("pwPattern");
        return pw.matches(pattern);
    }

    public static boolean emailMatches(String email) {
        FacesContext ctx = FacesContext.getCurrentInstance();
        String pattern = ctx.getExternalContext().getInitParameter("emailPattern");
        if (!email.matches(pattern)) {
            Utils.messageError(bundle.getString("message.invalidEmail"));
            return false;
        }
        return true;
    }

//    public static boolean checkInput(User user, boolean requiredTelephone, String newPassword, String confirmPassword) {
//        if ((requiredTelephone && user.getTelnumber().isEmpty()) || user.getEmail().isEmpty() || user.getUserid().isEmpty()
//                || user.getFirstname().isEmpty() || user.getLastname().isEmpty() || newPassword.isEmpty() || confirmPassword.isEmpty()) {
//            messageError(bundle.getString("message.required"));
//            return false;
//        }
//        return true;
//    }
//
//    public static boolean checkInput(User user, boolean requiredTelephone) {
//        if ((requiredTelephone && user.getTelnumber().isEmpty()) || user.getEmail().isEmpty() || user.getUserid().isEmpty()
//                || user.getFirstname().isEmpty() || user.getLastname().isEmpty()) {
//            messageError(bundle.getString("message.required"));
//            return false;
//        }
//        return true;
//    }
    
    public static void messageInfo(String message) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, message, null));
    }

    public static void messageError(String message) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, message, null));
    }
    
    public static String getLastTimerefOfDay() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date(IntervalBoundaries.getStartOfHalfH(new DateTime().minusMinutes(30)).getMillis()));
    }
    
    public static String checkDecimal(double value) {
        DecimalFormat df = new DecimalFormat("#.##");
        String result = df.format(value);
        return result;
    }
    
    public static float divide(float a, int b) {
        if (b == 0) {
            return 0;
        }
        return a / b;
    }
    
    public static int divide(int a, int b) {
        if (b == 0) {
            return 0;
        }
        float tmp = (float) a / b;
        return Math.round(tmp);
    }
}
