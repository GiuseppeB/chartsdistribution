package com.tvop.persistence.dbentities;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ch_cottruckcolumn")
public class CotTruckColumn implements Serializable {
    private String colheader;
    private String kpiid;
    private int ordering;
    private String label;
    private int type;
    private boolean mobile;
    
    public CotTruckColumn() {
        
    }
    
    public CotTruckColumn(CotTruckColumn agCol){
        this.colheader = agCol.colheader;
        this.kpiid = agCol.kpiid;
        this.ordering = agCol.ordering;
        this.label = agCol.label;
        this.type = agCol.type;
        this.mobile = agCol.mobile;
    }

    public String getColheader() {
        return colheader;
    }

    public void setColheader(String colheader) {
        this.colheader = colheader;
    }

    @Id
    public String getKpiid() {
        return kpiid;
    }

    public void setKpiid(String kpiid) {
        this.kpiid = kpiid;
    }

    public int getOrdering() {
        return ordering;
    }

    public void setOrdering(int ordering) {
        this.ordering = ordering;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public boolean isMobile() {
        return mobile;
    }

    public void setMobile(boolean mobile) {
        this.mobile = mobile;
    }
}
