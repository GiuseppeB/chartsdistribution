package com.tvop.persistence.dbentities;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ch_cotservizirow")
public class CotServiziRow implements Serializable {
    private String macroflussoid;
    private int ordering;
    private String label;
    
    public CotServiziRow() {
        
    }
    
    public CotServiziRow(CotServiziRow rtKpiAG){
        this.macroflussoid = rtKpiAG.macroflussoid;
        this.ordering = rtKpiAG.ordering;
        this.label = rtKpiAG.label;
    }
    
    @Id
    public String getMacroflussoid() {
        return macroflussoid;
    }

    public void setMacroflussoid(String macroflussoid) {
        this.macroflussoid = macroflussoid;
    }

    public int getOrdering() {
        return ordering;
    }

    public void setOrdering(int ordering) {
        this.ordering = ordering;
    } 

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
    
    
}
