package com.tvop.persistence.dbentities;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ag_agentgroup")
public class AgentGroup implements Serializable {
    private String agentgroupid;
    private String label;
    private boolean virtual;
    private boolean presplit;
    
    public AgentGroup() {
        
    }
    
    public AgentGroup(AgentGroup rtKpiAG){
        this.agentgroupid = rtKpiAG.agentgroupid;
        this.label = rtKpiAG.label;
        this.virtual = rtKpiAG.virtual;
        this.presplit = rtKpiAG.presplit;
    }
    
    @Id
    public String getAgentgroupid() {
        return agentgroupid;
    }

    public void setAgentgroupid(String agentgroupid) {
        this.agentgroupid = agentgroupid;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public boolean isVirtual() {
        return virtual;
    }

    public void setVirtual(boolean virtual) {
        this.virtual = virtual;
    }

    public boolean isPresplit() {
        return presplit;
    }

    public void setPresplit(boolean presplit) {
        this.presplit = presplit;
    }
}
