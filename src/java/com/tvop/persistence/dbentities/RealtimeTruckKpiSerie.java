package com.tvop.persistence.dbentities;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "k_realtimetruckkpiserie")
public class RealtimeTruckKpiSerie implements Serializable {
    private int serieid;
    private int kpiid;
    private Timestamp timeref;
    private String macroflussoid = "";
    private double value;
    private String arrowevaluation;
    
    public RealtimeTruckKpiSerie() {
        
    }
    
    public RealtimeTruckKpiSerie(RealtimeTruckKpiSerie rtKpiAG){
        this.serieid = rtKpiAG.serieid;
        this.kpiid = rtKpiAG.kpiid;
        this.timeref = rtKpiAG.timeref;
        this.macroflussoid = rtKpiAG.macroflussoid;
        this.value = rtKpiAG.value;
        this.arrowevaluation = rtKpiAG.arrowevaluation;
    }
    
    @Id
    public int getSerieid() {
        return serieid;
    }

    public void setSerieid(int serieid) {
        this.serieid = serieid;
    }

    public int getKpiid() {
        return kpiid;
    }

    public void setKpiid(int kpiid) {
        this.kpiid = kpiid;
    }

    public Timestamp getTimeref() {
        return timeref;
    }

    public void setTimeref(Timestamp timeref) {
        this.timeref = timeref;
    }

    public String getMacroflussoid() {
        return macroflussoid;
    }

    public void setMacroflussoid(String macroflussoid) {
        this.macroflussoid = macroflussoid;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public String getArrowevaluation() {
        return arrowevaluation;
    }

    public void setArrowevaluation(String arrowevaluation) {
        this.arrowevaluation = arrowevaluation;
    }
    
    
}
