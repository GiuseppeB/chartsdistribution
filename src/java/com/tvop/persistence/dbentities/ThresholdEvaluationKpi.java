package com.tvop.persistence.dbentities;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "k_thresholdevaluationkpi")
public class ThresholdEvaluationKpi implements Serializable {
    private String kpiid;
    private String label;
    private boolean greatervalue;
    private String goodformula;
    private int goodvalue;
    private String badformula;
    private int badvalue;
    private String alertformula;
    private int alertvalue;
    private boolean enable;
    private String type;
    private boolean enabledbad;
    private boolean enabledgood;
    private boolean enabledalert;
    
    public ThresholdEvaluationKpi() {
        
    }
    
    public ThresholdEvaluationKpi(ThresholdEvaluationKpi eval){
        this.kpiid = eval.kpiid;
        this.label = eval.label;
        this.greatervalue = eval.greatervalue;
        this.goodformula = eval.goodformula;
        this.goodvalue = eval.goodvalue;
        this.badformula = eval.badformula;
        this.badvalue = eval.badvalue;
        this.alertformula = eval.alertformula;
        this.alertvalue = eval.alertvalue;
        this.enable = eval.enable;
        this.type = eval.type;
        this.enabledbad = eval.enabledbad;
        this.enabledgood = eval.enabledgood;
        this.enabledalert = eval.enabledalert;
    }

    @Id
    public String getKpiid() {
        return kpiid;
    }

    public void setKpiid(String kpiid) {
        this.kpiid = kpiid;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public boolean isGreatervalue() {
        return greatervalue;
    }

    public void setGreatervalue(boolean greatervalue) {
        this.greatervalue = greatervalue;
    }

    public String getGoodformula() {
        return goodformula;
    }

    public void setGoodformula(String goodformula) {
        this.goodformula = goodformula;
    }

    public int getGoodvalue() {
        return goodvalue;
    }

    public void setGoodvalue(int goodvalue) {
        this.goodvalue = goodvalue;
    }

    public String getBadformula() {
        return badformula;
    }

    public void setBadformula(String badformula) {
        this.badformula = badformula;
    }

    public int getBadvalue() {
        return badvalue;
    }

    public void setBadvalue(int badvalue) {
        this.badvalue = badvalue;
    }

    public String getAlertformula() {
        return alertformula;
    }

    public void setAlertformula(String alertformula) {
        this.alertformula = alertformula;
    }

    public int getAlertvalue() {
        return alertvalue;
    }

    public void setAlertvalue(int alertvalue) {
        this.alertvalue = alertvalue;
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isEnabledbad() {
        return enabledbad;
    }

    public void setEnabledbad(boolean enabledbad) {
        this.enabledbad = enabledbad;
    }

    public boolean isEnabledgood() {
        return enabledgood;
    }

    public void setEnabledgood(boolean enabledgood) {
        this.enabledgood = enabledgood;
    }

    public boolean isEnabledalert() {
        return enabledalert;
    }

    public void setEnabledalert(boolean enabledalert) {
        this.enabledalert = enabledalert;
    }
}
