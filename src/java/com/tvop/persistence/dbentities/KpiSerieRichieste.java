package com.tvop.persistence.dbentities;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "k_kpiserierichieste")
public class KpiSerieRichieste implements Serializable {
    private int serieid;
    private String codrich;
    private String step;
    private Timestamp timeref;
    private int value;
    
    @Id
    @Column(name = "serieid")
    public int getSerieid() {
        return serieid;
    }
    public void setSerieid(int serieid) {
        this.serieid = serieid;
    }

    public String getCodrich() {
        return codrich;
    }

    public void setCodrich(String codrich) {
        this.codrich = codrich;
    }

    public String getStep() {
        return step;
    }

    public void setStep(String step) {
        this.step = step;
    }

    public Timestamp getTimeref() {
        return timeref;
    }

    public void setTimeref(Timestamp timeref) {
        this.timeref = timeref;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
