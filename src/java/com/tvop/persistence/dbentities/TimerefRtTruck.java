package com.tvop.persistence.dbentities;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "t_timeref_rttruck")
public class TimerefRtTruck implements Serializable {
    private Timestamp timeref;
    
    public TimerefRtTruck() {
        
    }
    
    public TimerefRtTruck(TimerefRtTruck t){
        this.timeref = t.timeref;
    }
    
    @Id
    public Timestamp getTimeref() {
        return timeref;
    }

    public void setTimeref(Timestamp timeref) {
        this.timeref = timeref;
    }
}
