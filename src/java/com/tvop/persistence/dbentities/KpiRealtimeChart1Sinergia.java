package com.tvop.persistence.dbentities;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "k_kpirealtimechart1")
public class KpiRealtimeChart1Sinergia implements Serializable {
    @Id
    private String id;
    private String nome;
    private String stato;
    private String sottostato;
    private String durata;
    private String totlavorato;
    private int totvoce;
    private String tempomedioconv;
    private int callbackricevute;
    private int callbackgestite;
    private int tkaperti;
    private int tkgestiti;
    private Timestamp timeref;
    
    public KpiRealtimeChart1Sinergia() {
        
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getStato() {
        return stato;
    }

    public void setStato(String stato) {
        this.stato = stato;
    }

    public String getSottostato() {
        return sottostato;
    }

    public void setSottostato(String sottostato) {
        this.sottostato = sottostato;
    }

    public String getDurata() {
        return durata;
    }

    public void setDurata(String durata) {
        this.durata = durata;
    }

    public String getTotlavorato() {
        return totlavorato;
    }

    public void setTotlavorato(String totlavorato) {
        this.totlavorato = totlavorato;
    }

    public int getTotvoce() {
        return totvoce;
    }

    public void setTotvoce(int totvoce) {
        this.totvoce = totvoce;
    }

    public String getTempomedioconv() {
        return tempomedioconv;
    }

    public void setTempomedioconv(String tempomedioconv) {
        this.tempomedioconv = tempomedioconv;
    }

    public int getCallbackricevute() {
        return callbackricevute;
    }

    public void setCallbackricevute(int callbackricevute) {
        this.callbackricevute = callbackricevute;
    }

    public int getCallbackgestite() {
        return callbackgestite;
    }

    public void setCallbackgestite(int callbackgestite) {
        this.callbackgestite = callbackgestite;
    }

    public int getTkaperti() {
        return tkaperti;
    }

    public void setTkaperti(int tkaperti) {
        this.tkaperti = tkaperti;
    }

    public int getTkgestiti() {
        return tkgestiti;
    }

    public void setTkgestiti(int tkgestiti) {
        this.tkgestiti = tkgestiti;
    }

    public Timestamp getTimeref() {
        return timeref;
    }

    public void setTimeref(Timestamp timeref) {
        this.timeref = timeref;
    }
}
