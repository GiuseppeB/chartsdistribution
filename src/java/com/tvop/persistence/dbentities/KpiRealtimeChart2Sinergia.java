package com.tvop.persistence.dbentities;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "k_kpirealtimechart2")
public class KpiRealtimeChart2Sinergia implements Serializable {
    @Id
    private String id;
    private String nome;
    private int totricevute;
    private int totingestione;
    private int totgestite;
    private int totinattesa;
    private int totabb;
    private int overflow;
    private float percabb;
    private float perclds;
    private int callbackingestione;
    private String tmpmedioattesa = "0";
    private String tmpmediorisposta = "0";
    private String tmpmedioabb = "0";
    private String tmpmaxattesa = "0";
    private int tkaperti;
    private int tkgestiti;
    private Timestamp timeref;
    
    public KpiRealtimeChart2Sinergia() {
        
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getTotricevute() {
        return totricevute;
    }

    public void setTotricevute(int totricevute) {
        this.totricevute = totricevute;
    }

    public int getTotingestione() {
        return totingestione;
    }

    public void setTotingestione(int totingestione) {
        this.totingestione = totingestione;
    }

    public int getTotgestite() {
        return totgestite;
    }

    public void setTotgestite(int totgestite) {
        this.totgestite = totgestite;
    }

    public int getTotinattesa() {
        return totinattesa;
    }

    public void setTotinattesa(int totinattesa) {
        this.totinattesa = totinattesa;
    }

    public int getTotabb() {
        return totabb;
    }

    public void setTotabb(int totabb) {
        this.totabb = totabb;
    }

    public float getPercabb() {
        return percabb;
    }

    public void setPercabb(float percabb) {
        this.percabb = percabb;
    }

    public float getPerclds() {
        return perclds;
    }

    public void setPerclds(float perclds) {
        this.perclds = perclds;
    }

    public int getCallbackingestione() {
        return callbackingestione;
    }

    public void setCallbackingestione(int callbackingestione) {
        this.callbackingestione = callbackingestione;
    }

    public String getTmpmedioattesa() {
        return tmpmedioattesa;
    }

    public void setTmpmedioattesa(String tmpmedioattesa) {
        this.tmpmedioattesa = tmpmedioattesa;
    }

    public String getTmpmediorisposta() {
        return tmpmediorisposta;
    }

    public void setTmpmediorisposta(String tmpmediorisposta) {
        this.tmpmediorisposta = tmpmediorisposta;
    }

    public String getTmpmedioabb() {
        return tmpmedioabb;
    }

    public void setTmpmedioabb(String tmpmedioabb) {
        this.tmpmedioabb = tmpmedioabb;
    }

    public String getTmpmaxattesa() {
        return tmpmaxattesa;
    }

    public void setTmpmaxattesa(String tmpmaxattesa) {
        this.tmpmaxattesa = tmpmaxattesa;
    }

    public int getTkaperti() {
        return tkaperti;
    }

    public void setTkaperti(int tkaperti) {
        this.tkaperti = tkaperti;
    }

    public int getTkgestiti() {
        return tkgestiti;
    }

    public void setTkgestiti(int tkgestiti) {
        this.tkgestiti = tkgestiti;
    }

    public Timestamp getTimeref() {
        return timeref;
    }

    public void setTimeref(Timestamp timeref) {
        this.timeref = timeref;
    }

    public int getOverflow() {
        return overflow;
    }

    public void setOverflow(int overflow) {
        this.overflow = overflow;
    }
}
