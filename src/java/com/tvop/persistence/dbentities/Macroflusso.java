package com.tvop.persistence.dbentities;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "f_macroflusso")
public class Macroflusso implements Serializable {
    private String macroflussoid;
    private String descr;
    private String sqlserverid;
    private boolean presplit;
    
    public Macroflusso() {
        
    }
    
    public Macroflusso(Macroflusso flusso){
        this.macroflussoid = flusso.macroflussoid;
        this.descr = flusso.descr;
        this.sqlserverid = flusso.sqlserverid;
        this.presplit = flusso.presplit;
    }
    
    @Id
    public String getMacroflussoid() {
        return macroflussoid;
    }

    public void setMacroflussoid(String macroflussoid) {
        this.macroflussoid = macroflussoid;
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public String getSqlserverid() {
        return sqlserverid;
    }

    public void setSqlserverid(String sqlserverid) {
        this.sqlserverid = sqlserverid;
    }

    public boolean isPresplit() {
        return presplit;
    }

    public void setPresplit(boolean presplit) {
        this.presplit = presplit;
    }
}
