package com.tvop.persistence.dbentities;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "k_kpichart2")
public class KpiChart2BCCSI implements Serializable {
    @Id
    private int serialid;
    private String area;
    private Timestamp timeref;
    private int offerte;
    private int risposte;
    private int abbandonate;
    private double percrisposte;
    private double percrispmin40;
    
    public KpiChart2BCCSI() {
        
    }

    public int getSerialid() {
        return serialid;
    }

    public void setSerialid(int serialid) {
        this.serialid = serialid;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public Timestamp getTimeref() {
        return timeref;
    }

    public void setTimeref(Timestamp timeref) {
        this.timeref = timeref;
    }

    public int getOfferte() {
        return offerte;
    }

    public void setOfferte(int offerte) {
        this.offerte = offerte;
    }

    public int getRisposte() {
        return risposte;
    }

    public void setRisposte(int risposte) {
        this.risposte = risposte;
    }

    public int getAbbandonate() {
        return abbandonate;
    }

    public void setAbbandonate(int abbandonate) {
        this.abbandonate = abbandonate;
    }

    public double getPercrisposte() {
        return percrisposte;
    }

    public void setPercrisposte(double percrisposte) {
        this.percrisposte = percrisposte;
    }

    public double getPercrispmin40() {
        return percrispmin40;
    }

    public void setPercrispmin40(double percrispmin40) {
        this.percrispmin40 = percrispmin40;
    }
}
