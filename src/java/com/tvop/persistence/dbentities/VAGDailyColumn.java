package com.tvop.persistence.dbentities;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ch_agentcolumn_d")
public class VAGDailyColumn implements Serializable {
    private String colheader;
    private String kpiid;
    private Integer ordering;
    private String label;
    private Integer type;
    
    public VAGDailyColumn() {

    }
    
    public VAGDailyColumn(VAGDailyColumn agCol){
        this.colheader = agCol.colheader;
        this.kpiid = agCol.kpiid;
        this.ordering = agCol.ordering;
        this.label = agCol.label;
        this.type = agCol.type;
    }

    @Id
    public String getColheader() {
        return colheader;
    }

    public void setColheader(String colheader) {
        this.colheader = colheader;
    }

    public String getKpiid() {
        return kpiid;
    }

    public void setKpiid(String kpiid) {
        this.kpiid = kpiid;
    }

    public Integer getOrdering() {
        return ordering;
    }

    public void setOrdering(Integer ordering) {
        this.ordering = ordering;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
}