/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tvop.persistence.dbentities;

import java.io.Serializable;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 *
 * @author matteo
 */
@Entity
@Table(name = "pc_ivrordering")
public class PC_IVROrdering {
    @EmbeddedId
    private Key key;

    public Key getKey() {
        return key;
    }

    public void setKey(Key key) {
        this.key = key;
    }
    
    @Embeddable
    public static class Key implements Serializable{
        private String coda;
        private String area;
        private Integer ordering;

        public String getCoda() {
            return coda;
        }

        public void setCoda(String coda) {
            this.coda = coda;
        }

        public String getArea() {
            return area;
        }

        public void setArea(String area) {
            this.area = area;
        }

        public Integer getOrdering() {
            return ordering;
        }

        public void setOrdering(Integer ordering) {
            this.ordering = ordering;
        }
        
    }
    
}
