package com.tvop.persistence.dbentities;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "a_wbticktopc")
public class AreasWbTicketToPC implements Serializable {
    @Id
    private int id;
    private String purecloud;
    private String wbticket;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPurecloud() {
        return purecloud;
    }

    public void setPurecloud(String purecloud) {
        this.purecloud = purecloud;
    }

    public String getWbticket() {
        return wbticket;
    }

    public void setWbticket(String wbticket) {
        this.wbticket = wbticket;
    }
}
