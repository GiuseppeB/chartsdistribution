package com.tvop.persistence.dbentities;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "t_timeref_rtagent")
public class TimerefRtAgent implements Serializable {
    private Timestamp timeref;
    
    public TimerefRtAgent() {
        
    }
    
    public TimerefRtAgent(TimerefRtAgent t){
        this.timeref = t.timeref;
    }
    
    @Id
    public Timestamp getTimeref() {
        return timeref;
    }

    public void setTimeref(Timestamp timeref) {
        this.timeref = timeref;
    }
}
