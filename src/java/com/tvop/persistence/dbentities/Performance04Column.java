package com.tvop.persistence.dbentities;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ch_performance04column")
public class Performance04Column implements Serializable {
    @Id
    private String kpiid;
    private int ordering;
    private String label;
    private int type;
    
    public Performance04Column() {
        
    }
    
    public Performance04Column(Performance04Column agCol){
        this.kpiid = agCol.kpiid;
        this.ordering = agCol.ordering;
        this.label = agCol.label;
        this.type = agCol.type;
    }

    public String getKpiid() {
        return kpiid;
    }

    public void setKpiid(String kpiid) {
        this.kpiid = kpiid;
    }

    public int getOrdering() {
        return ordering;
    }

    public void setOrdering(int ordering) {
        this.ordering = ordering;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
