package com.tvop.persistence.dbentities;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "pc_rt_flowin")
public class PC_RT_Queues_Inbound implements Serializable {
    @EmbeddedId
    private Key key;
    private String name;
    private int ricevute;
    private int gestite;
    private int ingestione;
    private int inattesa;
    private int acw;
    private int abbandonate;
    private float percabbandonate;
    private int dissuase;
    private int abbpremature;
    private float percabbpremature;
    private int maxattesa;
    private int tmrisposta;
    private int tmattesa;
    private int tmabbandono;
    private int tmconversazione;
    private float lds;
    private int overflowout;
    
    public PC_RT_Queues_Inbound() {   
        key = new Key();
    }

    public Key getKey() {
        return key;
    }

    public void setKey(Key key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRicevute() {
        return ricevute;
    }

    public void setRicevute(int ricevute) {
        this.ricevute = ricevute;
    }

    public int getGestite() {
        return gestite;
    }

    public void setGestite(int gestite) {
        this.gestite = gestite;
    }

    public int getIngestione() {
        return ingestione;
    }

    public void setIngestione(int ingestione) {
        this.ingestione = ingestione;
    }

    public int getInattesa() {
        return inattesa;
    }

    public void setInattesa(int inattesa) {
        this.inattesa = inattesa;
    }

    public int getAcw() {
        return acw;
    }

    public void setAcw(int acw) {
        this.acw = acw;
    }

    public int getAbbandonate() {
        return abbandonate;
    }

    public void setAbbandonate(int abbandonate) {
        this.abbandonate = abbandonate;
    }

    public float getPercabbandonate() {
        return percabbandonate;
    }

    public void setPercabbandonate(float percabbandonate) {
        this.percabbandonate = percabbandonate;
    }

    public int getDissuase() {
        return dissuase;
    }

    public void setDissuase(int dissuase) {
        this.dissuase = dissuase;
    }

    public int getAbbpremature() {
        return abbpremature;
    }

    public void setAbbpremature(int abbpremature) {
        this.abbpremature = abbpremature;
    }

    public float getPercabbpremature() {
        return percabbpremature;
    }

    public void setPercabbpremature(float percabbpremature) {
        this.percabbpremature = percabbpremature;
    }

    public int getMaxattesa() {
        return maxattesa;
    }

    public void setMaxattesa(int maxattesa) {
        this.maxattesa = maxattesa;
    }

    public int getTmrisposta() {
        return tmrisposta;
    }

    public void setTmrisposta(int tmrisposta) {
        this.tmrisposta = tmrisposta;
    }

    public int getTmattesa() {
        return tmattesa;
    }

    public void setTmattesa(int tmattesa) {
        this.tmattesa = tmattesa;
    }

    public int getTmabbandono() {
        return tmabbandono;
    }

    public void setTmabbandono(int tmabbandono) {
        this.tmabbandono = tmabbandono;
    }

    public int getTmconversazione() {
        return tmconversazione;
    }

    public void setTmconversazione(int tmconversazione) {
        this.tmconversazione = tmconversazione;
    }

    public float getLds() {
        return lds;
    }

    public void setLds(float lds) {
        this.lds = lds;
    }

    public int getOverflowout() {
        return overflowout;
    }

    public void setOverflowout(int overflowout) {
        this.overflowout = overflowout;
    }
    
    @Embeddable
    public static class Key implements Serializable {
        private String id;
        private Timestamp timeref;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public Timestamp getTimeref() {
            return timeref;
        }

        public void setTimeref(Timestamp timeref) {
            this.timeref = timeref;
        }
    }
}
