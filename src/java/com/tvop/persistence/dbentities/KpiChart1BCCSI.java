package com.tvop.persistence.dbentities;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "k_kpichart1_new")
public class KpiChart1BCCSI implements Serializable {
    @Id
    private int serialid;
    private Timestamp timeref;
    private String step;
    private int offerte;
    private int risposte;
    private int abbandonate;
    private int rispmin40;
    
    public KpiChart1BCCSI() {
        
    }

    public int getSerialid() {
        return serialid;
    }

    public void setSerialid(int serialid) {
        this.serialid = serialid;
    }

    public Timestamp getTimeref() {
        return timeref;
    }

    public void setTimeref(Timestamp timeref) {
        this.timeref = timeref;
    }

    public String getStep() {
        return step;
    }

    public void setStep(String step) {
        this.step = step;
    }

    public int getOfferte() {
        return offerte;
    }

    public void setOfferte(int offerte) {
        this.offerte = offerte;
    }

    public int getRisposte() {
        return risposte;
    }

    public void setRisposte(int risposte) {
        this.risposte = risposte;
    }

    public int getAbbandonate() {
        return abbandonate;
    }

    public void setAbbandonate(int abbandonate) {
        this.abbandonate = abbandonate;
    }

    public int getRispmin40() {
        return rispmin40;
    }

    public void setRispmin40(int rispmin40) {
        this.rispmin40 = rispmin40;
    }
}
