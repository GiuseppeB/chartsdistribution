package com.tvop.persistence.dbentities;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "k_realtimeagentkpiserie")
public class RealtimeAgentKpiSerie implements Serializable {
    private int serieid;
    private int kpiid;
    private Timestamp timeref;
    private String agentgroupid = "";
    private double value;
    private String arrowevaluation;
    
    public RealtimeAgentKpiSerie() {
        
    }
    
    public RealtimeAgentKpiSerie(RealtimeAgentKpiSerie rtKpiAG){
        this.serieid = rtKpiAG.serieid;
        this.kpiid = rtKpiAG.kpiid;
        this.timeref = rtKpiAG.timeref;
        this.agentgroupid = rtKpiAG.agentgroupid;
        this.value = rtKpiAG.value;
        this.arrowevaluation = rtKpiAG.arrowevaluation;
    }
    
    @Id
    public int getSerieid() {
        return serieid;
    }

    public void setSerieid(int serieid) {
        this.serieid = serieid;
    }

    public int getKpiid() {
        return kpiid;
    }

    public void setKpiid(int kpiid) {
        this.kpiid = kpiid;
    }

    public Timestamp getTimeref() {
        return timeref;
    }

    public void setTimeref(Timestamp timeref) {
        this.timeref = timeref;
    }

    public String getAgentgroupid() {
        return agentgroupid;
    }

    public void setAgentgroupid(String agentgroupid) {
        this.agentgroupid = agentgroupid;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public String getArrowevaluation() {
        return arrowevaluation;
    }

    public void setArrowevaluation(String arrowevaluation) {
        this.arrowevaluation = arrowevaluation;
    }
}
