package com.tvop.persistence.dbentities;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ch_vagrow")
public class VAGRow implements Serializable {
    private String agentgroupid;
    private int ordering;
    private String rowgui;
    
    public VAGRow() {
        
    }
    
    public VAGRow(VAGRow rtKpiAG){
        this.agentgroupid = rtKpiAG.agentgroupid;
        this.ordering = rtKpiAG.ordering;
        this.rowgui = rtKpiAG.rowgui;
    }
    
    @Id
    public String getAgentgroupid() {
        return agentgroupid;
    }

    public void setAgentgroupid(String agentgroupid) {
        this.agentgroupid = agentgroupid;
    }

    public int getOrdering() {
        return ordering;
    }

    public void setOrdering(int ordering) {
        this.ordering = ordering;
    } 

    public String getRowgui() {
        return rowgui;
    }

    public void setRowgui(String rowgui) {
        this.rowgui = rowgui;
    }
}
