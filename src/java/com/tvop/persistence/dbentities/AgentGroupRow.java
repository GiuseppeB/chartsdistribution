package com.tvop.persistence.dbentities;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ch_agentgrouprow")
public class AgentGroupRow implements Serializable {
    private String agentgroupid;
    private int ordering;
    private String label;
    
    public AgentGroupRow() {
        
    }
    
    public AgentGroupRow(AgentGroupRow rtKpiAG){
        this.agentgroupid = rtKpiAG.agentgroupid;
        this.ordering = rtKpiAG.ordering;
        this.label = rtKpiAG.label;
    }
    
    @Id
    public String getAgentgroupid() {
        return agentgroupid;
    }

    public void setAgentgroupid(String agentgroupid) {
        this.agentgroupid = agentgroupid;
    }

    public int getOrdering() {
        return ordering;
    }

    public void setOrdering(int ordering) {
        this.ordering = ordering;
    } 

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
