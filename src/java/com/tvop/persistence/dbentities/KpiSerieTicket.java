package com.tvop.persistence.dbentities;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "k_kpiserieticket_new")
public class KpiSerieTicket implements Serializable {
    private int serieid;
    private String kpiid;
    private Integer areaid;
    private String name;
    private double value;
    private String severity;
    private Timestamp timeref;
    private String procedura;
    
    public KpiSerieTicket() {
        
    }
    
    @Id
    public int getSerieid() {
        return serieid;
    }

    public void setSerieid(int serieid) {
        this.serieid = serieid;
    }

    public String getKpiid() {
        return kpiid;
    }

    public void setKpiid(String kpiid) {
        this.kpiid = kpiid;
    }

    public Integer getAreaid() {
        return areaid;
    }

    public void setAreaid(Integer areaid) {
        this.areaid = areaid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public String getSeverity() {
        return severity;
    }

    public void setSeverity(String severity) {
        this.severity = severity;
    }

    public Timestamp getTimeref() {
        return timeref;
    }

    public void setTimeref(Timestamp timeref) {
        this.timeref = timeref;
    }

    public String getProcedura() {
        return procedura;
    }

    public void setProcedura(String procedura) {
        this.procedura = procedura;
    }
}
