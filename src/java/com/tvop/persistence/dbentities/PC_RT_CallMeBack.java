package com.tvop.persistence.dbentities;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "pc_rt_cmb")
public class PC_RT_CallMeBack implements Serializable {
    @EmbeddedId
    private Key key;
    private String name;
    private int totali;
    private int gestite;
    private float percgestite;
    private int dagestire;
    private float percdagestire;
    private int ingestione;
    private float percingestione;
    
    public PC_RT_CallMeBack() {   
        key = new Key();
    }

    public Key getKey() {
        return key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTotali() {
        return totali;
    }

    public void setTotali(int totali) {
        this.totali = totali;
    }

    public int getGestite() {
        return gestite;
    }

    public void setGestite(int gestite) {
        this.gestite = gestite;
    }

    public float getPercgestite() {
        return percgestite;
    }

    public void setPercgestite(float percgestite) {
        this.percgestite = percgestite;
    }

    public int getDagestire() {
        return dagestire;
    }

    public void setDagestire(int dagestire) {
        this.dagestire = dagestire;
    }

    public float getPercdagestire() {
        return percdagestire;
    }

    public void setPercdagestire(float percdagestire) {
        this.percdagestire = percdagestire;
    }

    public int getIngestione() {
        return ingestione;
    }

    public void setIngestione(int ingestione) {
        this.ingestione = ingestione;
    }

    public float getPercingestione() {
        return percingestione;
    }

    public void setPercingestione(float percingestione) {
        this.percingestione = percingestione;
    }
    
    @Embeddable
    public static class Key implements Serializable {
        private String id;
        private Timestamp timeref;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public Timestamp getTimeref() {
            return timeref;
        }

        public void setTimeref(Timestamp timeref) {
            this.timeref = timeref;
        }
    }
}
