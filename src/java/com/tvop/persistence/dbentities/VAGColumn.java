package com.tvop.persistence.dbentities;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ch_vagcolumn")
public class VAGColumn implements Serializable {
    private String colheader;
    private String kpiid;
    private int ordering;
    private String colheadergui;
    private int type;
    
    public VAGColumn() {
        
    }
    
    public VAGColumn(VAGColumn vagCol){
        this.colheader = vagCol.colheader;
        this.kpiid = vagCol.kpiid;
        this.ordering = vagCol.ordering;
        this.colheadergui = vagCol.colheadergui;
        this.type = vagCol.type;
    }

    public String getColheader() {
        return colheader;
    }

    public void setColheader(String colheader) {
        this.colheader = colheader;
    }

    @Id
    public String getKpiid() {
        return kpiid;
    }

    public void setKpiid(String kpiid) {
        this.kpiid = kpiid;
    }

    public int getOrdering() {
        return ordering;
    }

    public void setOrdering(int ordering) {
        this.ordering = ordering;
    }

    public String getColheadergui() {
        return colheadergui;
    }

    public void setColheadergui(String colheadergui) {
        this.colheadergui = colheadergui;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
