package com.tvop.persistence.dbentities;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "pc_rt_wrapup")
public class PC_RT_WrapUp implements Serializable {
    @EmbeddedId
    private Key key;
    private String name;
    private int cmbrischedulate;
    private int dissuase;
    private int gestite;
    private int gestitenonconsegnate;
    private int inviatomessaggio;
    private int trasferite;
    private int trasferitebackoffice;
    private int trasferitetesoreria;
    
    public PC_RT_WrapUp() {   
        key = new Key();
    }

    public Key getKey() {
        return key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCmbrischedulate() {
        return cmbrischedulate;
    }

    public void setCmbrischedulate(int cmbrischedulate) {
        this.cmbrischedulate = cmbrischedulate;
    }

    public int getDissuase() {
        return dissuase;
    }

    public void setDissuase(int dissuase) {
        this.dissuase = dissuase;
    }

    public int getGestite() {
        return gestite;
    }

    public void setGestite(int gestite) {
        this.gestite = gestite;
    }

    public int getGestitenonconsegnate() {
        return gestitenonconsegnate;
    }

    public void setGestitenonconsegnate(int gestitenonconsegnate) {
        this.gestitenonconsegnate = gestitenonconsegnate;
    }

    public int getInviatomessaggio() {
        return inviatomessaggio;
    }

    public void setInviatomessaggio(int inviatomessaggio) {
        this.inviatomessaggio = inviatomessaggio;
    }

    public int getTrasferite() {
        return trasferite;
    }

    public void setTrasferite(int trasferite) {
        this.trasferite = trasferite;
    }

    public int getTrasferitebackoffice() {
        return trasferitebackoffice;
    }

    public void setTrasferitebackoffice(int trasferitebackoffice) {
        this.trasferitebackoffice = trasferitebackoffice;
    }

    public int getTrasferitetesoreria() {
        return trasferitetesoreria;
    }

    public void setTrasferitetesoreria(int trasferitetesoreria) {
        this.trasferitetesoreria = trasferitetesoreria;
    }
    
    @Embeddable
    public static class Key implements Serializable {
        private String id;
        private Timestamp timeref;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public Timestamp getTimeref() {
            return timeref;
        }

        public void setTimeref(Timestamp timeref) {
            this.timeref = timeref;
        }
    }
}
