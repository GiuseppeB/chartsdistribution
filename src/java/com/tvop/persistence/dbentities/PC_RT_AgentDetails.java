package com.tvop.persistence.dbentities;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "pc_rt_agentdetails")
public class PC_RT_AgentDetails implements Serializable {
    @EmbeddedId
    private Key key;
    private String name;
    private int totlogin;
    private int totcoda;
    private int totfuoricoda;
    private int totconversazione;
    private int tmconversazione;
    private int ricevute;
    private int gestite;
    
    public PC_RT_AgentDetails() {   
        key = new Key();
    }

    public Key getKey() {
        return key;
    }

    public void setKey(Key key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTotlogin() {
        return totlogin;
    }

    public void setTotlogin(int totlogin) {
        this.totlogin = totlogin;
    }

    public int getTotcoda() {
        return totcoda;
    }

    public void setTotcoda(int totcoda) {
        this.totcoda = totcoda;
    }

    public int getTotfuoricoda() {
        return totfuoricoda;
    }

    public void setTotfuoricoda(int totfuoricoda) {
        this.totfuoricoda = totfuoricoda;
    }

    public int getTotconversazione() {
        return totconversazione;
    }

    public void setTotconversazione(int totconversazione) {
        this.totconversazione = totconversazione;
    }

    public int getTmconversazione() {
        return tmconversazione;
    }

    public void setTmconversazione(int tmconversazione) {
        this.tmconversazione = tmconversazione;
    }

    public int getRicevute() {
        return ricevute;
    }

    public void setRicevute(int ricevute) {
        this.ricevute = ricevute;
    }

    public int getGestite() {
        return gestite;
    }

    public void setGestite(int gestite) {
        this.gestite = gestite;
    }
    
    @Embeddable
    public static class Key implements Serializable {
        private String id;
        private Timestamp timeref;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public Timestamp getTimeref() {
            return timeref;
        }

        public void setTimeref(Timestamp timeref) {
            this.timeref = timeref;
        }
    }
}
