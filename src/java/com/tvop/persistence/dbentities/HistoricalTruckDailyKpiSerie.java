package com.tvop.persistence.dbentities;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "k_historicaltruckkpiserie_d")
public class HistoricalTruckDailyKpiSerie implements Serializable {
    private int serieid;
    private String kpiid;
    private Timestamp timeref;
    private String macroflussoid;
    private String microflussoid;
    private double value;

    @Id
    public int getSerieid() {
        return serieid;
    }

    public void setSerieid(int serieid) {
        this.serieid = serieid;
    }

    public String getKpiid() {
        return kpiid;
    }

    public void setKpiid(String kpiid) {
        this.kpiid = kpiid;
    }

    public Timestamp getTimeref() {
        return timeref;
    }

    public void setTimeref(Timestamp timeref) {
        this.timeref = timeref;
    }

    public String getMacroflussoid() {
        return macroflussoid;
    }

    public void setMacroflussoid(String macroflussoid) {
        this.macroflussoid = macroflussoid;
    }

    public String getMicroflussoid() {
        return microflussoid;
    }

    public void setMicroflussoid(String microflussoid) {
        this.microflussoid = microflussoid;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    
}
