package com.tvop.persistence;

import com.tvop.exceptions.DMLException;
import com.tvop.persistence.dbentities.ThresholdEvaluationKpi;
import com.tvop.utils.HibernateUtil;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javax.faces.context.FacesContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

public class ThresholdEvaluationJPA {
    static final transient Logger LOGGER = LogManager.getLogger(ThresholdEvaluationJPA.class.getName());
    static final transient ResourceBundle BUNDLE = FacesContext.getCurrentInstance().getApplication().getResourceBundle(FacesContext.getCurrentInstance(), "txt");

    public static List<ThresholdEvaluationKpi> getAll() throws DMLException {
        Session session = null;
        List<ThresholdEvaluationKpi> result = new ArrayList<>();
        
        try {
            session = HibernateUtil.getSession();

            Criteria criteria = session.createCriteria(ThresholdEvaluationKpi.class)
                    .addOrder(Order.asc("label"));
            result = criteria.list();

            if (result.isEmpty()) {
                throw new DMLException("ThresholdEvaluation table empty");
            }
        } catch (DMLException | HibernateException e) {
            e.printStackTrace();
            LOGGER.error("Error getting vag cols list " + " " + e.getMessage());
            throw new DMLException(BUNDLE.getString("message.noDataFound"));
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return result;
    }

    public static ThresholdEvaluationKpi getAllByKpi(String kpiid) throws DMLException {
        Session session = null;
        ThresholdEvaluationKpi result = new ThresholdEvaluationKpi();
        try {
            session = HibernateUtil.getSession();
            result = (ThresholdEvaluationKpi) session.createCriteria(ThresholdEvaluationKpi.class).add(Restrictions.eq("kpiid", kpiid)).uniqueResult();
        } catch (HibernateException e) {
            LOGGER.error("Error getAllById " + e.getMessage());
            throw new DMLException("Error getAllById " + e.getMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return result;
    }
    
    public static void update(List<ThresholdEvaluationKpi> evaluationList) {
        Session session = null;
        Transaction t = null;

        try {
            session = HibernateUtil.getSession();
            t = session.beginTransaction();
            for (ThresholdEvaluationKpi eval : evaluationList) {
                if(!eval.isEnabledgood()){
                    eval.setGoodformula("");
                    eval.setGoodvalue(0);
                }
                if(!eval.isEnabledbad()){
                    eval.setBadformula("");
                    eval.setBadvalue(0);
                }
                session.update(eval);
            }
            t.commit();
                
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }
}
