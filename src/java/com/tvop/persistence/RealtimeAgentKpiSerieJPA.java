package com.tvop.persistence;

import com.tvop.exceptions.DMLException;
import com.tvop.persistence.dbentities.AgentGroupColumn;
import com.tvop.persistence.dbentities.AgentGroupRow;
import com.tvop.utils.HibernateUtil;
import com.tvop.utils.IntervalBoundariesStatica;
import com.tvop.utils.Utils;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javax.faces.context.FacesContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.joda.time.DateTime;

public class RealtimeAgentKpiSerieJPA {

    static final transient Logger LOGGER = LogManager.getLogger(RealtimeAgentKpiSerieJPA.class.getName());
    static final transient ResourceBundle BUNDLE = FacesContext.getCurrentInstance().getApplication().getResourceBundle(FacesContext.getCurrentInstance(), "txt");

    public static List<Object> getAll() throws DMLException {
        Session session = null;
        List<Object> result = new ArrayList<>();

        try {
            session = HibernateUtil.getSession();

            Timestamp ts = new Timestamp(IntervalBoundariesStatica.getStartOfDay(new DateTime()).getMillis());

            result = session.createQuery(
                    "SELECT kpi.value, r.ordering, c.ordering, r.agentgroupid, c.type, kpi.arrowevaluation "
                    + "FROM RealtimeAgentKpiSerie kpi, AgentGroupRow r, AgentGroupColumn c "
                    + "WHERE kpi.kpiid = c.kpiid "
                    + "AND kpi.agentgroupid = r.agentgroupid "
                    + "AND kpi.timeref = (SELECT MAX(timeref) "
                    + "FROM TimerefRtAgent "
                    + "WHERE timeref >= '" + ts + "')")
                    .list();
        } catch (Exception e) {
            LOGGER.error("Error getting real time AG " + e.getMessage());
            throw new DMLException(BUNDLE.getString("message.COTColserror") + " " + e.toString());
        } finally {
            if (session != null) {
                session.close();
            }
        }

        return result;
    }

    public static List<Object> getAll(int hierarchyId) throws DMLException {
        Session session = null;
//        List<RealtimeAgentKpiSerie> rtAgentkpis = new ArrayList<>();
        List<Object> rtAgentkpis = new ArrayList<>();

        try {
            session = HibernateUtil.getSession();

            rtAgentkpis = session.createQuery(
                    "SELECT kpi.value, r.ordering, c.ordering, r.agentgroupid, c.type, kpi.arrowevaluation "
                    + "FROM RealtimeAgentKpiSerie kpi, AgentGroupRow r, AgentGroupColumn c, HierarchyGroup hg "
                    + "WHERE kpi.kpiid = c.kpiid "
                    + "AND kpi.agentgroupid = hg.pk.groupid "
                    + "AND hg.pk.idhierarchy = " + hierarchyId
                    + " AND kpi.agentgroupid = r.agentgroupid "
                    + "AND kpi.timeref = (SELECT MAX(timeref) "
                    + "FROM TimerefRtAgent)")
                    .list();

            if (rtAgentkpis == null) {
                throw new DMLException("real time AG not found");
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
            LOGGER.error("Error getting real time AG " + e.getMessage());
            throw new DMLException(BUNDLE.getString("message.COTColserror") + " " + e.toString());
        } finally {
            if (session != null) {
                session.close();
            }
        }

        return rtAgentkpis;
    }

    public static List<AgentGroupColumn> getCols() throws DMLException {
        Session session = null;
        List<AgentGroupColumn> cols = new ArrayList<>();

        try {
            session = HibernateUtil.getSession();

            cols = session.createCriteria(AgentGroupColumn.class)
                    .addOrder(Order.asc("ordering")).list();

            if (cols == null) {
                throw new DMLException("column AG not found");
            }
        } catch (Exception e) {
            LOGGER.error("Error getting column AG " + e.getMessage());
            throw new DMLException(BUNDLE.getString("message.COTColserror") + " " + e.toString());
        } finally {
            if (session != null) {
                session.close();
            }
        }

        return cols;
    }

    public static List<AgentGroupRow> getRows() throws DMLException {
        Session session = null;
        List<AgentGroupRow> rows = new ArrayList<>();

        try {
            session = HibernateUtil.getSession();

            rows = session.createCriteria(AgentGroupRow.class)
                    .addOrder(Order.asc("ordering")).list();

            if (rows == null) {
                throw new DMLException("column AG not found");
            }
        } catch (Exception e) {
            LOGGER.error("Error getting column AG " + e.getMessage());
            throw new DMLException(BUNDLE.getString("message.COTColserror") + " " + e.toString());
        } finally {
            if (session != null) {
                session.close();
            }
        }

        return rows;
    }

    public static List<Object> getAllMobile() throws DMLException {
        Session session = null;
//        List<RealtimeAgentKpiSerie> rtAgentkpis = new ArrayList<>();
        List<Object> rtAgentkpis = new ArrayList<>();

        try {
            session = HibernateUtil.getSession();

            rtAgentkpis = session.createQuery(
                    "SELECT kpi.value, r.ordering, c.ordering, r.agentgroupid, c.type, kpi.arrowevaluation "
                    + "FROM RealtimeAgentKpiSerie kpi, AgentGroupRow r, AGColumnMobile c "
                    + "WHERE kpi.kpiid = c.kpiid "
                    + "AND kpi.agentgroupid = r.agentgroupid "
                    + "AND kpi.timeref = (SELECT MAX(timeref) "
                    + "FROM TimerefRtAgent)")
                    .list();

            if (rtAgentkpis == null) {
                throw new DMLException("real time AG not found");
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
            LOGGER.error("Error getting real time AG " + e.getMessage());
            throw new DMLException(BUNDLE.getString("message.COTColserror") + " " + e.toString());
        } finally {
            if (session != null) {
                session.close();
            }
        }

        return rtAgentkpis;
    }
}
