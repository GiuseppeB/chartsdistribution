package com.tvop.persistence;

import com.tvop.beans.charts.Chart3bccsi;
import com.tvop.beans.charts.Chart4bccsi;
import com.tvop.persistence.dbentities.KpiSerieCatalogazione;
import com.tvop.persistence.dbentities.KpiSerieRichieste;
import com.tvop.persistence.dbentities.KpiSerieTicket;
import com.tvop.utils.HibernateUtil;
import com.tvop.utils.IntervalBoundaries;
import com.tvop.utils.IntervalsBCCSI;
import static com.tvop.utils.IntervalsBCCSI.DAY;
import static com.tvop.utils.IntervalsBCCSI.WEEK;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import javax.faces.context.FacesContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.joda.time.DateTime;

public class BccsiJPA {

    static final transient Logger LOGGER = LogManager.getLogger(BccsiJPA.class.getName());
    static final transient ResourceBundle BUNDLE = FacesContext.getCurrentInstance().getApplication().getResourceBundle(FacesContext.getCurrentInstance(), "txt");

    public static List<Chart3bccsi> getLastForAllTicket() {
        List<KpiSerieTicket> temp = null;
        List<Chart3bccsi> result = new ArrayList<>();

        List<String> areas = new ArrayList<>();
        Chart3bccsi ticket = new Chart3bccsi();
        Session session = null;
        try {
            session = HibernateUtil.getSession();
            Timestamp ts = new Timestamp(IntervalBoundaries.getStartOfDay(new DateTime(2020, 6, 5, 18, 15)).getMillis());
            String query 
                    = "SELECT DISTINCT C "
                    + "FROM KpiSerieTicket C "
                    + "WHERE C.timeref = :ts "
                    + "AND C.kpiid != '57' "
                    + "AND C.name IN (SELECT DISTINCT pc.wbticket FROM AreasWbTicketToPC pc) "
                    + "ORDER BY C.name ASC, C.procedura ASC";
            Query q = session.createQuery(query);
            q.setParameter("ts", ts);
            temp = q.list();
            int i = 0;
            double[][] values = new double[temp.size()][8];
            for (KpiSerieTicket kpi : temp) {
                if (areas.size() < temp.size()) {
                    areas.add(kpi.getName());
                }
                switch (kpi.getKpiid()) {
                    case "50":
                        values[i][0] = kpi.getValue();
                        break;
                    case "51":
                        values[i][1] = kpi.getValue();
                        break;
                    case "52":
                        values[i][2] = kpi.getValue();
                        break;
                    case "53":
                        values[i][3] = kpi.getValue();
                        break;
                    case "60":
                        values[i][4] = kpi.getValue();
                        break;
                    case "54":
                        values[i][5] = kpi.getValue();
                        break;
                    case "55":
                        values[i][6] = kpi.getValue();
                        break;
                    case "56":
                        values[i][7] = kpi.getValue();
                        break;
                }
                i++;
            }
            for (int r = 0; r < temp.size(); r++) {
                ticket.setArea(areas.get(r));
                ticket.setProcedura(temp.get(r).getProcedura());
                ticket.setTkPublisher((int) values[r][0]);
                ticket.setTkNotAccepted((int) values[r][1]);
                ticket.setPercTkNotAccepted(values[r][2]);
                ticket.setTkClosed((int) values[r][3]);
                ticket.setTkAccepted((int) values[r][4]);
                ticket.setTkTecnicalIntervention((int) values[r][5]);
                ticket.setBacklogAssistance((int) values[r][6]);
                ticket.setBacklogTerzeParti((int) values[r][7]);
                result.add(ticket);
                ticket = new Chart3bccsi();
            }
        } catch (Exception e) {
            LOGGER.error("Error getting getLastForAllTicket " + e.getMessage());
            return new ArrayList<>();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return result;
    }

    public static List<Chart3bccsi> getYesterdayAllTicket() {
        List<KpiSerieTicket> temp = null;
        List<Chart3bccsi> result = new ArrayList<>();
        List<String> areas = new ArrayList<>();
        Chart3bccsi ticket = new Chart3bccsi();
        Session session = null;
        try {
            session = HibernateUtil.getSession();
            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date());
            boolean monday = cal.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY;
            Timestamp ts = null;
            if (monday) {
                ts = new Timestamp(IntervalBoundaries.getStartOfDay(new DateTime(2020, 6, 5, 18, 15).minusDays(3)).getMillis());
            } else {
                ts = new Timestamp(IntervalBoundaries.getStartOfDay(new DateTime(2020, 6, 5, 18, 15).minusDays(1)).getMillis());
            }
            String query 
                    = "SELECT DISTINCT C "
                    + "FROM KpiSerieTicket C "
                    + "WHERE C.timeref = :ts "
                    + "AND C.kpiid != '57' "
                    + "AND C.name IN (SELECT DISTINCT pc.wbticket FROM AreasWbTicketToPC pc) "
                    + "ORDER BY C.name ASC, C.procedura ASC";
            Query q = session.createQuery(query);
            q.setParameter("ts", ts);
            temp = q.list();            
            if (!temp.isEmpty()) {
                int i = 0;
                double[][] values = new double[temp.size()][8];
                for (KpiSerieTicket kpi : temp) {
                    if (areas.size() < temp.size()) {
                        areas.add(kpi.getName());
                    }
                    switch (kpi.getKpiid()) {
                        case "50":
                            values[i][0] = kpi.getValue();
                            break;
                        case "51":
                            values[i][1] = kpi.getValue();
                            break;
                        case "52":
                            values[i][2] = kpi.getValue();
                            break;
                        case "53":
                            values[i][3] = kpi.getValue();
                            break;
                        case "60":
                            values[i][4] = kpi.getValue();
                            break;
                        case "54":
                            values[i][5] = kpi.getValue();
                            break;
                        case "55":
                            values[i][6] = kpi.getValue();
                            break;
                        case "56":
                            values[i][7] = kpi.getValue();
                            break;
                    }
                    i++;
                }
                for (int r = 0; r < temp.size(); r++) {
                    ticket.setArea(areas.get(r));
                    ticket.setProcedura(temp.get(r).getProcedura());
                    ticket.setTkPublisher((int) values[r][0]);
                    ticket.setTkNotAccepted((int) values[r][1]);
                    ticket.setPercTkNotAccepted(values[r][2]);
                    ticket.setTkClosed((int) values[r][3]);
                    ticket.setTkAccepted((int) values[r][4]);
                    ticket.setTkTecnicalIntervention((int) values[r][5]);
                    ticket.setBacklogAssistance((int) values[r][6]);
                    ticket.setBacklogTerzeParti((int) values[r][7]);
                    result.add(ticket);
                    ticket = new Chart3bccsi();
                }
            }
        } catch (Exception e) {
            LOGGER.error("Error getting getLastForAllTicket " + e.getMessage());
            return new ArrayList<>();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return result;
    }

    public static List<Chart4bccsi> getLastForChart4() {
        List<Chart4bccsi> result = new ArrayList<>();
        Session session = null;
        try {
            session = HibernateUtil.getSession();
            Timestamp ts = new Timestamp(IntervalBoundaries.getStartOfDay(new DateTime(2020, 6, 5, 18, 15)).getMillis());
            String query
                    = "SELECT C.name, C.kpiid, SUM(C.value) "
                    + "FROM KpiSerieTicket C "
                    + "WHERE C.kpiid IN ('50', '55', '57') "
                    + "AND C.name IN (SELECT DISTINCT pc.wbticket FROM AreasWbTicketToPC pc) "
                    + "AND timeref = :ts "
                    + "GROUP BY C.name, C.kpiid "
                    + "ORDER BY C.name DESC";
            Query q = session.createQuery(query);
            q.setParameter("ts", ts);
            List<Object> temp = q.list();
            for(Object o : temp) {
                Object[] elem = (Object[]) o;
                Chart4bccsi ticket = new Chart4bccsi();
                ticket.setArea(elem[0].toString());
                Double val = Double.valueOf(elem[2].toString());
                switch(elem[1].toString()) {
                    case "50":
                        ticket.setTkPubblicati(val.intValue());
                        break;
                    case "55":
                        ticket.setTkInEssere(val.intValue());
                        break;
                    case "57":
                        ticket.setTkChiusi(val.intValue());
                        break;
                }
                result.add(ticket);
            }
        } catch (Exception e) {
            LOGGER.error("Error getting getLastForChart4 " + e.getMessage());
            return new ArrayList<>();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return result;
    }
    
    public static List<KpiSerieRichieste> getAllByStepChart5(IntervalsBCCSI step) {
        List<KpiSerieRichieste> result = null;
        Session session = null;
        try {
            session = HibernateUtil.getSession();
            Timestamp ts = null;
            switch(step) {
                case DAY:
                    ts = new Timestamp(IntervalBoundaries.getStartOfDay(new DateTime(2020, 6, 5, 18, 15)).getMillis());
                    break;
                case WEEK:
                    ts = new Timestamp(IntervalBoundaries.getStartOfWeek(new DateTime(2020, 6, 5, 18, 15)).getMillis());
                    break;
                case MONTH:
                    ts = new Timestamp(IntervalBoundaries.getStartOfMonth(new DateTime(2020, 6, 5, 18, 15)).getMillis());
                    break;
            }
            result = session.createCriteria(KpiSerieRichieste.class)
                    .add(Restrictions.eq("timeref", ts))
                    .add(Restrictions.eq("step", step.getValue())).list();
        } catch (HibernateException e) {
            LOGGER.error("Error getting getAllByStepChart5 " + e.getMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return result;
    }
    
    public static List<KpiSerieCatalogazione> getAllByStepChart6(IntervalsBCCSI step) {
        List<KpiSerieCatalogazione> result = null;
        Session session = null;
        try {
            session = HibernateUtil.getSession();
            Timestamp ts = null;
            switch(step) {
                case DAY:
                    ts = new Timestamp(IntervalBoundaries.getStartOfDay(new DateTime(2020, 6, 5, 18, 15)).getMillis());
                    break;
                case WEEK:
                    ts = new Timestamp(IntervalBoundaries.getStartOfWeek(new DateTime(2020, 6, 5, 18, 15)).getMillis());
                    break;
                case MONTH:
                    ts = new Timestamp(IntervalBoundaries.getStartOfMonth(new DateTime(2020, 6, 5, 18, 15)).getMillis());
                    break;
            }
            result = session.createCriteria(KpiSerieCatalogazione.class)
                    .add(Restrictions.eq("timeref", ts))
                    .add(Restrictions.eq("step", step.getValue())).list();
        } catch (HibernateException e) {
            LOGGER.error("Error getting getAllByStepChart6 " + e.getMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return result;
    }
    
    public static double getPublischedTicket(IntervalsBCCSI step) {
        double result = 0;
        Session session = null;
        try {
            session = HibernateUtil.getSession();
            Timestamp ts = null;
            switch(step) {
                case DAY:
                    ts = new Timestamp(IntervalBoundaries.getStartOfDay(new DateTime(2020, 6, 5, 18, 15)).getMillis());
                    break;
                case WEEK:
                    ts = new Timestamp(IntervalBoundaries.getStartOfWeek(new DateTime(2020, 6, 5, 18, 15)).getMillis());
                    break;
                case MONTH:
                    ts = new Timestamp(IntervalBoundaries.getStartOfMonth(new DateTime(2020, 6, 5, 18, 15)).getMillis());
                    break;
            }
            List<KpiSerieTicket> temp = session.createCriteria(KpiSerieTicket.class)
                    .add(Restrictions.ge("timeref", ts))
                    .add(Restrictions.eq("kpiid", "50")).list();
            for(KpiSerieTicket ticket : temp) {
                result += ticket.getValue();
            }
        } catch (HibernateException e) {
            LOGGER.error("Error getting getPublischedTicket " + e.getMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return result;
    }
}
