package com.tvop.persistence;

import com.tvop.exceptions.DMLException;
import com.tvop.utils.HibernateUtil;
import com.tvop.utils.IntervalBoundariesStatica;
import com.tvop.utils.Utils;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javax.faces.context.FacesContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.joda.time.DateTime;

public class RealtimeCosanTruckKpiSerieJPA {

    static final transient Logger LOGGER = LogManager.getLogger(RealtimeCosanTruckKpiSerieJPA.class.getName());
    static final transient ResourceBundle BUNDLE = FacesContext.getCurrentInstance().getApplication().getResourceBundle(FacesContext.getCurrentInstance(), "txt");

    public static List<Object> getAll() throws DMLException {
        Session session = null;
        List<Object> result = new ArrayList<>();

        try {
            session = HibernateUtil.getSession();

            Timestamp ts = new Timestamp(IntervalBoundariesStatica.getStartOfDay(new DateTime()).getMillis());

            result = session.createQuery(
                    "SELECT kpi.value, r.ordering, c.ordering, r.label, c.type, kpi.arrowevaluation "
                    + "FROM RealtimeTruckKpiSerie kpi, CosanTruckRow r, CosanTruckColumn c "
                    + "WHERE kpi.kpiid = c.kpiid "
                    + "AND kpi.macroflussoid = r.macroflussoid "
                    + "AND kpi.timeref = (SELECT MAX(timeref) "
                    + "FROM TimerefRtTruck "
                    + "WHERE timeref >= '" + ts + "')")
                    .list();
        } catch (Exception e) {
            LOGGER.error("Error getting real time Cosan Truck " + e.getMessage());
            throw new DMLException(BUNDLE.getString("message.COTColserror") + " " + e.toString());
        } finally {
            if (session != null) {
                session.close();
            }
        }

        return result;
    }

    public static List<String> getCols() throws DMLException {
        Session session = null;
        List<String> cols = new ArrayList<>();

        try {
            session = HibernateUtil.getSession();

            cols = session.createQuery(
                    "SELECT label "
                    + "FROM CosanTruckColumn "
                    + "ORDER BY ordering ASC")
                    .list();

            if (cols == null) {
                throw new DMLException("column Cosan Truck not found");
            }
        } catch (DMLException | HibernateException e) {
            LOGGER.error("Error getting column Cosan Truck " + e.getMessage());
            throw new DMLException(BUNDLE.getString("message.COTColserror") + " " + e.toString());
        } finally {
            if (session != null) {
                session.close();
            }
        }

        return cols;
    }

    public static List<String> getRows() throws DMLException {
        Session session = null;
        List<String> rows = new ArrayList<>();

        try {
            session = HibernateUtil.getSession();

            rows = session.createQuery(
                    "SELECT label "
                    + "FROM CosanTruckRow "
                    + "ORDER BY ordering ASC")
                    .list();

            if (rows == null) {
                throw new DMLException("row Cosan Truck not found");
            }
        } catch (DMLException | HibernateException e) {
            LOGGER.error("Error getting row Cosan Truck " + e.getMessage());
            throw new DMLException(BUNDLE.getString("message.COTColserror") + " " + e.toString());
        } finally {
            if (session != null) {
                session.close();
            }
        }

        return rows;
    }
}
