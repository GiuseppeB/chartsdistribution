package com.tvop.persistence;

import com.tvop.exceptions.DMLException;
import com.tvop.persistence.dbentities.KpiChart1BCCSI;
import com.tvop.persistence.dbentities.KpiChart2BCCSI;
import com.tvop.persistence.dbentities.KpiRealtimeChart1Sinergia;
import com.tvop.persistence.dbentities.KpiRealtimeChart2Sinergia;
import com.tvop.persistence.dbentities.OrderingAreaSinergia;
import com.tvop.utils.HibernateUtil;
import com.tvop.utils.IntervalBoundariesStatica;
import com.tvop.utils.IntervalsBCCSI;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javax.faces.context.FacesContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.joda.time.DateTime;

public class KpiRealtimeJPA {

    static final transient Logger LOGGER = LogManager.getLogger(KpiRealtimeJPA.class.getName());
    static final transient ResourceBundle BUNDLE = FacesContext.getCurrentInstance().getApplication().getResourceBundle(FacesContext.getCurrentInstance(), "txt");

    public static List<KpiChart1BCCSI> getAllChart1NRT(IntervalsBCCSI interval) throws DMLException {
        Session session = null;
        List<KpiChart1BCCSI> result = new ArrayList<>();
        String step = "D";
        if (interval.equals(IntervalsBCCSI.WEEK)) {
            step = "W";
        } else if (interval.equals(IntervalsBCCSI.MONTH)) {
            step = "M";
        }

        try {
            session = HibernateUtil.getSession();
            Timestamp ts = null;
            switch (interval) {
                case DAY:
                    ts = new Timestamp(IntervalBoundariesStatica.getStartOfDay(new DateTime(2020, 6, 5, 0, 0)).getMillis());
                    break;
                case WEEK:
                    ts = new Timestamp(IntervalBoundariesStatica.getStartOfWeek(new DateTime(2020, 6, 5, 0, 0)).getMillis());
                    break;
                case MONTH:
                    ts = new Timestamp(IntervalBoundariesStatica.getStartOfMonth(new DateTime()).getMillis());
                    break;
            }
            result = session.createCriteria(KpiChart1BCCSI.class)
                    .add(Restrictions.eq("step", step))
                    .add(Restrictions.ge("timeref", ts))
                    .add(Restrictions.le("timeref", new Timestamp(new DateTime(2020, 6, 5, 23, 59).getMillis())))
                    .addOrder(Order.asc("timeref"))
                    .list();

        } catch (HibernateException e) {
            throw new DMLException("Error getAllChart1NRT " + e.getMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return result;
    }

    public static List<KpiChart2BCCSI> getAllChart2NRT(boolean old) throws DMLException {
        Session session = null;
        List<KpiChart2BCCSI> result = new ArrayList<>();

        try {
            session = HibernateUtil.getSession();
            if (old) {
                Timestamp ts = new Timestamp(IntervalBoundariesStatica.getStartOfHalfH(new DateTime(2020, 6, 5, 18, 15).minusDays(1)).getMillis());
                result = session.createCriteria(KpiChart2BCCSI.class)
                        .add(Restrictions.eq("timeref", ts))
                        .list();
            } else {
//                DetachedCriteria maxQuery = DetachedCriteria.forClass(KpiChart2BCCSI.class);
//                maxQuery.setProjection(Projections.max("timeref"));
//                result = session.createCriteria(KpiChart2BCCSI.class)
//                        .add(Property.forName("timeref").eq(maxQuery))
//                        .list();
                Timestamp ts = new Timestamp(IntervalBoundariesStatica.getStartOfHalfH(new DateTime(2020, 6, 5, 18, 15)).getMillis());
                result = session.createCriteria(KpiChart2BCCSI.class)
                        .add(Restrictions.eq("timeref", ts))
                        .list();
            }

        } catch (HibernateException e) {
            throw new DMLException("Error getAllChart2NRT " + e.getMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return result;
    }
    
    
    // *********************************** SINERGIA ******************************************
    
    public static List<KpiRealtimeChart1Sinergia> getAllChart1RTsinergia(boolean old) {
        Session session = null;
        List<KpiRealtimeChart1Sinergia> result = new ArrayList<>();
        Timestamp ts = new Timestamp(IntervalBoundariesStatica.getStartOfDay(DateTime.now()).getMillis());  
        if (old) {
            ts = new Timestamp(IntervalBoundariesStatica.getStartOfDay(DateTime.now().minusDays(1)).getMillis());
        }

        try {
            session = HibernateUtil.getSession();

            result = session.createCriteria(KpiRealtimeChart1Sinergia.class)
                    .add(Restrictions.eq("timeref", ts))
                    .addOrder(Order.asc("nome"))
                    .list();

        } catch (HibernateException e) {
            return new ArrayList<>();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return result;
    }
    
    public static List<KpiRealtimeChart2Sinergia> getAllChart2RTsinergia(boolean old) {
        Session session = null;
        List<KpiRealtimeChart2Sinergia> result = new ArrayList<>();
        Timestamp ts = new Timestamp(IntervalBoundariesStatica.getStartOfDay(DateTime.now()).getMillis());
        if (old) {
            ts = new Timestamp(IntervalBoundariesStatica.getStartOfDay(DateTime.now().minusDays(1)).getMillis());
        }

        try {
            session = HibernateUtil.getSession();

            result = session.createCriteria(KpiRealtimeChart2Sinergia.class)
                    .add(Restrictions.eq("timeref", ts))
                    .addOrder(Order.asc("nome"))
                    .list();

        } catch (HibernateException e) {
            return new ArrayList<>();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return result;
    }

    public static List<OrderingAreaSinergia> getOrderingArea() {
        Session session = null;
        List<OrderingAreaSinergia> result = new ArrayList<>();
        try {
            session = HibernateUtil.getSession();

            result = session.createCriteria(OrderingAreaSinergia.class).list();

        } catch (HibernateException e) {
            return new ArrayList<>();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return result;
    }
}
