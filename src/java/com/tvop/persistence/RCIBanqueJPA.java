package com.tvop.persistence;

import com.tvop.exceptions.DMLException;
import com.tvop.persistence.dbentities.HistoricalTruckDailyKpiSerie;
import com.tvop.persistence.dbentities.Macroflusso;
import com.tvop.persistence.dbentities.RealtimeTruckKpiSerie;
import com.tvop.utils.HibernateUtil;
import com.tvop.utils.IntervalBoundaries;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javax.faces.context.FacesContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.joda.time.DateTime;

public class RCIBanqueJPA {

    static final transient Logger LOGGER = LogManager.getLogger(RCIBanqueJPA.class.getName());
    static final transient ResourceBundle BUNDLE = FacesContext.getCurrentInstance().getApplication().getResourceBundle(FacesContext.getCurrentInstance(), "txt");

    public static double[] getAll(String kpiId, String macroflussoId) throws DMLException {
        Session session = null;
        int halfHours = 48;
        double[] result = new double[halfHours];
        int nowHalfHour = rowPosition(new Timestamp(IntervalBoundaries.getStartOfHalfH(new DateTime().minusMinutes(6)).getMillis()));
        for (int i = 0; i < result.length; i++) {
            if (i <= nowHalfHour) {
                result[i] = 0;
            } else {
                result[i] = Double.NaN;
            }
        }

        try {
            session = HibernateUtil.getSession();
            DateTime dt = IntervalBoundaries.getStartOfDay(new DateTime());
            Timestamp timeref = new Timestamp(dt.getMillis());

            List<HistoricalTruckDailyKpiSerie> temp = session.createCriteria(HistoricalTruckDailyKpiSerie.class)
                    .add(Restrictions.eq("kpiid", kpiId))
                    .add(Restrictions.eq("macroflussoid", macroflussoId))
                    .add(Restrictions.gt("timeref", timeref))
                    .list();

            if (!temp.isEmpty()) {
                for (HistoricalTruckDailyKpiSerie elem : temp) {
                    int row = rowPosition(elem.getTimeref());
                    result[row] = elem.getValue();
                }
            }

        } catch (HibernateException e) {
            LOGGER.error("Error getAll " + e.getMessage());
            throw new DMLException(BUNDLE.getString("message.COTColserror") + " " + e.toString());
        } finally {
            if (session != null) {
                session.close();
            }
        }

        return result;
    }

    public static List<Macroflusso> getTrucksName() throws DMLException {
        Session session = null;
        List<Macroflusso> result = new ArrayList<>();

        try {
            session = HibernateUtil.getSession();

            result = session.createCriteria(Macroflusso.class).list();
        } catch (HibernateException e) {
            LOGGER.error("Error getTrucksName " + e.getMessage());
            throw new DMLException(BUNDLE.getString("message.COTColserror") + " " + e.toString());
        } finally {
            if (session != null) {
                session.close();
            }
        }

        return result;
    }

    public static double getDailyValue(String kpiid, String flusso) {
        Session session = null;
        RealtimeTruckKpiSerie result = null;
        try {
            session = HibernateUtil.getSession();
            DetachedCriteria maxQuery = DetachedCriteria.forClass(RealtimeTruckKpiSerie.class)
                    .add(Restrictions.gt("timeref", new Timestamp(IntervalBoundaries.getStartOfDay(DateTime.now()).getMillis())))
                    .setProjection(Projections.max("timeref"));

            result = (RealtimeTruckKpiSerie) session.createCriteria(RealtimeTruckKpiSerie.class)
                    .add(Restrictions.eq("kpiid", kpiid))
                    .add(Restrictions.eq("macroflussoid", flusso))
                    .add(Property.forName("timeref").eq(maxQuery))
                    .uniqueResult();
            if (result == null) {
                return 0;
            }
        } catch (Exception e) {
            LOGGER.error("Error getDailyValue " + e.getMessage());
            return 0;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return result.getValue();
    }

    public static String getDailyArrow(String kpiid, String flusso) {
        Session session = null;
        RealtimeTruckKpiSerie result = null;
        String noVal = "./resources/icons/NotVaried.png";
        try {
            session = HibernateUtil.getSession();
            DetachedCriteria maxQuery = DetachedCriteria.forClass(RealtimeTruckKpiSerie.class)
                    .add(Restrictions.gt("timeref", new Timestamp(IntervalBoundaries.getStartOfDay(DateTime.now()).getMillis())))
                    .setProjection(Projections.max("timeref"));

            result = (RealtimeTruckKpiSerie) session.createCriteria(RealtimeTruckKpiSerie.class)
                    .add(Restrictions.eq("kpiid", kpiid))
                    .add(Restrictions.eq("macroflussoid", flusso))
                    .add(Property.forName("timeref").eq(maxQuery))
                    .uniqueResult();
            if (result == null) {
                return noVal;
            }
        } catch (Exception e) {
            LOGGER.error("Error getDailyValue " + e.getMessage());
            return noVal;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return result.getArrowevaluation();
    }

    private static int rowPosition(Timestamp timeref) {
        long startOfDay = IntervalBoundaries.getStartOfDay(new DateTime(timeref.getTime())).getMillis();
        long currentHalfHour = timeref.getTime();
        return (int) ((currentHalfHour / 1000) / 1800 - (startOfDay / 1000) / 1800); // +1; se vogliamo partire da [1] invece che [0]
    }
}
