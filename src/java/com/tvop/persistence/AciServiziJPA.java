package com.tvop.persistence;

import com.tvop.exceptions.DMLException;
import com.tvop.persistence.dbentities.AgentGroupColumn;
import com.tvop.persistence.dbentities.AgentGroupRow;
import com.tvop.persistence.dbentities.VAGColumn;
import com.tvop.persistence.dbentities.VAGRow;
import com.tvop.utils.HibernateUtil;
import com.tvop.utils.Utils;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javax.faces.context.FacesContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;

public class AciServiziJPA {

    static final transient Logger LOGGER = LogManager.getLogger(AciServiziJPA.class.getName());
    static final transient ResourceBundle BUNDLE = FacesContext.getCurrentInstance().getApplication().getResourceBundle(FacesContext.getCurrentInstance(), "txt");

    public static List<Object> getAllAG() throws DMLException {
        Session session = null;
        List<Object> rtAgentkpis = new ArrayList<>();
        try {
            session = HibernateUtil.getSession();
            rtAgentkpis = session.createQuery(
                    "SELECT kpi.value, r.ordering, c.ordering, r.agentgroupid, c.type, kpi.arrowevaluation "
                    + "FROM RealtimeAgentKpiSerie kpi, AgentGroupRow r, AgentGroupColumn c "
                    + "WHERE kpi.kpiid = c.kpiid "
                    + "AND kpi.agentgroupid = r.agentgroupid "
                    + "AND kpi.timeref = (SELECT MAX(timeref) "
                    + "FROM TimerefRtAgent)")
                    .list();
            if (rtAgentkpis == null) {
                throw new DMLException("real time AG not found");
            }
        } catch (DMLException e) {
            LOGGER.error("Error getting real time AG " + e.getMessage());
            throw new DMLException(BUNDLE.getString("message.COTColserror") + " " + e.toString());
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return rtAgentkpis;
    }

    public static List<AgentGroupColumn> getColsAG() throws DMLException {
        Session session = null;
        List<AgentGroupColumn> cols = new ArrayList<>();
        try {
            session = HibernateUtil.getSession();
            cols = session.createCriteria(AgentGroupColumn.class)
                    .addOrder(Order.asc("ordering")).list();
            if (cols == null) {
                throw new DMLException("column AG not found");
            }
        } catch (DMLException | HibernateException e) {
            LOGGER.error("Error getting column AG " + e.getMessage());
            throw new DMLException(BUNDLE.getString("message.COTColserror") + " " + e.toString());
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return cols;
    }

    public static List<AgentGroupRow> getRowsAG() throws DMLException {
        Session session = null;
        List<AgentGroupRow> rows = new ArrayList<>();
        try {
            session = HibernateUtil.getSession();
            rows = session.createCriteria(AgentGroupRow.class)
                    .addOrder(Order.asc("ordering")).list();
            if (rows == null) {
                throw new DMLException("column AG not found");
            }
        } catch (DMLException | HibernateException e) {
            LOGGER.error("Error getting column AG " + e.getMessage());
            throw new DMLException(BUNDLE.getString("message.COTColserror") + " " + e.toString());
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return rows;
    }
    
    public static List<Object> getAllVAG() throws DMLException {
        Session session = null;
        List<Object> rtAgentkpis = new ArrayList<>();        
        try {
            session = HibernateUtil.getSession();
            rtAgentkpis = session.createQuery(
                "SELECT kpi.value, r.ordering, c.ordering, r.rowgui, c.type, kpi.arrowevaluation " +
                "FROM RealtimeAgentKpiSerie kpi, VAGRow r, VAGColumn c " +
                "WHERE kpi.kpiid = c.kpiid " +
                "AND kpi.agentgroupid = r.agentgroupid " + 
                "AND kpi.timeref = (SELECT MAX(timeref) " + 
                                    "FROM TimerefRtAgent)")
                .list();            
            if (rtAgentkpis == null) {
                throw new DMLException("real time VAG not found");
            }
        } catch (DMLException e) {
            LOGGER.error("Error getting real time VAG " + e.getMessage());
            throw new DMLException(BUNDLE.getString("message.COTColserror") + " " + e.toString());
        }
        finally{
            if(session != null){
                session.close();
            }
        }        
        return rtAgentkpis;
    }
    
    public static List<VAGColumn> getColsVAG() throws DMLException {
        Session session = null;
        List<VAGColumn> cols = new ArrayList<>();        
        try {
            session = HibernateUtil.getSession();
            cols = session.createCriteria(VAGColumn.class)
                    .addOrder(Order.asc("ordering")).list();            
            if (cols == null) {
                throw new DMLException("column VAG not found");
            }
        } catch (DMLException | HibernateException e) {
            LOGGER.error("Error getting column VAG " + e.getMessage());
            throw new DMLException(BUNDLE.getString("message.COTColserror") + " " + e.toString());
        }
        finally{
            if(session != null){
                session.close();
            }
        }        
        return cols;
    }
    
    public static List<VAGRow> getRowsVAG() throws DMLException {
        Session session = null;
        List<VAGRow> rows = new ArrayList<>();        
        try {
            session = HibernateUtil.getSession();
            rows = session.createCriteria(VAGRow.class)
                    .addOrder(Order.asc("ordering")).list();            
            if (rows == null) {
                throw new DMLException("row VAG not found");
            }
        } catch (DMLException | HibernateException e) {
            LOGGER.error("Error getting row VAG " + e.getMessage());
            throw new DMLException(BUNDLE.getString("message.COTColserror") + " " + e.toString());
        }
        finally{
            if(session != null){
                session.close();
            }
        }        
        return rows;
    }
    
    public static List<Object> getAllTruck() throws DMLException {
        Session session = null;
        List<Object> rtAgentkpis = new ArrayList<>();
        try {
            session = HibernateUtil.getSession();
            rtAgentkpis = session.createQuery(
                    "SELECT kpi.value, r.ordering, c.ordering, r.label, c.type, kpi.arrowevaluation "
                    + "FROM RealtimeTruckKpiSerie kpi, CotServiziRow r, CotServiziColumn c "
                    + "WHERE kpi.kpiid = c.kpiid "
                    + "AND kpi.macroflussoid = r.macroflussoid "
                    + "AND kpi.timeref = (SELECT MAX(timeref) "
                    + "FROM TimerefRtTruck)").list();
            // ************* INIZIO PARTE DA TOGLIERE ***********************
            Object risposteSoci = (Object) session.createQuery(
                    "SELECT SUM(value) "
                    + "FROM RealtimeTruckKpiSerie "
                    + "WHERE kpiid = '21' "
                    + "AND macroflussoid IN ('SOCI_ACI_(ag_ldc)','OVERFLOW_SOCI') "
                    + "AND timeref = (SELECT MAX(timeref) FROM TimerefRtTruck)").uniqueResult();
            if (risposteSoci == null) {
                risposteSoci = 0;
            }
            Object offerteSoci = (Object) session.createQuery(
                    "SELECT value "
                    + "FROM RealtimeTruckKpiSerie "
                    + "WHERE kpiid = '1' "
                    + "AND macroflussoid = 'SOCI_ACI_(ag_ldc)' "
                    + "AND timeref = (SELECT MAX(timeref) FROM TimerefRtTruck)").uniqueResult();
            if (offerteSoci == null) {
                offerteSoci = 0;
            }
            Object risposteOverflow = (Object) session.createQuery(
                    "SELECT SUM(value) "
                    + "FROM RealtimeTruckKpiSerie "
                    + "WHERE kpiid = '21' "
                    + "AND macroflussoid = 'OVERFLOW_SOCI' "
                    + "AND timeref = (SELECT MAX(timeref) FROM TimerefRtTruck)").uniqueResult();
            if (risposteOverflow == null) {
                 risposteOverflow = 0;
            }
            Object rispMin20Soci = (Object) session.createQuery(
                    "SELECT SUM(value) "
                    + "FROM RealtimeTruckKpiSerie "
                    + "WHERE kpiid = '13' "
                    + "AND macroflussoid IN ('SOCI_ACI_(ag_ldc)','OVERFLOW_SOCI') "
                    + "AND timeref = (SELECT MAX(timeref) FROM TimerefRtTruck)").uniqueResult();
            if (rispMin20Soci == null) {
                rispMin20Soci = 0;
            }
            Object abbandonateSoci = (Object) session.createQuery(
                    "SELECT value "
                    + "FROM RealtimeTruckKpiSerie "
                    + "WHERE kpiid = '8' "
                    + "AND macroflussoid = 'SOCI_ACI_(ag_ldc)' "
                    + "AND timeref = (SELECT MAX(timeref) FROM TimerefRtTruck)").uniqueResult();
            if (abbandonateSoci == null) {
                abbandonateSoci = 0;
            }
            offerteSoci = (Object) (Double.valueOf(offerteSoci.toString()) + Double.valueOf(risposteOverflow.toString()));
            for (Object obj : rtAgentkpis) {
                Object[] valuePosition = (Object[]) obj;
                if (Integer.valueOf(valuePosition[1].toString()) == 20 && Integer.valueOf(valuePosition[2].toString()) == 1) {
                    
                    valuePosition[0] = offerteSoci;
                }
                if (Integer.valueOf(valuePosition[1].toString()) == 20 && Integer.valueOf(valuePosition[2].toString()) == 2) {
                    valuePosition[0] = risposteSoci;
                }
                if (Integer.valueOf(valuePosition[1].toString()) == 20 && Integer.valueOf(valuePosition[2].toString()) == 6) {
                    valuePosition[0] = rispMin20Soci;
                }
                if (Integer.valueOf(valuePosition[1].toString()) == 20 && Integer.valueOf(valuePosition[2].toString()) == 7 && Double.valueOf(risposteSoci.toString()) > 0) { // LDS
                    valuePosition[0] = (Object) ((Double.valueOf(rispMin20Soci.toString()) / Double.valueOf(risposteSoci.toString())) * 100);
                }
                if (Integer.valueOf(valuePosition[1].toString()) == 20 && Integer.valueOf(valuePosition[2].toString()) == 5 && Double.valueOf(offerteSoci.toString()) > 0) { // ACR
                    valuePosition[0] = (Object) ((Double.valueOf(abbandonateSoci.toString()) / Double.valueOf(offerteSoci.toString())) * 100);
                }
            }
            // ************* FINE PARTE DA TOGLIERE ***********************
            if (rtAgentkpis == null) {
                throw new DMLException("real time CotTruck not found");
            }
        } catch (DMLException | NumberFormatException e) {
            LOGGER.error("Error getting real time CotTruck " + e.getMessage());
            throw new DMLException(BUNDLE.getString("message.COTColserror") + " " + e.toString());
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return rtAgentkpis;
    }

    public static List<String> getColsTruck() throws DMLException {
        Session session = null;
        List<String> cols = new ArrayList<>();
        try {
            session = HibernateUtil.getSession();
            cols = session.createQuery(
                    "SELECT label "
                    + "FROM CotServiziColumn "
                    + "ORDER BY ordering ASC")
                    .list();
            if (cols == null) {
                throw new DMLException("column CotTruck not found");
            }
        } catch (DMLException e) {
            LOGGER.error("Error getting column CotServizi " + e.getMessage());
            throw new DMLException(BUNDLE.getString("message.COTColserror") + " " + e.toString());
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return cols;
    }

    public static List<String> getRowsTruck() throws DMLException {
        Session session = null;
        List<String> rows = new ArrayList<>();
        try {
            session = HibernateUtil.getSession();
            rows = session.createQuery(
                    "SELECT label "
                    + "FROM CotServiziRow "
                    + "ORDER BY ordering ASC")
                    .list();
            if (rows == null) {
                throw new DMLException("row CotServizi not found");
            }
        } catch (DMLException e) {
            LOGGER.error("Error getting row CotServizi " + e.getMessage());
            throw new DMLException(BUNDLE.getString("message.COTColserror") + " " + e.toString());
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return rows;
    }
}
