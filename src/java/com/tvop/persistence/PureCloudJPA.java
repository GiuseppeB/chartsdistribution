package com.tvop.persistence;

import com.tvop.utils.HibernateUtil;
import com.tvop.utils.Utils;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javax.faces.context.FacesContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;

public class PureCloudJPA {

    static final transient Logger LOGGER = LogManager.getLogger(PureCloudJPA.class.getName());
    static final transient ResourceBundle BUNDLE = FacesContext.getCurrentInstance().getApplication().getResourceBundle(FacesContext.getCurrentInstance(), "txt");

    public static List getAllByOrderingASC(Class c, String field) {
        Session session = null;
        List res = new ArrayList<>();

        try {
            session = HibernateUtil.getSession();

            res = session.createCriteria(c)
                    .addOrder(Order.asc(field))
                    .list();
        } catch (HibernateException e) {
            LOGGER.error("getAllByOrderingASC - " + e.getMessage());
            return new ArrayList<>();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return res;
    }

    public static List getPC_IvrOrdering(Class c, String field) {
        Session session = null;
        List res = new ArrayList<>();

        try {
            session = HibernateUtil.getSession();

            res = session.createCriteria(c)
                    .addOrder(Order.asc(field))
                    .list();
        } catch (HibernateException e) {
            LOGGER.error("getPC_IvrOrdering - " + e.getMessage());
            return new ArrayList<>();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return res;
    }
}
