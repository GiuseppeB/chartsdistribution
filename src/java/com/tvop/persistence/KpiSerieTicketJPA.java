package com.tvop.persistence;

import com.tvop.persistence.dbentities.KpiSerieTicket;
import com.tvop.utils.HibernateUtil;
import com.tvop.beans.charts.Chart3bccsi;
import com.tvop.beans.charts.Chart4bccsi;
import com.tvop.persistence.dbentities.KpiSerieCatalogazione;
import com.tvop.persistence.dbentities.KpiSerieRichieste;
import com.tvop.utils.IntervalBoundaries;
import com.tvop.utils.IntervalsBCCSI;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import javax.faces.context.FacesContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.joda.time.DateTime;

public class KpiSerieTicketJPA {

    static final transient Logger LOGGER = LogManager.getLogger(KpiSerieTicketJPA.class.getName());
    static final transient ResourceBundle BUNDLE = FacesContext.getCurrentInstance().getApplication().getResourceBundle(FacesContext.getCurrentInstance(), "txt");

    public static List<Chart3bccsi> getLastForAllTicket() {
        List<KpiSerieTicket> temp = null;
        List<Chart3bccsi> result = new ArrayList<>();
        double[][] values = new double[9][8];
        List<String> areas = new ArrayList<>();
        Chart3bccsi ticket = new Chart3bccsi();
        Session session = null;
        try {
            session = HibernateUtil.getSession();
            Timestamp ts = new Timestamp(IntervalBoundaries.getStartOfDay(new DateTime()).getMillis());
            temp = session.createCriteria(KpiSerieTicket.class)
                    .add(Restrictions.eq("timeref", ts))
                    .add(Restrictions.ne("kpiid", "57"))
                    .addOrder(Order.asc("kpiid"))
                    .addOrder(Order.asc("name")).list();
            int i = 0;
            for (KpiSerieTicket kpi : temp) {
                if (areas.size() < 7) {
                    areas.add(kpi.getName());
                }
                if (i > 6) {
                    i = 0;
                }
                switch (kpi.getKpiid()) {
                    case "50":
                        values[i][0] = kpi.getValue();
                        i++;
                        break;
                    case "51":
                        values[i][1] = kpi.getValue();
                        i++;
                        break;
                    case "52":
                        values[i][2] = kpi.getValue();
                        i++;
                        break;
                    case "53":
                        values[i][3] = kpi.getValue();
                        i++;
                        break;
                    case "60":
                        values[i][4] = kpi.getValue();
                        i++;
                        break;
                    case "54":
                        values[i][5] = kpi.getValue();
                        i++;
                        break;
                    case "55":
                        values[i][6] = kpi.getValue();
                        i++;
                        break;
                    case "56":
                        values[i][7] = kpi.getValue();
                        i++;
                        break;
                }
            }
            Chart3bccsi total = new Chart3bccsi("Totale", 0, 0, 0, 0, 0, 0, 0, 0);
            for (int r = 0; r < 7; r++) {
                ticket.setArea(areas.get(r));
                ticket.setTkPublisher((int) values[r][0]);
                total.setTkPublisher(total.getTkPublisher() + ticket.getTkPublisher());
                ticket.setTkNotAccepted((int) values[r][1]);
                total.setTkNotAccepted(total.getTkNotAccepted() + ticket.getTkNotAccepted());
                ticket.setPercTkNotAccepted(values[r][2]);
                ticket.setTkClosed((int) values[r][3]);
                total.setTkClosed(total.getTkClosed() + ticket.getTkClosed());
                ticket.setTkAccepted((int) values[r][4]);
                total.setTkAccepted(total.getTkAccepted() + ticket.getTkAccepted());
                ticket.setTkTecnicalIntervention((int) values[r][5]);
                total.setTkTecnicalIntervention(total.getTkTecnicalIntervention() + ticket.getTkTecnicalIntervention());
                ticket.setBacklogAssistance((int) values[r][6]);
                total.setBacklogAssistance(total.getBacklogAssistance() + ticket.getBacklogAssistance());
                ticket.setBacklogTerzeParti((int) values[r][7]);
                total.setBacklogTerzeParti(total.getBacklogTerzeParti() + ticket.getBacklogTerzeParti());
                result.add(ticket);
                ticket = new Chart3bccsi();
            }
            if (total.getTkNotAccepted() != 0 & total.getTkPublisher() != 0) {
                total.setPercTkNotAccepted(((double) total.getTkNotAccepted() / (double) total.getTkPublisher()) * 100);
            }
            result.add(total);
        } catch (HibernateException e) {
            LOGGER.error("Error getting getLastForAllTicket " + e.getMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return result;
    }

    public static List<Chart3bccsi> getYesterdayAllTicket() {
        List<KpiSerieTicket> temp = null;
        List<Chart3bccsi> result = new ArrayList<>();
        double[][] values = new double[9][8];
        List<String> areas = new ArrayList<>();
        Chart3bccsi ticket = new Chart3bccsi();
        Session session = null;
        try {
            session = HibernateUtil.getSession();
            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date());
            boolean monday = cal.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY;
            Timestamp ts = null;
            if (monday) {
                ts = new Timestamp(IntervalBoundaries.getStartOfDay(new DateTime().minusDays(3)).getMillis());
            } else {
                ts = new Timestamp(IntervalBoundaries.getStartOfDay(new DateTime().minusDays(1)).getMillis());
            }
            temp = session.createCriteria(KpiSerieTicket.class)
                    .add(Restrictions.eq("timeref", ts))
                    .add(Restrictions.ne("kpiid", "57"))
                    .addOrder(Order.asc("kpiid"))
                    .addOrder(Order.asc("name")).list();
            if (!temp.isEmpty()) {
                int i = 0;
                for (KpiSerieTicket kpi : temp) {
                    if (areas.size() < 7) {
                        areas.add(kpi.getName());
                    }
                    if (i > 6) {
                        i = 0;
                    }
                    switch (kpi.getKpiid()) {
                        case "50":
                            values[i][0] = kpi.getValue();
                            i++;
                            break;
                        case "51":
                            values[i][1] = kpi.getValue();
                            i++;
                            break;
                        case "52":
                            values[i][2] = kpi.getValue();
                            i++;
                            break;
                        case "53":
                            values[i][3] = kpi.getValue();
                            i++;
                            break;
                        case "60":
                            values[i][4] = kpi.getValue();
                            i++;
                            break;
                        case "54":
                            values[i][5] = kpi.getValue();
                            i++;
                            break;
                        case "55":
                            values[i][6] = kpi.getValue();
                            i++;
                            break;
                        case "56":
                            values[i][7] = kpi.getValue();
                            i++;
                            break;
                    }
                }
                Chart3bccsi total = new Chart3bccsi("Totale", 0, 0, 0, 0, 0, 0, 0, 0);
                for (int r = 0; r < 7; r++) {
                    ticket.setArea(areas.get(r));
                    ticket.setTkPublisher((int) values[r][0]);
                    total.setTkPublisher(total.getTkPublisher() + ticket.getTkPublisher());
                    ticket.setTkNotAccepted((int) values[r][1]);
                    total.setTkNotAccepted(total.getTkNotAccepted() + ticket.getTkNotAccepted());
                    ticket.setPercTkNotAccepted(values[r][2]);
                    ticket.setTkClosed((int) values[r][3]);
                    total.setTkClosed(total.getTkClosed() + ticket.getTkClosed());
                    ticket.setTkAccepted((int) values[r][4]);
                    total.setTkAccepted(total.getTkAccepted() + ticket.getTkAccepted());
                    ticket.setTkTecnicalIntervention((int) values[r][5]);
                    total.setTkTecnicalIntervention(total.getTkTecnicalIntervention() + ticket.getTkTecnicalIntervention());
                    ticket.setBacklogAssistance((int) values[r][6]);
                    total.setBacklogAssistance(total.getBacklogAssistance() + ticket.getBacklogAssistance());
                    ticket.setBacklogTerzeParti((int) values[r][7]);
                    total.setBacklogTerzeParti(total.getBacklogTerzeParti() + ticket.getBacklogTerzeParti());
                    result.add(ticket);
                    ticket = new Chart3bccsi();
                }
                if (total.getTkNotAccepted() != 0 & total.getTkPublisher() != 0) {
                    total.setPercTkNotAccepted(((double) total.getTkNotAccepted() / (double) total.getTkPublisher()) * 100);
                }
                result.add(total);
            }
        } catch (HibernateException e) {
            LOGGER.error("Error getting getLastForAllTicket " + e.getMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return result;
    }

    public static List<Chart4bccsi> getLastForChart4() {
        List<KpiSerieTicket> temp = null;
        List<Chart4bccsi> result = new ArrayList<>();
        double[][] values = new double[9][3];
        List<String> areas = new ArrayList<>();
        Chart4bccsi ticket = new Chart4bccsi();
        List<String> kpis = Arrays.asList("50", "55", "57");
        Session session = null;
        try {
            session = HibernateUtil.getSession();
            Timestamp ts = new Timestamp(IntervalBoundaries.getStartOfDay(new DateTime()).getMillis());
            temp = session.createCriteria(KpiSerieTicket.class)
                    .add(Restrictions.eq("timeref", ts))
                    .add(Restrictions.in("kpiid", kpis))
                    .addOrder(Order.asc("kpiid"))
                    .addOrder(Order.desc("name"))
                    .list();
            int i = 0;
            for (KpiSerieTicket kpi : temp) {
                if (areas.size() < 7 && !areas.contains(kpi.getName())) {
                    areas.add(kpi.getName());
                }
                if (i > 6) {
                    i = 0;
                }
                switch (kpi.getKpiid()) {
                    case "50":
                        values[i][0] = kpi.getValue();
                        i++;
                        break;
                    case "57":
                        values[i][1] = kpi.getValue();
                        i++;
                        break;
                    case "55":
                        values[i][2] = kpi.getValue();
                        i++;
                        break;
                }
            }
            for (int r = 0; r < 7; r++) {
                ticket.setArea(areas.get(r));
                ticket.setTkPubblicati((int) values[r][0]);
                ticket.setTkChiusi((int) values[r][1]);
                ticket.setTkInEssere((int) values[r][2]);
                result.add(ticket);
                ticket = new Chart4bccsi();
            }
        } catch (HibernateException e) {
            LOGGER.error("Error getting getLastForChart4 " + e.getMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return result;
    }

    public static List<KpiSerieRichieste> getAllByStepChart5(IntervalsBCCSI step) {
        List<KpiSerieRichieste> result = null;
        Session session = null;
        try {
            session = HibernateUtil.getSession();
            Timestamp ts = null;
            switch (step) {
                case DAY:
                    ts = new Timestamp(IntervalBoundaries.getStartOfDay(new DateTime()).getMillis());
                    break;
                case WEEK:
                    ts = new Timestamp(IntervalBoundaries.getStartOfWeek(new DateTime()).getMillis());
                    break;
                case MONTH:
                    ts = new Timestamp(IntervalBoundaries.getStartOfMonth(new DateTime()).getMillis());
                    break;
            }
            result = session.createCriteria(KpiSerieRichieste.class)
                    .add(Restrictions.eq("timeref", ts))
                    .add(Restrictions.eq("step", step.getValue())).list();
        } catch (HibernateException e) {
            LOGGER.error("Error getting getAllByStepChart5 " + e.getMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return result;
    }

    public static List<KpiSerieCatalogazione> getAllByStepChart6(IntervalsBCCSI step) {
        List<KpiSerieCatalogazione> result = null;
        Session session = null;
        try {
            session = HibernateUtil.getSession();
            Timestamp ts = null;
            switch (step) {
                case DAY:
                    ts = new Timestamp(IntervalBoundaries.getStartOfDay(new DateTime()).getMillis());
                    break;
                case WEEK:
                    ts = new Timestamp(IntervalBoundaries.getStartOfWeek(new DateTime()).getMillis());
                    break;
                case MONTH:
                    ts = new Timestamp(IntervalBoundaries.getStartOfMonth(new DateTime()).getMillis());
                    break;
            }
            result = session.createCriteria(KpiSerieCatalogazione.class)
                    .add(Restrictions.eq("timeref", ts))
                    .add(Restrictions.eq("step", step.getValue())).list();
        } catch (HibernateException e) {
            LOGGER.error("Error getting getAllByStepChart6 " + e.getMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return result;
    }

    public static double getPublischedTicket(IntervalsBCCSI step) {
        double result = 0;
        Session session = null;
        try {
            session = HibernateUtil.getSession();
            Timestamp ts = null;
            switch (step) {
                case DAY:
                    ts = new Timestamp(IntervalBoundaries.getStartOfDay(new DateTime()).getMillis());
                    break;
                case WEEK:
                    ts = new Timestamp(IntervalBoundaries.getStartOfWeek(new DateTime()).getMillis());
                    break;
                case MONTH:
                    ts = new Timestamp(IntervalBoundaries.getStartOfMonth(new DateTime()).getMillis());
                    break;
            }
            List<KpiSerieTicket> temp = session.createCriteria(KpiSerieTicket.class)
                    .add(Restrictions.ge("timeref", ts))
                    .add(Restrictions.eq("kpiid", "50")).list();
            for (KpiSerieTicket ticket : temp) {
                result += ticket.getValue();
            }
        } catch (HibernateException e) {
            LOGGER.error("Error getting getPublischedTicket " + e.getMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return result;
    }
    
    public static double getPublischedTicketVoice(IntervalsBCCSI step) {
        double result = 0;
        Session session = null;
        try {
            session = HibernateUtil.getSession();
            Timestamp ts = null;
            switch (step) {
                case DAY:
                    ts = new Timestamp(IntervalBoundaries.getStartOfDay(new DateTime()).getMillis());
                    break;
                case WEEK:
                    ts = new Timestamp(IntervalBoundaries.getStartOfWeek(new DateTime()).getMillis());
                    break;
                case MONTH:
                    ts = new Timestamp(IntervalBoundaries.getStartOfMonth(new DateTime()).getMillis());
                    break;
            }
            List<KpiSerieTicket> temp = session.createCriteria(KpiSerieTicket.class)
                    .add(Restrictions.ge("timeref", ts))
                    .add(Restrictions.eq("kpiid", "61")).list();
            for (KpiSerieTicket ticket : temp) {
                result += ticket.getValue();
            }
        } catch (HibernateException e) {
            LOGGER.error("Error getting getPublischedTicketVoice " + e.getMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return result;
    }
}
