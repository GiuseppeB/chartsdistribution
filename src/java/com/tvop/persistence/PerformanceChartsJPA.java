package com.tvop.persistence;

import com.tvop.utils.HibernateUtil;
import com.tvop.utils.IntervalBoundariesStatica;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javax.faces.context.FacesContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.joda.time.DateTime;

public class PerformanceChartsJPA {

    static final transient Logger LOGGER = LogManager.getLogger(PerformanceChartsJPA.class.getName());
    static final transient ResourceBundle BUNDLE = FacesContext.getCurrentInstance().getApplication().getResourceBundle(FacesContext.getCurrentInstance(), "txt");

    public static List getCols(Class c) {
        Session session = null;
        List res = new ArrayList<>();

        try {
            session = HibernateUtil.getSession();

            res = session.createCriteria(c)
                    .addOrder(Order.asc("ordering"))
                    .list();
        } catch (Exception e) {
            LOGGER.error("getCols - " + e.getMessage());
            return new ArrayList<>();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return res;
    }
    
    public static List getRows(Class c) {
        Session session = null;
        List res = new ArrayList<>();

        try {
            session = HibernateUtil.getSession();

            res = session.createCriteria(c)
                    .addOrder(Order.asc("ordering"))
                    .list();
        } catch (Exception e) {
            LOGGER.error("getRows - " + e.getMessage());
            return new ArrayList<>();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return res;
    }
    
    public static List<Object> getAll(String col, String row, boolean truck) {
        Session session = null;
        List<Object> result = new ArrayList<>();
        String table = "RealtimeAgentKpiSerie";
        if (truck) {
            table = "RealtimeTruckKpiSerie";
        }
        try {
            session = HibernateUtil.getSession();

            Timestamp ts = new Timestamp(IntervalBoundariesStatica.getStartOfDay(new DateTime()).getMillis());
            String query = "SELECT kpi.value, r.ordering, c.ordering, r.agentgroupid, c.type, kpi.arrowevaluation "
                    + "FROM RealtimeAgentKpiSerie kpi, " + row + " r, " + col + " c "
                    + "WHERE kpi.kpiid = c.kpiid "
                    + "AND kpi.agentgroupid = r.agentgroupid "
                    + "AND kpi.timeref = (SELECT MAX(timeref) "
                    + "FROM TimerefRtAgent "
                    + "WHERE timeref >= '" + ts + "')";
            if (truck) {
            query = "SELECT kpi.value, r.ordering, c.ordering, r.macroflussoid, c.type, kpi.arrowevaluation "
                    + "FROM RealtimeTruckKpiSerie kpi, " + row + " r, " + col + " c "
                    + "WHERE kpi.kpiid = c.kpiid "
                    + "AND kpi.macroflussoid = r.macroflussoid "
                    + "AND kpi.timeref = (SELECT MAX(timeref) "
                    + "FROM TimerefRtTruck "
                    + "WHERE timeref >= '" + ts + "')";
            }
            result = session.createQuery(query).list();
        } catch (HibernateException e) {
            LOGGER.error("getAll - " + e.getMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return result;
    }     
    
    /**
     * Get All Data from Table
     * @param c SimpleName of Class
     * @return data or empty list
     */
    public static List getAll(Class c) {
        Session session = null;
        List res = new ArrayList<>();

        try {
            session = HibernateUtil.getSession();

            res = session.createCriteria(c).list();
        } catch (Exception e) {
            LOGGER.error("getAll - " + e.getMessage());
            return new ArrayList<>();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return res;
    }
    
    /**
     * get all ordering data from table
     * @param c class
     * @param field name of ordering column
     * @return data or empty list
     */
    public static List getAllbyOrder(Class c, String field) {
        Session session = null;
        List res = new ArrayList<>();

        try {
            session = HibernateUtil.getSession();

            res = session.createCriteria(c)
                    .addOrder(Order.asc(field))
                    .list();
        } catch (Exception e) {
            LOGGER.error("getAllbyOrder - " + e.getMessage());
            return new ArrayList<>();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return res;
    }

    /**
     * ********************** NUOVE ***********************
     */
    /**
     * get all ordering data from table
     *
     * @param c class
     * @param field name of column
     * @param old try for yesterday datas
     * @return data or empty list
     */
    public static List getAllbyUsers(Class c, String field, boolean old) {
        Session session = null;
        List res = new ArrayList<>();

        try {
            session = HibernateUtil.getSession();
            Timestamp startDay = new Timestamp(IntervalBoundariesStatica.getStartOfDay(DateTime.now()).getMillis());
            Timestamp endDay = new Timestamp(IntervalBoundariesStatica.getEndOfDay(DateTime.now()).getMillis());
            if (old) {
                startDay = new Timestamp(IntervalBoundariesStatica.getStartOfDay(DateTime.now().minusDays(1)).getMillis());
                endDay = new Timestamp(IntervalBoundariesStatica.getEndOfDay(DateTime.now().minusDays(1)).getMillis());
            }
            String table = c.getSimpleName();
            String query 
                    = "SELECT K "
                    + "FROM " + table + " K "
                    + "WHERE K.key.timeref = (SELECT MAX(key.timeref) FROM " + table + " WHERE key.timeref >= :startDay AND key.timeref <= :endDay)";
            Query q = session.createQuery(query);
            q.setParameter("startDay", startDay);
            q.setParameter("endDay", endDay);
            res = q.list();
        } catch (Exception e) {
            LOGGER.error("getAllbyUsers - " + e.getMessage());
            return new ArrayList<>();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return res;
    }
    /**
     * get all ordering data from table
     *
     * @param c class
     * @param field name of column
     * @param old try for yesterday datas
     * @return data or empty list
     */
    public static List getAllbyQueues(Class c, String field, boolean old) {
        Session session = null;
        List res = new ArrayList<>();

        try {
            session = HibernateUtil.getSession();
            Timestamp startDay = new Timestamp(IntervalBoundariesStatica.getStartOfDay(DateTime.now()).getMillis());
            Timestamp endDay = new Timestamp(IntervalBoundariesStatica.getEndOfDay(DateTime.now()).getMillis());
            if (old) {
                startDay = new Timestamp(IntervalBoundariesStatica.getStartOfDay(DateTime.now().minusDays(1)).getMillis());
                endDay = new Timestamp(IntervalBoundariesStatica.getEndOfDay(DateTime.now().minusDays(1)).getMillis());
            }
            String table = c.getSimpleName();
            String query 
                    = "SELECT K "
                    + "FROM " + table + " K "
                    + "WHERE K.key.timeref = (SELECT MAX(key.timeref) FROM " + table + " WHERE key.timeref >= :startDay AND key.timeref <= :endDay)";
            Query q = session.createQuery(query);
            q.setParameter("startDay", startDay);
            q.setParameter("endDay", endDay);
            res = q.list();
        } catch (Exception e) {
            LOGGER.error("getAllbyQueues - " + e.getMessage());
            return new ArrayList<>();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return res;
    }
}
