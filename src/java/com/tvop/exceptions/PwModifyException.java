/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tvop.exceptions;

/**
 *
 * @author mauro
 */
public class PwModifyException extends Exception {
    
    public PwModifyException(String errmess){
        super(errmess);
    }
    
}
