package com.tvop.beans;

import com.tvop.persistence.PerformanceChartsJPA;
import com.tvop.persistence.ThresholdEvaluationJPA;
import com.tvop.persistence.dbentities.PC_RT_AgentDetails;
import com.tvop.persistence.dbentities.ThresholdEvaluationKpi;
import com.tvop.utils.Utils;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

@ManagedBean(name = "pC_RT_AgentDetailsView")
@RequestScoped
public class PC_RT_AgentDetailsView implements Serializable {

    static final transient Logger LOGGER = LogManager.getLogger(PC_RT_AgentDetailsView.class.getName());
    private List<PC_RT_AgentDetails> values;
    private List<PC_RT_AgentDetails> valuesOld;

    public PC_RT_AgentDetailsView() {
    }

    @PostConstruct
    public void init() {
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("schema", "iccrea_sinergia_b2b");
        values = PerformanceChartsJPA.getAllbyUsers(PC_RT_AgentDetails.class, "name", false);
        valuesOld = PerformanceChartsJPA.getAllbyUsers(PC_RT_AgentDetails.class, "name", true);
        values.add(0, getTotal(values));
        valuesOld.add(0, getTotal(valuesOld));
    }

    private PC_RT_AgentDetails getTotal(List<PC_RT_AgentDetails> list) {
        PC_RT_AgentDetails result = new PC_RT_AgentDetails();
        result.getKey().setId("Totale");
        result.setName("TOTALE");
        for (PC_RT_AgentDetails elem : list) {
            result.setTotlogin(result.getTotlogin() + elem.getTotlogin());
            result.setTotcoda(result.getTotcoda() + elem.getTotcoda());
            result.setTotfuoricoda(result.getTotfuoricoda() + elem.getTotfuoricoda());
            result.setTotconversazione(result.getTotconversazione() + elem.getTotconversazione());
            result.setRicevute(result.getRicevute() + elem.getRicevute());
            result.setGestite(result.getGestite() + elem.getGestite());
        }
        result.setTmconversazione(Math.round(divide(result.getTotconversazione(), result.getGestite())));
        return result;
    }

    private float divide(float a, int b) {
        if (b == 0) {
            return 0;
        }
        return (float) a / b;
    }

    public String thresholdEval(PC_RT_AgentDetails row, String kpiid) {
        try {
            PC_RT_AgentDetails old = new PC_RT_AgentDetails();
            for (PC_RT_AgentDetails tmp : valuesOld) {
                if (row.getKey().getId().equals(tmp.getKey().getId())) {
                    old = tmp;
                    break;
                }
            }
            ThresholdEvaluationKpi eval = ThresholdEvaluationJPA.getAllByKpi(kpiid);
            String newValue = "0";
            String oldValue = "0";
            switch (kpiid) {
                case "034": // Tot. Chiamate ricevute
                    newValue = String.valueOf(row.getTotlogin());
                    oldValue = String.valueOf(old.getTotlogin());
                    break;
                case "035": // Tot. Chiamate Gestite
                    newValue = String.valueOf(row.getTotcoda());
                    oldValue = String.valueOf(old.getTotcoda());
                    break;
                case "036": // Tot. Abbandonate
                    newValue = String.valueOf(row.getTotfuoricoda());
                    oldValue = String.valueOf(old.getTotfuoricoda());
                    break;
                case "037": // % abbandonate
                    newValue = String.valueOf(row.getTotconversazione());
                    oldValue = String.valueOf(old.getTotconversazione());
                    break;
                case "038": // Overflow Engineering
                    newValue = String.valueOf(row.getTmconversazione());
                    oldValue = String.valueOf(old.getTmconversazione());
                    break;
                case "039": // % LDS
                    newValue = String.valueOf(row.getGestite());
                    oldValue = String.valueOf(old.getGestite());
                    break;
                case "040": // Tempo medio attesa
                    newValue = String.valueOf(row.getRicevute());
                    oldValue = String.valueOf(old.getRicevute());
                    break;
            }
            return Utils.evaluationKpiWithValues(eval, newValue, oldValue);
        } catch (Exception e) {
            return "";
        }
    }

    public String tFormat(int val) {
        return Utils.tFormat(val);
    }

    public List<PC_RT_AgentDetails> getValues() {
        return values;
    }
}
