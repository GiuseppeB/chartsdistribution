package com.tvop.beans;

import com.tvop.exceptions.DMLException;
import com.tvop.persistence.PerformanceChartsJPA;
import com.tvop.persistence.RCIBanqueJPA;
import com.tvop.persistence.RealtimeAgentKpiSerieJPA;
import com.tvop.persistence.RealtimeCosanTruckKpiSerieJPA;
import com.tvop.persistence.dbentities.AgentGroupColumn;
import com.tvop.persistence.dbentities.AgentGroupRow;
import com.tvop.persistence.dbentities.CosanTruckRow;
import com.tvop.persistence.dbentities.Macroflusso;
import com.tvop.persistence.dbentities.Performance03Column;
import com.tvop.persistence.dbentities.Performance04Column;
import com.tvop.persistence.dbentities.Performance05Column;
import com.tvop.utils.IntervalBoundaries;
import com.tvop.utils.Utils;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;
import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.BarChartSeries;
import org.primefaces.model.chart.DateAxis;
import org.primefaces.model.chart.LineChartSeries;

@ManagedBean
@RequestScoped
public class RCIBanqueView implements Serializable {

    static final transient Logger LOGGER = LogManager.getLogger(RCIBanqueView.class.getName());
    private final int HALFHOURS = 48;
    private final String RISP = "02";
    private final String LDS = "08";
    private final String TDA = "07";
    private final String FLUSSO = "RCI_ALL";

    private BarChartModel[] barModelChart;
    private double[] answeredValues;
    private double[] ldsValues;
    private double[] tdaValues;
    private List<Macroflusso> trucksName;
    private String[] labelxAxisChartsDay;

    private List<Object> kpis;
    private Object[] valuePosition;
    private String value;
    private String arrow;
    private int col;
    private int row;
    private Object[][][] values;
    private List modelTableAG;
    private List<AgentGroupColumn> colsAG;
    private List<AgentGroupRow> rowsAG;
    private List modelTableFL;
    private List<String> colsFL;
    private List<String> rowsFL;
    private List modelTableRisp;
    private List<Performance03Column> colsRisp;
    private List<CosanTruckRow> rowsRisp;
    private List modelTableAbn;
    private List<Performance04Column> colsAbn;
    private List<CosanTruckRow> rowsAbn;
    private List modelTableLDS;
    private List<Performance05Column> colsLDS;
    private List<CosanTruckRow> rowsLDS;

    public RCIBanqueView() {
    }

    @PostConstruct
    public void init() {
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("schema", "rcibanque");
        try {
            modelTableAG = getAllAG();
            modelTableFL = getAllFL();
            modelTableRisp = getAllRisp();
            modelTableAbn = getAllAbn();
            modelTableLDS = getAllLDS();
            labelxAxisChartsDay = getXAxisChartsDay();
            trucksName = RCIBanqueJPA.getTrucksName();
            barModelChart = new BarChartModel[trucksName.size()];
            for (int i = 0; i < trucksName.size(); i++) {
                answeredValues = RCIBanqueJPA.getAll(RISP, trucksName.get(i).getMacroflussoid());
                ldsValues = RCIBanqueJPA.getAll(TDA, trucksName.get(i).getMacroflussoid());
                tdaValues = RCIBanqueJPA.getAll(LDS, trucksName.get(i).getMacroflussoid());
                barModelChart[i] = createBarModel();
            }
        } catch (DMLException e) {
            LOGGER.error("Error getting array list cot kpis " + e.getMessage());
        }
    }

    private List getAllAG() {
        try {
            colsAG = RealtimeAgentKpiSerieJPA.getCols();
            rowsAG = RealtimeAgentKpiSerieJPA.getRows();
            kpis = RealtimeAgentKpiSerieJPA.getAll();

            values = new Object[rowsAG.size()][colsAG.size() + 1][3];

            //popolo tutta la tabella di valori '0' e simboli '='
            for (int i = 0; i < rowsAG.size(); i++) {
                for (int j = 0; j < colsAG.size() + 1; j++) {
                    values[i][j][0] = 0;
                }
            }

            for (int i = 0; i < rowsAG.size(); i++) {
                values[i][0][0] = rowsAG.get(i).getLabel();
            }

            // Popolo tutta la tabella
            for (Object element : kpis) {
                valuePosition = (Object[]) element;
                value = Utils.convertValue(valuePosition[0], Integer.valueOf(valuePosition[4].toString()));
                arrow = Utils.evaluationKpi(valuePosition[5].toString());
                row = Integer.valueOf(valuePosition[1].toString());
                col = Integer.valueOf(valuePosition[2].toString());
                values[row - 1][col][0] = value;
                values[row - 1][col][1] = arrow;
            }

        } catch (Exception e) {
            LOGGER.error("Error getting array list cot kpis " + e.getMessage());
        }
        return Arrays.asList(values);
    }

    private List getAllFL() {
        try {
            colsFL = RealtimeCosanTruckKpiSerieJPA.getCols();
            rowsFL = RealtimeCosanTruckKpiSerieJPA.getRows();
            kpis = RealtimeCosanTruckKpiSerieJPA.getAll();
            values = new Object[rowsFL.size()][colsFL.size() + 1][2];

            //popolo tutta la tabella di 0
            for (int i = 0; i < rowsFL.size(); i++) {
                for (int j = 0; j < colsFL.size() + 1; j++) {
                    values[i][j][0] = 0;
                }
            }

            for (int i = 0; i < rowsFL.size(); i++) {
                values[i][0][0] = rowsFL.get(i);
            }

            for (Object element : kpis) {
                valuePosition = (Object[]) element;
                value = Utils.convertValue(valuePosition[0], Integer.valueOf(valuePosition[4].toString()));
                arrow = Utils.evaluationKpi(valuePosition[5].toString());
                row = Integer.valueOf(valuePosition[1].toString());
                col = Integer.valueOf(valuePosition[2].toString());
                values[row - 1][col][0] = value;
                values[row - 1][col][1] = arrow;
            }
        } catch (Exception e) {
            LOGGER.error("Error getting array list cot kpis " + e.getMessage());
        }
        return Arrays.asList(values);
    }

    private List getAllRisp() {
        try {
            colsRisp = PerformanceChartsJPA.getCols(Performance03Column.class);            
            rowsRisp = PerformanceChartsJPA.getRows(CosanTruckRow.class);
            kpis = PerformanceChartsJPA.getAll(Performance03Column.class.getSimpleName(), CosanTruckRow.class.getSimpleName(), true);
            values = new Object[rowsRisp.size()][colsRisp.size()+1][2];
            
            //popolo tutta la tabella di 0
            for(int i=0; i<rowsRisp.size(); i++){
                for(int j=0; j<colsRisp.size()+1; j++){
                    values[i][j][0] = 0;
                }
            }
            
            for(int i=0; i<rowsRisp.size(); i++){
                values[i][0][0] = rowsRisp.get(i).getLabel();
            }
                        
            for (Object element : kpis) {
                valuePosition = (Object[]) element;
                value = Utils.convertValue(valuePosition[0], Integer.valueOf(valuePosition[4].toString()));
                arrow = Utils.evaluationKpi(valuePosition[5].toString());
                row = Integer.valueOf(valuePosition[1].toString());
                col = Integer.valueOf(valuePosition[2].toString());
                values[row-1][col][0] = value;
                values[row-1][col][1] = arrow;
            }            
        } catch (Exception e) {
            LOGGER.error("Error getting array list cot kpis " + e.getMessage());
        }
        return Arrays.asList(values);
    }

    private List getAllAbn() {
        try {
            colsAbn = PerformanceChartsJPA.getCols(Performance04Column.class);            
            rowsAbn = PerformanceChartsJPA.getRows(CosanTruckRow.class);
            kpis = PerformanceChartsJPA.getAll(Performance04Column.class.getSimpleName(), CosanTruckRow.class.getSimpleName(), true);
            values = new Object[rowsAbn.size()][colsAbn.size()+1][2];
            
            //popolo tutta la tabella di 0
            for(int i=0; i<rowsAbn.size(); i++){
                for(int j=0; j<colsAbn.size()+1; j++){
                    values[i][j][0] = 0;
                }
            }
            
            for(int i=0; i<rowsAbn.size(); i++){
                values[i][0][0] = rowsAbn.get(i).getLabel();
            }
                        
            for (Object element : kpis) {
                valuePosition = (Object[]) element;
                value = Utils.convertValue(valuePosition[0], Integer.valueOf(valuePosition[4].toString()));
                arrow = Utils.evaluationKpi(valuePosition[5].toString());
                row = Integer.valueOf(valuePosition[1].toString());
                col = Integer.valueOf(valuePosition[2].toString());
                values[row-1][col][0] = value;
                values[row-1][col][1] = arrow;
            }            
        } catch (Exception e) {
            LOGGER.error("Error getting array list cot kpis " + e.getMessage());
        }
        return Arrays.asList(values);
    }

    private List getAllLDS() {
        try {
            colsLDS = PerformanceChartsJPA.getCols(Performance05Column.class);            
            rowsLDS = PerformanceChartsJPA.getRows(CosanTruckRow.class);
            kpis = PerformanceChartsJPA.getAll(Performance05Column.class.getSimpleName(), CosanTruckRow.class.getSimpleName(), true);
            values = new Object[rowsLDS.size()][colsLDS.size()+1][2];
            
            //popolo tutta la tabella di 0
            for(int i=0; i<rowsLDS.size(); i++){
                for(int j=0; j<colsLDS.size()+1; j++){
                    values[i][j][0] = 0;
                }
            }
            
            for(int i=0; i<rowsLDS.size(); i++){
                values[i][0][0] = rowsLDS.get(i).getLabel();
            }
                        
            for (Object element : kpis) {
                valuePosition = (Object[]) element;
                value = Utils.convertValue(valuePosition[0], Integer.valueOf(valuePosition[4].toString()));
                arrow = Utils.evaluationKpi(valuePosition[5].toString());
                row = Integer.valueOf(valuePosition[1].toString());
                col = Integer.valueOf(valuePosition[2].toString());
                values[row-1][col][0] = value;
                values[row-1][col][1] = arrow;
            }            
        } catch (Exception e) {
            LOGGER.error("Error getting array list cot kpis " + e.getMessage());
        }
        return Arrays.asList(values);
    }

    private BarChartModel createBarModel() {
        DateAxis xAxis = new DateAxis();
        Axis yAxis;
        xAxis.setTickFormat("%H:%M");
        xAxis.setMin("08:00");
        xAxis.setMax("20:00");
        xAxis.setTickCount(25);
        xAxis.setTickAngle(-50);
        BarChartModel barModel = initBarModel();
        barModel.setResetAxesOnResize(false);
        barModel.setSeriesColors("58BA27,FFCC33,F74A4A");
        barModel.setExtender("chartExtender");
        barModel.setAnimate(true);
        barModel.setBarWidth(4);
        barModel.getAxes().put(AxisType.X, xAxis);
        yAxis = barModel.getAxis(AxisType.Y);
        yAxis.setMin(0);
        return barModel;
    }

    private BarChartModel initBarModel() {
        BarChartModel model = new BarChartModel();
        LineChartSeries ldsSeries = new LineChartSeries();
        LineChartSeries tdaSeries = new LineChartSeries();
        BarChartSeries answeredSeries = new BarChartSeries();
        for (int i = 0; i < HALFHOURS; i++) {
            ldsSeries.set(labelxAxisChartsDay[i], ldsValues[i]);
            tdaSeries.set(labelxAxisChartsDay[i], tdaValues[i]);
            answeredSeries.set(labelxAxisChartsDay[i], answeredValues[i]);
        }

        ldsSeries.setShowMarker(false);
        tdaSeries.setShowMarker(false);

        ldsSeries.setSmoothLine(true);
        tdaSeries.setSmoothLine(true);

        model.addSeries(ldsSeries);
        model.addSeries(tdaSeries);
        model.addSeries(answeredSeries);
        return model;
    }

    private String[] getXAxisChartsDay() {
        String[] result = new String[HALFHOURS];
        DateTime dt = IntervalBoundaries.getStartOfDay(new DateTime());
        for (int i = 0; i < HALFHOURS; i++) {
            result[i] = dt.toString();
            dt = dt.plusMinutes(30);
        }
        return result;
    }

    public String checkDecimal(double val) {
        return Utils.getPercValue(val);
    }

    public String ldsColor(double val) {
        if (val >= 80) {
            return "#58BA27"; //verde
        }
        return "#F74A4A"; //rosso
    }

    public BarChartModel[] getBarModelChart() {
        return barModelChart;
    }

    public List<Macroflusso> getTrucksName() {
        return trucksName;
    }

    public List getModelTableAG() {
        return modelTableAG;
    }

    public List<AgentGroupColumn> getColsAG() {
        return colsAG;
    }

    public List getModelTableFL() {
        return modelTableFL;
    }

    public List<String> getColsFL() {
        return colsFL;
    }

    public List getModelTableRisp() {
        return modelTableRisp;
    }

    public List<Performance03Column> getColsRisp() {
        return colsRisp;
    }

    public List getModelTableAbn() {
        return modelTableAbn;
    }

    public List<Performance04Column> getColsAbn() {
        return colsAbn;
    }

    public List getModelTableLDS() {
        return modelTableLDS;
    }

    public List<Performance05Column> getColsLDS() {
        return colsLDS;
    }
}
