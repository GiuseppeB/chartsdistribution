package com.tvop.beans;

import com.tvop.persistence.PerformanceChartsJPA;
import com.tvop.persistence.PureCloudJPA;
import com.tvop.persistence.ThresholdEvaluationJPA;
import com.tvop.persistence.dbentities.PC_IVROrdering;
import com.tvop.persistence.dbentities.PC_RT_CallMeBack;
import com.tvop.persistence.dbentities.ThresholdEvaluationKpi;
import com.tvop.utils.Utils;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

@ManagedBean(name = "pC_RT_CallMeBackView")
@RequestScoped
public class PC_RT_CallMeBackView implements Serializable {

    static final transient Logger LOGGER = LogManager.getLogger(PC_RT_CallMeBackView.class.getName());
    private List<PC_RT_CallMeBack> values;
    private List<PC_RT_CallMeBack> valuesOld;

    public PC_RT_CallMeBackView() {
        values = new ArrayList<>();
        valuesOld = new ArrayList<>();
    }

    @PostConstruct
    public void init() {
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("schema", "iccrea_sinergia_b2b");
        values = order(PerformanceChartsJPA.getAllbyQueues(PC_RT_CallMeBack.class, "name", false));
        values.add(0, getTotal(values));
        valuesOld = order(PerformanceChartsJPA.getAllbyQueues(PC_RT_CallMeBack.class, "name", true));
        valuesOld.add(0, getTotal(valuesOld));
    }

    private List<PC_RT_CallMeBack> order(List<PC_RT_CallMeBack> list) {
        List<PC_IVROrdering> ivrOrdering = PureCloudJPA.getPC_IvrOrdering(PC_IVROrdering.class, "key.ordering");
        List<PC_RT_CallMeBack> result = new ArrayList<>();
        for (PC_IVROrdering ivr : ivrOrdering) {
            for (PC_RT_CallMeBack elem : list) {
                if (ivr.getKey().getCoda().equals(elem.getName())) {
                    elem.setName(elem.getName().replace(" - Callback", ""));
                    result.add(elem);
                    break;
                }
            }
        }
        return result;
    }

    private PC_RT_CallMeBack getTotal(List<PC_RT_CallMeBack> list) {
        PC_RT_CallMeBack result = new PC_RT_CallMeBack();
        result.getKey().setId("Totale");
        result.setName("TOTALE");
        for (PC_RT_CallMeBack elem : list) {
            result.setTotali(result.getTotali() + elem.getTotali());
            result.setGestite(result.getGestite() + elem.getGestite());
            result.setDagestire(result.getDagestire() + elem.getDagestire());
            result.setIngestione(result.getIngestione() + elem.getIngestione());
        }
        result.setPercgestite(Utils.divide((float) result.getGestite(), result.getTotali()) * 100);
        result.setPercdagestire(Utils.divide((float) result.getDagestire(), result.getTotali()) * 100);
        result.setPercingestione(Utils.divide((float) result.getPercingestione(), result.getDagestire()) * 100);
        return result;
    }

    public String thresholdEval(PC_RT_CallMeBack row, String kpiid) {
        try {
            PC_RT_CallMeBack old = new PC_RT_CallMeBack();
            for (PC_RT_CallMeBack tmp : valuesOld) {
                if (row.getKey().getId().equals(tmp.getKey().getId())) {
                    old = tmp;
                    break;
                }
            }
            ThresholdEvaluationKpi eval = ThresholdEvaluationJPA.getAllByKpi(kpiid);
            String newValue = "0";
            String oldValue = "0";
            switch (kpiid) {
                case "021": // Tot. Chiamate ricevute
                    newValue = String.valueOf(row.getTotali());
                    oldValue = String.valueOf(old.getTotali());
                    break;
                case "022": // Tot. Chiamate Gestite
                    newValue = String.valueOf(row.getGestite());
                    oldValue = String.valueOf(old.getGestite());
                    break;
                case "023": // Tot. Abbandonate
                    newValue = String.valueOf(row.getPercgestite());
                    oldValue = String.valueOf(old.getPercgestite());
                    break;
                case "024": // % abbandonate
                    newValue = String.valueOf(row.getDagestire());
                    oldValue = String.valueOf(old.getDagestire());
                    break;
                case "025": // Overflow Engineering
                    newValue = String.valueOf(row.getPercdagestire());
                    oldValue = String.valueOf(old.getPercdagestire());
                    break;
                case "026": // % LDS
                    newValue = String.valueOf(row.getIngestione());
                    oldValue = String.valueOf(old.getIngestione());
                    break;
                case "027": // Tempo medio attesa
                    newValue = String.valueOf(row.getPercingestione());
                    oldValue = String.valueOf(old.getPercingestione());
                    break;
            }
            return Utils.evaluationKpiWithValues(eval, newValue, oldValue);
        } catch (Exception e) {
            return "";
        }
    }

    public String tFormat(int val) {
        return Utils.tFormat(val);
    }

    public String getPerc(float val) {
        if (val == 0) {
            return "-";
        }
        return Utils.checkDecimal(val) + "%";
    }

    public List<PC_RT_CallMeBack> getValues() {
        return values;
    }
}
