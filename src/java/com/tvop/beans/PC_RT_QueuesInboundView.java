package com.tvop.beans;

import com.tvop.persistence.PerformanceChartsJPA;
import com.tvop.persistence.PureCloudJPA;
import com.tvop.persistence.ThresholdEvaluationJPA;
import com.tvop.persistence.dbentities.PC_IVROrdering;
import com.tvop.persistence.dbentities.PC_RT_Queues_Inbound;
import com.tvop.persistence.dbentities.ThresholdEvaluationKpi;
import com.tvop.utils.Utils;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

@ManagedBean(name = "pC_RT_QueuesInboundView")
@RequestScoped
public class PC_RT_QueuesInboundView implements Serializable {

    static final transient Logger LOGGER = LogManager.getLogger(PC_RT_QueuesInboundView.class.getName());
    private List<PC_RT_Queues_Inbound> values;
    private List<PC_RT_Queues_Inbound> valuesOld;

    public PC_RT_QueuesInboundView() {
        values = new ArrayList<>();
        valuesOld = new ArrayList<>();
    }

    @PostConstruct
    public void init() {
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("schema", "iccrea_sinergia_b2b");
        values = order(PerformanceChartsJPA.getAllbyQueues(PC_RT_Queues_Inbound.class, "name", false));
        values.add(0, getTotal(values));
        valuesOld = order(PerformanceChartsJPA.getAllbyQueues(PC_RT_Queues_Inbound.class, "name", true));
        valuesOld.add(0, getTotal(valuesOld));
    }

    private List<PC_RT_Queues_Inbound> order(List<PC_RT_Queues_Inbound> list) {
        List<PC_IVROrdering> ivrOrdering = PureCloudJPA.getPC_IvrOrdering(PC_IVROrdering.class, "key.ordering");
        List<PC_RT_Queues_Inbound> result = new ArrayList<>();
        for (PC_IVROrdering ivr : ivrOrdering) {
            for (PC_RT_Queues_Inbound elem : list) {
                if (ivr.getKey().getCoda().equals(elem.getName())) {
                    result.add(elem);
                    break;
                }
            }
        }
        return result;
    }

    private PC_RT_Queues_Inbound getTotal(List<PC_RT_Queues_Inbound> list) {
        PC_RT_Queues_Inbound result = new PC_RT_Queues_Inbound();
        result.getKey().setId("Totale");
        result.setName("TOTALE");
        float perAbb = 0;
        float perAbbPremat = 0;
        float lds = 0;
        int countTmRisp = 0;
        int countTmAtt = 0;
        int countTmAbb = 0;
        int countTmConv = 0;
        for (PC_RT_Queues_Inbound elem : list) {
            result.setRicevute(result.getRicevute() + elem.getRicevute());
            result.setGestite(result.getGestite() + elem.getGestite());
            result.setIngestione(result.getIngestione() + elem.getIngestione());
            result.setInattesa(result.getInattesa() + elem.getInattesa());
            result.setAcw(result.getAcw() + elem.getAcw());
            result.setAbbandonate(result.getAbbandonate() + elem.getAbbandonate());
            result.setDissuase(result.getDissuase() + elem.getDissuase());
            result.setAbbpremature(result.getAbbpremature() + elem.getAbbpremature());
            result.setOverflowout(result.getOverflowout() + elem.getOverflowout());
            perAbb += elem.getPercabbandonate() * elem.getRicevute();
            perAbbPremat += elem.getPercabbpremature() * elem.getRicevute();
            lds += elem.getLds() * elem.getGestite();
            result.setMaxattesa(Integer.max(result.getMaxattesa(), elem.getMaxattesa()));
            if (elem.getTmrisposta() > 0) {
                result.setTmrisposta(result.getTmrisposta() + elem.getTmrisposta());
                countTmRisp++;
            }
            if (elem.getTmattesa() > 0) {
                result.setTmattesa(result.getTmattesa() + elem.getTmattesa());
                countTmAtt++;
            }
            if (elem.getTmabbandono() > 0) {
                result.setTmabbandono(result.getTmabbandono() + elem.getTmabbandono());
                countTmAbb++;
            }
            if (elem.getTmconversazione() > 0) {
                result.setTmconversazione(result.getTmconversazione() + elem.getTmconversazione());
                countTmConv++;
            }
        }
        result.setPercabbandonate(Utils.divide(perAbb, result.getRicevute()));
        result.setPercabbpremature(Utils.divide(perAbbPremat, result.getRicevute()));
        result.setLds(Utils.divide(lds, result.getGestite()));
        result.setTmrisposta(Utils.divide(result.getTmrisposta(), countTmRisp));
        result.setTmattesa(Utils.divide(result.getTmattesa(), countTmAtt));
        result.setTmabbandono(Utils.divide(result.getTmabbandono(), countTmAbb));
        result.setTmconversazione(Utils.divide(result.getTmconversazione(), countTmConv));
        return result;
    }

    public String thresholdEval(PC_RT_Queues_Inbound row, String kpiid) {
        try {
            PC_RT_Queues_Inbound old = new PC_RT_Queues_Inbound();
            for (PC_RT_Queues_Inbound tmp : valuesOld) {
                if (row.getKey().getId().equals(tmp.getKey().getId())) {
                    old = tmp;
                    break;
                }
            }
            ThresholdEvaluationKpi eval = ThresholdEvaluationJPA.getAllByKpi(kpiid);
            String newValue = "0";
            String oldValue = "0";
            switch (kpiid) {
                case "001": 
                    newValue = String.valueOf(row.getRicevute());
                    oldValue = String.valueOf(old.getRicevute());
                    break;
                case "021": 
                    newValue = String.valueOf(row.getAcw());
                    oldValue = String.valueOf(old.getAcw());
                    break;
                case "002": // Tot. Chiamate Gestite
                    newValue = String.valueOf(row.getGestite());
                    oldValue = String.valueOf(old.getGestite());
                    break;
                case "003": // Tot. Abbandonate
                    newValue = String.valueOf(row.getIngestione());
                    oldValue = String.valueOf(old.getIngestione());
                    break;
                case "004": // % abbandonate
                    newValue = String.valueOf(row.getInattesa());
                    oldValue = String.valueOf(old.getInattesa());
                    break;
                case "006": // % LDS
                    newValue = String.valueOf(row.getAbbandonate());
                    oldValue = String.valueOf(old.getAbbandonate());
                    break;
                case "007": // Tempo medio attesa
                    newValue = String.valueOf(row.getPercabbandonate());
                    oldValue = String.valueOf(old.getPercabbandonate());
                    break;
                case "008": // Tempo medio attesa
                    newValue = String.valueOf(row.getDissuase());
                    oldValue = String.valueOf(old.getDissuase());
                    break;
                case "009": // Tempo medio attesa
                    newValue = String.valueOf(row.getAbbpremature());
                    oldValue = String.valueOf(old.getAbbpremature());
                    break;
                case "010": // Tempo medio attesa
                    newValue = String.valueOf(row.getPercabbpremature());
                    oldValue = String.valueOf(old.getPercabbpremature());
                    break;
                case "011": // Tempo medio attesa
                    newValue = String.valueOf(row.getMaxattesa());
                    oldValue = String.valueOf(old.getMaxattesa());
                    break;
                case "012": // Tempo medio attesa
                    newValue = String.valueOf(row.getTmrisposta());
                    oldValue = String.valueOf(old.getTmrisposta());
                    break;
                case "014": // Tempo medio attesa
                    newValue = String.valueOf(row.getTmattesa());
                    oldValue = String.valueOf(old.getTmattesa());
                    break;
                case "015": // Tempo medio attesa
                    newValue = String.valueOf(row.getTmabbandono());
                    oldValue = String.valueOf(old.getTmabbandono());
                    break;
                case "016": // Tempo medio attesa
                    newValue = String.valueOf(row.getTmconversazione());
                    oldValue = String.valueOf(old.getTmconversazione());
                    break;
                case "017": // Tempo medio attesa
                    newValue = String.valueOf(row.getLds());
                    oldValue = String.valueOf(old.getLds());
                    break;
            }
            return Utils.evaluationKpiWithValues(eval, newValue, oldValue);
        } catch (Exception e) {
            return "";
        }
    }

    public String tFormat(int val) {
        return Utils.tFormat(val);
    }

    public String getPerc(float val) {
        if (val == 0) {
            return "-";
        }
        return Utils.checkDecimal(val) + "%";
    }

    public String getPercLDS(PC_RT_Queues_Inbound val) {
        if (val.getGestite() == 0) {
            return "-";
        }
        return Utils.checkDecimal(val.getLds()) + "%";
    }

    public List<PC_RT_Queues_Inbound> getValues() {
        return values;
    }
}
