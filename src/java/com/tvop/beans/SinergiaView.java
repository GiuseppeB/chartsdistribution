package com.tvop.beans;

import com.tvop.exceptions.DMLException;
import com.tvop.persistence.KpiRealtimeJPA;
import com.tvop.persistence.ThresholdEvaluationJPA;
import com.tvop.persistence.dbentities.KpiRealtimeChart1Sinergia;
import com.tvop.persistence.dbentities.KpiRealtimeChart2Sinergia;
import com.tvop.persistence.dbentities.OrderingAreaSinergia;
import com.tvop.persistence.dbentities.ThresholdEvaluationKpi;
import com.tvop.utils.Utils;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

@ManagedBean
@RequestScoped
public class SinergiaView implements Serializable {

    static final transient Logger LOGGER = LogManager.getLogger(SinergiaView.class.getName());
    private List<KpiRealtimeChart2Sinergia> values;
    private List<KpiRealtimeChart2Sinergia> valuesOld;

    public SinergiaView() {
    }

    @PostConstruct
    public void init() {
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("schema", "iccrea_sinergia");
        values = orderingList(KpiRealtimeJPA.getAllChart2RTsinergia(false));
        valuesOld = orderingList(KpiRealtimeJPA.getAllChart2RTsinergia(true));
        values.add(0, getTotal(values));
        valuesOld.add(0, getTotal(valuesOld));
    }

    private List<KpiRealtimeChart2Sinergia> orderingList(List<KpiRealtimeChart2Sinergia> list) {
        List<KpiRealtimeChart2Sinergia> result = new ArrayList<>();
        List<OrderingAreaSinergia> orderingList = KpiRealtimeJPA.getOrderingArea();
        for (OrderingAreaSinergia elem1 : orderingList) {
            for (KpiRealtimeChart2Sinergia elem2 : list) {
                if (elem1.getName().equals(elem2.getNome())) {
                    result.add(elem2);
                }
            }
        }
        return result;
    }

    private KpiRealtimeChart2Sinergia getTotal(List<KpiRealtimeChart2Sinergia> list) {
        float totAbb = 0;
        int countAbb = 0;
        float totLDS = 0;
        int countLDS = 0;
        int totTmAttesa = 0;
        int countTmAttesa = 0;
        int totTmRisp = 0;
        int countTmRisp = 0;
        int totTmAbb = 0;
        int countTmAbb = 0;
        KpiRealtimeChart2Sinergia result = new KpiRealtimeChart2Sinergia();
        result.setId("TOTALE");
        result.setNome("TOTALE");
        for (KpiRealtimeChart2Sinergia elem : list) {
            result.setTotricevute(result.getTotricevute() + elem.getTotricevute());
            result.setTotingestione(result.getTotingestione() + elem.getTotingestione());
            result.setTotgestite(result.getTotgestite() + elem.getTotgestite());
            result.setTotinattesa(result.getTotinattesa() + elem.getTotinattesa());
            result.setTotabb(result.getTotabb() + elem.getTotabb());
            if (elem.getPercabb() > 0) {
                countAbb++;
                totAbb += elem.getPercabb();
            }
            result.setOverflow(result.getOverflow() + elem.getOverflow());
            if (elem.getPerclds() > 0) {
                countLDS++;
                totLDS += elem.getPerclds();
            }
            result.setCallbackingestione(result.getCallbackingestione() + elem.getCallbackingestione());
            if (Double.valueOf(elem.getTmpmedioattesa()) > 0) {
                countTmAttesa++;
                totTmAttesa += Double.valueOf(elem.getTmpmedioattesa()).intValue();
            }
            if (Double.valueOf(elem.getTmpmediorisposta()) > 0) {
                countTmRisp++;
                totTmRisp += Double.valueOf(elem.getTmpmediorisposta()).intValue();
            }
            if (Double.valueOf(elem.getTmpmedioabb()) > 0) {
                countTmAbb++;
                totTmAbb += Double.valueOf(elem.getTmpmedioabb()).intValue();
            }
            result.setTmpmaxattesa(String.valueOf(Double.max(Double.valueOf(result.getTmpmaxattesa()), Double.valueOf(elem.getTmpmaxattesa()))));
            result.setTkaperti(result.getTkaperti() + elem.getTkaperti());
            result.setTkgestiti(result.getTkgestiti() + elem.getTkgestiti());
        }
        result.setPercabb(divide(totAbb, countAbb));
        result.setPerclds(divide(totLDS, countLDS));
        result.setTmpmedioattesa(String.valueOf(divide(totTmAttesa, countTmAttesa)));
        result.setTmpmediorisposta(String.valueOf(divide(totTmRisp, countTmRisp)));
        result.setTmpmedioabb(String.valueOf(divide(totTmAbb, countTmAbb)));
        return result;
    }

    private float divide(float a, int b) {
        if (b == 0) {
            return 0;
        }
        return a / b;
    }

    /**
     * @param row
     * @param kpiid 01=off;02=risp;03=abn;04=%rispSuTot;05=%rispMin40
     * @return path del simbolo/freccia
     */
    public String thresholdEval(KpiRealtimeChart2Sinergia row, String kpiid) throws DMLException {
        String result = "";
        KpiRealtimeChart2Sinergia old = null;
        for (KpiRealtimeChart2Sinergia tmp : valuesOld) {
            if (row.getId().equals(tmp.getId())) {
                old = tmp;
            }
        }
        ThresholdEvaluationKpi eval = ThresholdEvaluationJPA.getAllByKpi(kpiid);
        String newValue = "0";
        String oldValue = "0";
        if (old != null) {
            switch (kpiid) {
                case "55": // Tot. Chiamate ricevute
                    newValue = String.valueOf(row.getTotricevute());
                    oldValue = String.valueOf(old.getTotricevute());
                    break;
                case "59": // Tot. Chiamate Gestite
                    newValue = String.valueOf(row.getTotgestite());
                    oldValue = String.valueOf(old.getTotgestite());
                    break;
                case "02": // Tot. Abbandonate
                    newValue = String.valueOf(row.getTotabb());
                    oldValue = String.valueOf(old.getTotabb());
                    break;
                case "04": // % abbandonate
                    newValue = String.valueOf(row.getPercabb());
                    oldValue = String.valueOf(old.getPercabb());
                    break;
                case "05": // Overflow Engineering
                    newValue = String.valueOf(row.getOverflow());
                    oldValue = String.valueOf(old.getOverflow());
                    break;
                case "01": // % LDS
                    newValue = String.valueOf(row.getPerclds());
                    oldValue = String.valueOf(old.getPerclds());
                    break;
                case "60": // Tempo medio attesa
                    newValue = String.valueOf(row.getTmpmedioattesa());
                    oldValue = String.valueOf(old.getTmpmedioattesa());
                    break;
                case "53": // Tempo medio risposta
                    newValue = String.valueOf(row.getTmpmediorisposta());
                    oldValue = String.valueOf(old.getTmpmediorisposta());
                    break;
                case "57": // Tempo medio abbandono
                    newValue = String.valueOf(row.getTmpmedioabb());
                    oldValue = String.valueOf(old.getTmpmedioabb());
                    break;
                case "54": // Tempo max attesa
                    newValue = String.valueOf(row.getTmpmaxattesa());
                    oldValue = String.valueOf(old.getTmpmaxattesa());
                    break;
                case "52": // Tk Aperti
                    newValue = String.valueOf(row.getTkaperti());
                    oldValue = String.valueOf(old.getTkaperti());
                    break;
                case "51": // Tk Gestiti
                    newValue = String.valueOf(row.getTkgestiti());
                    oldValue = String.valueOf(old.getTkgestiti());
                    break;
            }
        }
        return Utils.evaluationKpiWithValues(eval, newValue, oldValue);
    }

    public String tFormat(String time) {
        double val = Double.valueOf(time);
        String res = Utils.tFormat(val / 1000);
        return res;
    }

    public String getPercentageFormat(float val) {
        return com.tvop.utils.Utils.getPercValue(val);
    }

    public List<KpiRealtimeChart2Sinergia> getValues() {
        return values;
    }
}
