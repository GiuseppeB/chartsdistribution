package com.tvop.beans;

import com.tvop.exceptions.DMLException;
import com.tvop.persistence.AciInHouseJPA;
import com.tvop.persistence.AciServiziJPA;
import com.tvop.persistence.dbentities.AgentGroupColumn;
import com.tvop.persistence.dbentities.AgentGroupRow;
import com.tvop.utils.IntervalBoundaries;
import com.tvop.utils.Utils;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import org.joda.time.DateTime;
import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.DateAxis;
import org.primefaces.model.chart.LineChartSeries;
import org.primefaces.model.chart.PieChartModel;

@ManagedBean
@RequestScoped
public class AciInHouseAllView implements Serializable {

    private ResourceBundle bundle;
    private final int INTERVAL = 48;
    private final String OFFERTE = "1";
    private final String RISPOSTE = "21";
    private final String LDS_STD = "25";
    private final String ACR_STD = "29";
    private final String SOCI = "SOCI_ACI";
    private final String OVERFLOW = "OVERFLOW_SOCI";
    private final String INHOUSE = "ACI_SOCI";
    private final String RISPOSTE_SERVIZI = "51";
    private final String RISPOSTE_LDC = "52";
    private final String ABBANDONATE_SERVIZI = "53";
    private final String ABBANDONATE_LDC = "54";

    private List<Object> objects;
    private Object[] valuePosition;
    private String value;
    private String arrow;
    private int col;
    private int row;
    private Object[][][] values;
    private List modelTableAG;
    private List<AgentGroupColumn> colsAG;
    private List<AgentGroupRow> rowsAG;
    private List modelTableTruckCot;
    private List<String> colsTruckCot;
    private List<String> rowsTruckCot;
    private List modelTableTruckCos;
    private List<String> colsTruckCos;
    private List<String> rowsTruckCos;
    private BarChartModel barModel;
    private String[] labelxAxisCharts;
    private double[] barChartsSoci;
    private double[] barChartsOverServiziRisp;
    private double[] barChartsOverServiziAbn;
    private double[] barChartsOverLdcRisp;    
    private double[] barChartsOverLdcAbn; 
    private double[] lineChartLDS;
    private double[] lineChartACR;
    private PieChartModel pieModelOffered;
    private double offerteSoci;
    private PieChartModel pieModelAnswered;
    private double rispSoci;
    private double rispOverServizi;
    private double rispOverLDC;
    private double abnOverServizi;
    private double abnOverLDC;

    static final transient Logger LOGGER = LogManager.getLogger(AciInHouseAllView.class.getName());

    public AciInHouseAllView() {
        FacesContext context = FacesContext.getCurrentInstance();
        bundle = context.getApplication().getResourceBundle(context, "txt");
        colsAG = new ArrayList<>();
        rowsAG = new ArrayList<>();
    }

    @PostConstruct
    public void init() {
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("schema", "aci_inhouse");
        modelTableAG = getAG();
        modelTableTruckCot = getTruckCot();
        modelTableTruckCos = getTruckCos();
        getBarChart();
        getPieChart();
    }

    private List getAG() {
        try {
            colsAG = AciInHouseJPA.getColsAG();
            rowsAG = AciInHouseJPA.getRowsAG();
            objects = AciInHouseJPA.getAllAG();
            values = new Object[rowsAG.size()][colsAG.size() + 1][2];
            for (int i = 0; i < rowsAG.size(); i++) {
                for (int j = 0; j < colsAG.size() + 1; j++) {
                    values[i][j][0] = 0;
                }
            }
            for (int i = 0; i < rowsAG.size(); i++) {
                values[i][0][0] = rowsAG.get(i).getLabel();
            }
            // Popolo tutta la tabella
            for (Object element : objects) {
                valuePosition = (Object[]) element;
                value = Utils.convertValue(valuePosition[0], Integer.valueOf(valuePosition[4].toString()));
                arrow = Utils.evaluationKpi(valuePosition[5].toString());
                row = Integer.valueOf(valuePosition[1].toString());
                col = Integer.valueOf(valuePosition[2].toString());
                values[row - 1][col][0] = value;
                values[row - 1][col][1] = arrow;
            }
        } catch (DMLException | NumberFormatException e) {
            LOGGER.error("Error getting array list cot kpis " + e.getMessage());
        }
        return Arrays.asList(values);
    }

    private List getTruckCot() {
        try {
            colsTruckCot = AciInHouseJPA.getColsTruckCot();
            rowsTruckCot = AciInHouseJPA.getRowsTruckCot();
            objects = AciInHouseJPA.getAllTruckCot();
            values = new Object[rowsTruckCot.size()][colsTruckCot.size() + 1][2];
            for (int i = 0; i < rowsTruckCot.size(); i++) {
                for (int j = 0; j < colsTruckCot.size() + 1; j++) {
                    values[i][j][0] = 0;
                }
            }
            for (int i = 0; i < rowsTruckCot.size(); i++) {
                values[i][0][0] = rowsTruckCot.get(i);
            }
            for (Object element : objects) {
                valuePosition = (Object[]) element;
                value = Utils.convertValue(valuePosition[0], Integer.valueOf(valuePosition[4].toString()));
                arrow = Utils.evaluationKpi(valuePosition[5].toString());
                row = Integer.valueOf(valuePosition[1].toString());
                col = Integer.valueOf(valuePosition[2].toString());
                values[row - 1][col][0] = value;
                values[row - 1][col][1] = arrow;
            }
        } catch (DMLException | NumberFormatException e) {
            LOGGER.error("Error getting array list cot kpis " + e.getMessage());
        }
        return Arrays.asList(values);
    }

    private List getTruckCos() {
        try {
            colsTruckCos = AciInHouseJPA.getColsTruckCos();
            rowsTruckCos = AciInHouseJPA.getRowsTruckCos();
            objects = AciInHouseJPA.getAllTruckCos();
            values = new Object[rowsTruckCos.size()][colsTruckCos.size() + 1][2];
            for (int i = 0; i < rowsTruckCos.size(); i++) {
                for (int j = 0; j < colsTruckCos.size() + 1; j++) {
                    values[i][j][0] = 0;
                }
            }
            for (int i = 0; i < rowsTruckCos.size(); i++) {
                values[i][0][0] = rowsTruckCos.get(i);
            }
            for (Object element : objects) {
                valuePosition = (Object[]) element;
                value = Utils.convertValue(valuePosition[0], Integer.valueOf(valuePosition[4].toString()));
                arrow = Utils.evaluationKpi(valuePosition[5].toString());
                row = Integer.valueOf(valuePosition[1].toString());
                col = Integer.valueOf(valuePosition[2].toString());
                values[row - 1][col][0] = value;
                values[row - 1][col][1] = arrow;
            }
        } catch (DMLException | NumberFormatException e) {
            LOGGER.error("Error getting array list cot kpis " + e.getMessage());
        }
        return Arrays.asList(values);
    }

    private void getBarChart() {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd mm:ss");
            DateTime dt = IntervalBoundaries.getStartOfDay(new DateTime());
            String today = sdf.format(new Date(dt.getMillis()));
            labelxAxisCharts = getXAxisCharts();
            barChartsSoci = AciInHouseJPA.getBarValues(OFFERTE, SOCI, today, INTERVAL, true);
            barChartsOverServiziRisp = AciInHouseJPA.getBarValues(RISPOSTE_SERVIZI, OVERFLOW, today, INTERVAL, true);
            barChartsOverServiziAbn = AciInHouseJPA.getBarValues(ABBANDONATE_SERVIZI, OVERFLOW, today, INTERVAL, true);
            barChartsOverLdcRisp = AciInHouseJPA.getBarValues(RISPOSTE_LDC, OVERFLOW, today, INTERVAL, true);
            barChartsOverLdcAbn = AciInHouseJPA.getBarValues(ABBANDONATE_LDC, OVERFLOW, today, INTERVAL, true);
            lineChartLDS = AciInHouseJPA.getLineValues(LDS_STD, SOCI, today, INTERVAL, true);
            lineChartACR = AciInHouseJPA.getLineValues(ACR_STD, SOCI, today, INTERVAL, true);
        } catch (DMLException e) {
            LOGGER.error("OfferedTrabocco init: " + e.getMessage());
        }
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        DateTime dt = IntervalBoundaries.getStartOfHalfH(new DateTime());
        String min = sdf.format(new Date(dt.minusHours(7).plusMinutes(30).getMillis()));
        String max = sdf.format(new Date(dt.plusMinutes(30).getMillis()));
        DateAxis xAxis = new DateAxis();
        xAxis.setTickAngle(-50);
        xAxis.setTickFormat("%H:%M");
        xAxis.setMin(min);
        xAxis.setMax(max);
        xAxis.setTickCount(15);
        barModel = new BarChartModel();
        LineChartSeries lds = new LineChartSeries();
        for (int i = 0; i < INTERVAL; i++) {
            lds.set(labelxAxisCharts[i], lineChartLDS[i]);
        }
        lds.setShowMarker(false);
        lds.setSmoothLine(true);
        LineChartSeries acr = new LineChartSeries();
        for (int i = 0; i < INTERVAL; i++) {
            acr.set(labelxAxisCharts[i], lineChartACR[i]);
        }
        acr.setShowMarker(false);
        acr.setSmoothLine(true);
        ChartSeries chartSeriesSoci = new ChartSeries();
        for (int i = 0; i < INTERVAL; i++) {
            chartSeriesSoci.set(labelxAxisCharts[i], barChartsSoci[i]);
        }
        ChartSeries chartSeriesOverServizi = new ChartSeries();
        for (int i = 0; i < INTERVAL; i++) {
            chartSeriesOverServizi.set(labelxAxisCharts[i], barChartsOverServiziRisp[i]+barChartsOverServiziAbn[i]);
        }
        ChartSeries chartSeriesOverLDC = new ChartSeries();
        for (int i = 0; i < INTERVAL; i++) {
            chartSeriesOverLDC.set(labelxAxisCharts[i], barChartsOverLdcRisp[i]+barChartsOverLdcAbn[i]);
        }
        barModel.addSeries(chartSeriesSoci);
        barModel.addSeries(chartSeriesOverServizi);
        barModel.addSeries(chartSeriesOverLDC);
        barModel.addSeries(lds);
        barModel.addSeries(acr);
        barModel.setBarWidth(10);
//        barModel.setSeriesColors("58BA27,1300DA");
        barModel.setSeriesColors("FFCC33,F74A4A,666,58BA27,1300DA");
        barModel.setExtender("chartExtender");
        barModel.setAnimate(true);
        barModel.setTitle(bundle.getString("uilabel.overflowBar"));
        barModel.getAxes().put(AxisType.X, xAxis);
        Axis yAxis = barModel.getAxis(AxisType.Y);
        yAxis.setMin(0);
    }

    private String[] getXAxisCharts() {
        String[] result = new String[INTERVAL];
        DateTime dt = IntervalBoundaries.getStartOfDay(new DateTime()).plusMinutes(30);
        for (int i = 0; i < INTERVAL; i++) {
            result[i] = dt.toString();
            dt = dt.plusMinutes(30);
        }
        return result;
    }

    private void getPieChart() {
        try {
            offerteSoci = AciInHouseJPA.getPieValue(OFFERTE, SOCI, true);
            rispSoci = AciInHouseJPA.getPieValue(RISPOSTE, SOCI, true);
            rispOverServizi = AciInHouseJPA.getPieValue(RISPOSTE_SERVIZI, SOCI, true);
            rispOverLDC = AciInHouseJPA.getPieValue(RISPOSTE_LDC, SOCI, true);
            abnOverServizi = AciInHouseJPA.getPieValue(ABBANDONATE_SERVIZI, SOCI, true);
            abnOverLDC = AciInHouseJPA.getPieValue(ABBANDONATE_LDC, SOCI, true);
        } catch (DMLException e) {
            LOGGER.error("OverflowPieChart init: " + e.getMessage());
        }
        pieModelOffered = new PieChartModel();
        double totaleOfferte = offerteSoci;
        pieModelOffered.set("TOTALE OFFERTE SOCI: " + totaleOfferte, totaleOfferte);
        pieModelOffered.set("Di cui SERVIZI: " + (rispOverServizi + abnOverServizi), (rispOverServizi + rispOverLDC) / totaleOfferte * 100);
        pieModelOffered.set("Di cui LDC: " + (rispOverLDC + abnOverLDC), (rispOverLDC + abnOverLDC) / totaleOfferte * 100);
        pieModelOffered.setTitle(bundle.getString("uilabel.overflowPieOff"));
        pieModelOffered.setShowDataLabels(true);
        pieModelOffered.setLegendPosition("ne");
        pieModelOffered.setShadow(false);
        pieModelOffered.setSeriesColors("58BA27,FFCC33,1300DA");
        pieModelOffered.setDataLabelThreshold(1);
        pieModelOffered.setExtender("pieExtender");

        pieModelAnswered = new PieChartModel();
        pieModelAnswered.set("SOCI: " + rispSoci, rispSoci);
        pieModelAnswered.set("Di cui SERVIZI: " + rispOverServizi, (rispOverServizi / rispSoci) * 100);
        pieModelAnswered.set("Di cui LDC: " + rispOverLDC, (rispOverLDC / rispSoci) * 100);
        pieModelAnswered.setTitle(bundle.getString("uilabel.overflowPieRisp"));
        pieModelAnswered.setShowDataLabels(true);
        pieModelAnswered.setLegendPosition("ne");
        pieModelAnswered.setShadow(false);
        pieModelAnswered.setSeriesColors("58BA27,FFCC33,1300DA");
        pieModelAnswered.setExtender("pieExtender");
        pieModelAnswered.setDataLabelThreshold(1);
    }

    public List getModelTableAG() {
        return modelTableAG;
    }

    public List<AgentGroupColumn> getColsAG() {
        return colsAG;
    }

    public List getModelTableTruckCot() {
        return modelTableTruckCot;
    }

    public List<String> getColsTruckCot() {
        return colsTruckCot;
    }

    public List getModelTableTruckCos() {
        return modelTableTruckCos;
    }

    public List<String> getColsTruckCos() {
        return colsTruckCos;
    }

    public BarChartModel getBarModel() {
        return barModel;
    }

    public PieChartModel getPieModelOffered() {
        return pieModelOffered;
    }

    public PieChartModel getPieModelAnswered() {
        return pieModelAnswered;
    }
}
