package com.tvop.beans.charts;

public class Chart4bccsi {
    private String area;
    private int tkPubblicati;
    private int tkChiusi;
    private int tkInEssere;

    public Chart4bccsi() {
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public int getTkChiusi() {
        return tkChiusi;
    }

    public void setTkChiusi(int tkChiusi) {
        this.tkChiusi = tkChiusi;
    }

    public int getTkInEssere() {
        return tkInEssere;
    }

    public void setTkInEssere(int tkInEssere) {
        this.tkInEssere = tkInEssere;
    }

    public int getTkPubblicati() {
        return tkPubblicati;
    }

    public void setTkPubblicati(int tkPubblicati) {
        this.tkPubblicati = tkPubblicati;
    }
}
