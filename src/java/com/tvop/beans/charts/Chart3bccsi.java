package com.tvop.beans.charts;

public class Chart3bccsi {
    private String area;
    private int tkPublisher;
    private int tkNotAccepted;
    private double percTkNotAccepted;
    private int tkClosed;
    private int tkAccepted;
    private int tkTecnicalIntervention;
    private int backlogAssistance;
    private int backlogTerzeParti;
    private String procedura;

    public Chart3bccsi() {
        tkPublisher=0;
        tkNotAccepted=0;
        percTkNotAccepted=0.0d;
        tkClosed=0;
        tkAccepted=0;
        tkTecnicalIntervention=0;
        backlogAssistance=0;
        backlogTerzeParti=0;
    }

    public Chart3bccsi(String area, int tkPublisher, int tkNotAccepted, double percTkNotAccepted, int tkClosed, int tkAccepted, int tkTecnicalIntervention, int backlogAssistance, int backlogTerzeParti) {
        this.area = area;
        this.tkPublisher = tkPublisher;
        this.tkNotAccepted = tkNotAccepted;
        this.percTkNotAccepted = percTkNotAccepted;
        this.tkClosed = tkClosed;
        this.tkAccepted = tkAccepted;
        this.tkTecnicalIntervention = tkTecnicalIntervention;
        this.backlogAssistance = backlogAssistance;
        this.backlogTerzeParti = backlogTerzeParti;
    }
    
    public Chart3bccsi(String area, int tkPublisher, int tkNotAccepted, double percTkNotAccepted, int tkClosed, int tkAccepted, int tkTecnicalIntervention, int backlogAssistance, int backlogTerzeParti, String procedura) {
        this.area = area;
        this.tkPublisher = tkPublisher;
        this.tkNotAccepted = tkNotAccepted;
        this.percTkNotAccepted = percTkNotAccepted;
        this.tkClosed = tkClosed;
        this.tkAccepted = tkAccepted;
        this.tkTecnicalIntervention = tkTecnicalIntervention;
        this.backlogAssistance = backlogAssistance;
        this.backlogTerzeParti = backlogTerzeParti;
        this.procedura = procedura;
    }

    public String getProcedura() {
        return procedura;
    }

    public void setProcedura(String procedura) {
        this.procedura = procedura;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public int getTkPublisher() {
        return tkPublisher;
    }

    public void setTkPublisher(int tkPublisher) {
        this.tkPublisher = tkPublisher;
    }

    public int getTkNotAccepted() {
        return tkNotAccepted;
    }

    public void setTkNotAccepted(int tkNotAccepted) {
        this.tkNotAccepted = tkNotAccepted;
    }

    public double getPercTkNotAccepted() {
        return percTkNotAccepted;
    }

    public void setPercTkNotAccepted(double percTkNotAccepted) {
        this.percTkNotAccepted = percTkNotAccepted;
    }

    public int getTkClosed() {
        return tkClosed;
    }

    public void setTkClosed(int tkClosed) {
        this.tkClosed = tkClosed;
    }

    public int getTkAccepted() {
        return tkAccepted;
    }

    public void setTkAccepted(int tkAccepted) {
        this.tkAccepted = tkAccepted;
    }

    public int getTkTecnicalIntervention() {
        return tkTecnicalIntervention;
    }

    public void setTkTecnicalIntervention(int tkTecnicalIntervention) {
        this.tkTecnicalIntervention = tkTecnicalIntervention;
    }

    public int getBacklogAssistance() {
        return backlogAssistance;
    }

    public void setBacklogAssistance(int backlogAssistance) {
        this.backlogAssistance = backlogAssistance;
    }

    public int getBacklogTerzeParti() {
        return backlogTerzeParti;
    }

    public void setBacklogTerzeParti(int backlogTerzeParti) {
        this.backlogTerzeParti = backlogTerzeParti;
    }
}
