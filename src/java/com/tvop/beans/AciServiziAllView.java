package com.tvop.beans;

import com.tvop.exceptions.DMLException;
import com.tvop.persistence.AciServiziJPA;
import com.tvop.persistence.dbentities.AgentGroupColumn;
import com.tvop.persistence.dbentities.AgentGroupRow;
import com.tvop.persistence.dbentities.CotTruckColumn;
import com.tvop.persistence.dbentities.CotTruckRow;
import com.tvop.persistence.dbentities.VAGColumn;
import com.tvop.persistence.dbentities.VAGRow;
import com.tvop.utils.Utils;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

@ManagedBean
@RequestScoped
public class AciServiziAllView implements Serializable {

    private List<Object> objects;
    private Object[] valuePosition;
    private String value;
    private String arrow;
    private int col;
    private int row;
    private Object[][][] values;
    private List modelTableAG;
    private List<AgentGroupColumn> colsAG;
    private List<AgentGroupRow> rowsAG;
    private List modelTableVAG;
    private List<VAGColumn> colsVAG;
    private List<VAGRow> rowsVAG;
    private List modelTableTruck;
    private List<String> colsTruck;
    private List<String> rowsTruck;

    private String colRowHeader;

    static final transient Logger LOGGER = LogManager.getLogger(AciServiziAllView.class.getName());

    public AciServiziAllView() {
        colsAG = new ArrayList<>();
        rowsAG = new ArrayList<>();
    }

    @PostConstruct
    public void init() {
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("schema", "aci_servizi");
        modelTableAG = getAG();
        modelTableVAG = getVAG();
        modelTableTruck = getTruck();
    }

    private List getAG() {
        try {
            colsAG = AciServiziJPA.getColsAG();
            rowsAG = AciServiziJPA.getRowsAG();
            objects = AciServiziJPA.getAllAG();
            values = new Object[rowsAG.size()][colsAG.size() + 1][2];
            //popolo tutta la tabella di valori '0' e simboli '='
            for (int i = 0; i < rowsAG.size(); i++) {
                for (int j = 0; j < colsAG.size() + 1; j++) {
                    values[i][j][0] = 0;
                }
            }
            for (int i = 0; i < rowsAG.size(); i++) {
                values[i][0][0] = rowsAG.get(i).getLabel();
            }
            // Popolo tutta la tabella
            for (Object element : objects) {
                valuePosition = (Object[]) element;
                value = Utils.convertValue(valuePosition[0], Integer.valueOf(valuePosition[4].toString()));
                arrow = Utils.evaluationKpi(valuePosition[5].toString());
                row = Integer.valueOf(valuePosition[1].toString());
                col = Integer.valueOf(valuePosition[2].toString());
                values[row - 1][col][0] = value;
                values[row - 1][col][1] = arrow;
            }
        } catch (DMLException | NumberFormatException e) {
            LOGGER.error("Error getting array list cot kpis " + e.getMessage());
        }
        return Arrays.asList(values);
    }

    private List getVAG() {
        try {
            colsVAG = AciServiziJPA.getColsVAG();
            rowsVAG = AciServiziJPA.getRowsVAG();
            objects = AciServiziJPA.getAllVAG();
            values = new Object[rowsVAG.size()][colsVAG.size() + 1][3];
            //popolo tutta la tabella di 0
            for (int i = 0; i < rowsVAG.size(); i++) {
                for (int j = 0; j < colsVAG.size() + 1; j++) {
                    values[i][j][0] = 0;
                }
            }
            for (int i = 0; i < rowsVAG.size(); i++) {
                values[i][0][0] = rowsVAG.get(i).getRowgui();
            }
            for (Object element : objects) {
                valuePosition = (Object[]) element;
                value = Utils.convertValue(valuePosition[0], Integer.valueOf(valuePosition[4].toString()));
                arrow = Utils.evaluationKpi(valuePosition[5].toString());
                row = Integer.valueOf(valuePosition[1].toString());
                col = Integer.valueOf(valuePosition[2].toString());
                values[row - 1][col][0] = value;
                values[row - 1][col][1] = arrow;
            }
        } catch (DMLException | NumberFormatException e) {
            LOGGER.error("Error getting array list cot kpis " + e.getMessage());
        }
        return Arrays.asList(values);
    }
    
    private List getTruck() {
        try {
            colsTruck = AciServiziJPA.getColsTruck();
            rowsTruck = AciServiziJPA.getRowsTruck();
            objects = AciServiziJPA.getAllTruck();
            values = new Object[rowsTruck.size()][colsTruck.size()+1][2];
            for(int i=0; i<rowsTruck.size(); i++){
                for(int j=0; j<colsTruck.size()+1; j++){
                    values[i][j][0] = 0;
                }
            }
            for(int i=0; i<rowsTruck.size(); i++){
                values[i][0][0] = rowsTruck.get(i);
            }
            for (Object element : objects) {
                valuePosition = (Object[]) element;
                value = Utils.convertValue(valuePosition[0], Integer.valueOf(valuePosition[4].toString()));
                arrow = Utils.evaluationKpi(valuePosition[5].toString());
                row = Integer.valueOf(valuePosition[1].toString());
                col = Integer.valueOf(valuePosition[2].toString());
                values[row-1][col][0] = value;
                values[row-1][col][1] = arrow;
            }
        } catch (DMLException | NumberFormatException e) {
            LOGGER.error("Error getting array list cot kpis " + e.getMessage());
        }
        return Arrays.asList(values);
    }

    public List getModelTableAG() {
        return modelTableAG;
    }

    public List<AgentGroupColumn> getColsAG() {
        return colsAG;
    }

    public List getModelTableVAG() {
        return modelTableVAG;
    }

    public List<VAGColumn> getColsVAG() {
        return colsVAG;
    }

    public List getModelTableTruck() {
        return modelTableTruck;
    }

    public List<String> getColsTruck() {
        return colsTruck;
    }
}
