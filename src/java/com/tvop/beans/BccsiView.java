package com.tvop.beans;

import com.tvop.persistence.BccsiJPA;
import com.tvop.beans.charts.Chart3bccsi;
import com.tvop.beans.charts.Chart4bccsi;
import com.tvop.exceptions.DMLException;
import com.tvop.persistence.KpiRealtimeJPA;
import com.tvop.persistence.KpiSerieTicketJPA;
import com.tvop.persistence.ThresholdEvaluationJPA;
import com.tvop.persistence.dbentities.KpiChart1BCCSI;
import com.tvop.persistence.dbentities.KpiChart2BCCSI;
import com.tvop.persistence.dbentities.KpiSerieCatalogazione;
import com.tvop.persistence.dbentities.KpiSerieRichieste;
import com.tvop.persistence.dbentities.ThresholdEvaluationKpi;
import com.tvop.utils.IntervalBoundaries;
import com.tvop.utils.IntervalsBCCSI;
import com.tvop.utils.Utils;
import java.awt.AWTException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import javax.faces.context.FacesContext;
import javax.imageio.ImageIO;
import org.joda.time.DateTime;
import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.DateAxis;
import org.primefaces.model.chart.HorizontalBarChartModel;
import org.primefaces.model.chart.LineChartModel;
import org.primefaces.model.chart.LineChartSeries;
import org.primefaces.model.chart.PieChartModel;

@ManagedBean
@ViewScoped
public class BccsiView implements Serializable {

    private final int HALFHOUR = 48 * 2;
    private IntervalsBCCSI selectedStep;
    private List<KpiChart2BCCSI> values;
    private List<KpiChart2BCCSI> valuesOld;
    private List<Chart3bccsi> valuesChart3;
    private List<Chart3bccsi> valuesChart3old;
    private List<Chart4bccsi> valuesChart4;
    private List<KpiSerieRichieste> valuesChart5a;
    private List<KpiSerieCatalogazione> valuesChart5b;
    private double percRispMin40;
    private double percTicketSuRisp;
    private LineChartModel lineModelDay;
    private String[] labelxAxisCharts;
    private double[] lineChartsDayOff;
    private double[] lineChartsDayRisp;
    private double[] lineChartsDayAbn;
    private int totOfferte;
    private int totRisposte;
    private int totAbbandonate;
    private HorizontalBarChartModel tkChiusiModel;
    private int maxValue = 0;
    private PieChartModel chart5Model;
    private PieChartModel chart6Model;
    private int daysOfMonth;
    private int maxVal;
    private boolean day;

    static final transient Logger LOGGER = LogManager.getLogger(BccsiView.class.getName());

    public BccsiView() {
        values = new ArrayList<>();
        valuesOld = new ArrayList<>();
    }

    @PostConstruct
    public void init() {
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("schema", "iccrea_bccsi");
        String step = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("step");
        selectedStep = IntervalsBCCSI.DAY;
        day = true;
        switch (step) {
            case "D":
                selectedStep = IntervalsBCCSI.DAY;
                day = true;
                break;
            case "W":
                selectedStep = IntervalsBCCSI.WEEK;
                day = false;
                break;
            case "M":
                selectedStep = IntervalsBCCSI.MONTH;
                day = false;
                break;
        }
        createChart1();
        createChart2();
        createChart3();
        createChart4();
        createChart5();
    }

    private void createChart1() {
        try {
            maxVal = 0;
            daysOfMonth = Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_MONTH);
            labelxAxisCharts = getXAxisChartsDay();
            List<KpiChart1BCCSI> values = KpiRealtimeJPA.getAllChart1NRT(selectedStep);
            lineChartsDayOff = new double[labelxAxisCharts.length];
            lineChartsDayRisp = new double[labelxAxisCharts.length];
            lineChartsDayAbn = new double[labelxAxisCharts.length];
            for (int i = 0; i < labelxAxisCharts.length; i++) {
                lineChartsDayOff[i] = 0;
                lineChartsDayRisp[i] = 0;
                lineChartsDayAbn[i] = 0;
            }
            totOfferte = totRisposte = totAbbandonate = 0;
            double totResp40s = 0;
            int i = 0;
            for (KpiChart1BCCSI val : values) {
                totOfferte += val.getOfferte();
                totRisposte += val.getRisposte();
                totAbbandonate += val.getAbbandonate();
                totResp40s += val.getRispmin40();
                lineChartsDayOff[i] = val.getOfferte();
                lineChartsDayRisp[i] = val.getRisposte();
                lineChartsDayAbn[i] = val.getAbbandonate();
                i++;
                if (val.getOfferte() > maxVal) {
                    maxVal = (int) val.getOfferte();
                }
            }
            double res = 0;
            double res2 = 0;
            double ticket = KpiSerieTicketJPA.getPublischedTicketVoice(selectedStep);
            if (totOfferte > 0) {
                res = (totResp40s / (double) totOfferte) * 100;
                res2 = (ticket / (double) totOfferte) * 100;
            }
            percRispMin40 = res;
            percTicketSuRisp = res2;
            popolateValAtNow(selectedStep);
            createLineModels();
        } catch (Exception e) {
            LOGGER.error("Error getting array list cot kpis " + e.getMessage());
        }
    }

    private void createLineModels() {
        DateAxis xAxis = new DateAxis();
        Axis yAxis;
        switch (selectedStep) {
            case DAY:
                xAxis.setTickFormat("%H:%M");
                xAxis.setMin("07:00");
                xAxis.setMax("19:00");
                xAxis.setTickCount(49);
                xAxis.setTickAngle(-50);
                break;
            case WEEK:
                xAxis.setTickFormat("%d-%m-%y");
                xAxis.setMin(labelxAxisCharts[0]);
                xAxis.setMax(labelxAxisCharts[6]);
                xAxis.setTickCount(7);
                xAxis.setTickAngle(-50);
                break;
            case MONTH:
                int max = daysOfMonth;
                int tickCount = (daysOfMonth / 2) + 1;
                if (daysOfMonth % 2 == 1) { // giorni dispari
                    max = daysOfMonth - 1;
                    tickCount = (daysOfMonth + 1) / 2;
                }
                xAxis.setTickFormat("%d-%m-%y");
                xAxis.setMin(labelxAxisCharts[0]);
                xAxis.setMax(labelxAxisCharts[max]);
                xAxis.setTickCount(tickCount);
                xAxis.setTickAngle(-50);
                break;

        }
        lineModelDay = initLinearModel();
        lineModelDay.setResetAxesOnResize(false);
        lineModelDay.setZoom(true);
        lineModelDay.setSeriesColors("58BA27,FFCC33,F74A4A");
//        lineModelDay.setExtender("chartExtender");
//        lineModelDay.setAnimate(true);
        lineModelDay.getAxes().put(AxisType.X, xAxis);
        yAxis = lineModelDay.getAxis(AxisType.Y);
        yAxis.setMin(0);
        int max = 10 - (maxVal % 10) + maxVal;
        yAxis.setMax(max);
        yAxis.setTickCount(11);
    }

    private LineChartModel initLinearModel() {
        LineChartModel model = new LineChartModel();
        LineChartSeries offered = new LineChartSeries();
        LineChartSeries answered = new LineChartSeries();
        LineChartSeries abnMag20 = new LineChartSeries();
        int count = 0;
        switch (selectedStep) {
            case DAY:
                count = HALFHOUR;
                break;
            case WEEK:
                count = 7;
                break;
            case MONTH:
                count = daysOfMonth;
                break;
        }
        for (int i = 0; i < count; i++) {
            offered.set(labelxAxisCharts[i], lineChartsDayOff[i]);
            answered.set(labelxAxisCharts[i], lineChartsDayRisp[i]);
            abnMag20.set(labelxAxisCharts[i], lineChartsDayAbn[i]);
        }

        offered.setShowMarker(false);
        answered.setShowMarker(false);
        abnMag20.setShowMarker(false);

        offered.setSmoothLine(true);
        answered.setSmoothLine(true);
        abnMag20.setSmoothLine(true);

        model.addSeries(offered);
        model.addSeries(answered);
        model.addSeries(abnMag20);
        return model;
    }

    private String[] getXAxisChartsDay() {
        String[] result = null;
        SimpleDateFormat sdfTemp = new SimpleDateFormat("yyyy-MM-dd");
        DateTime dt;
        switch (selectedStep) {
            case DAY:
                result = new String[HALFHOUR];
                dt = IntervalBoundaries.getStartOfDay(new DateTime(2020, 6, 5, 18, 15));
                for (int i = 0; i < HALFHOUR; i++) {
                    result[i] = dt.toString();
                    dt = dt.plusMinutes(15);
                }
                break;
            case WEEK:
                result = new String[7];
                dt = IntervalBoundaries.getStartOfWeek(new DateTime(2020, 6, 5, 18, 15));
                for (int i = 0; i < 7; i++) {
                    result[i] = sdfTemp.format(new Date(dt.getMillis()));
                    dt = dt.plusDays(1);
                }
                break;
            case MONTH:
                result = new String[daysOfMonth + 1];
                dt = IntervalBoundaries.getStartOfMonth(new DateTime(2020, 6, 5, 18, 15));
                for (int i = 0; i < result.length; i++) {
                    result[i] = sdfTemp.format(new Date(dt.getMillis()));
                    dt = dt.plusDays(1);
                }
                break;
        }
        return result;
    }

    private void popolateValAtNow(IntervalsBCCSI interval) {
        int current = 0;
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Europe/Rome"));
        cal.set(2020, 6, 5);
        switch (interval) {
            case DAY:
                long startOfDay = IntervalBoundaries.getStartOfDay(new DateTime(2020, 6, 5, 18, 15)).getMillis();
                long currentHalfHour = new DateTime(2020, 6, 5, 18, 15).getMillis();
                current = (int) ((currentHalfHour / 1000) / 900 - (startOfDay / 1000) / 900) + 1;
                break;
            case WEEK:
                current = cal.get(Calendar.DAY_OF_WEEK) - 1; // da testare. la settimana inizia da domenica
                break;
            case MONTH:
                current = cal.get(Calendar.DAY_OF_MONTH);
                break;
        }
        for (int i = 0; i < lineChartsDayOff.length; i++) {
            if (i > current) {
                lineChartsDayOff[i] = Double.NaN;
                lineChartsDayAbn[i] = Double.NaN;
                lineChartsDayRisp[i] = Double.NaN;
            }
        }
    }

    public String getPercValue(double value) {
        return Utils.getPercValue(value);
    }

    private void createChart2() {
        try {
            List<KpiChart2BCCSI> temp = KpiRealtimeJPA.getAllChart2NRT(false);
            List<KpiChart2BCCSI> res = orderList(temp);
            List<KpiChart2BCCSI> resOld = KpiRealtimeJPA.getAllChart2NRT(true);
            KpiChart2BCCSI total = getTotal(res);
            values.add(total);
            for (KpiChart2BCCSI elem : res) {
                values.add(elem);
            }
            KpiChart2BCCSI totalOld = getTotal(resOld);
            valuesOld.add(totalOld);
            for (KpiChart2BCCSI elem : resOld) {
                valuesOld.add(elem);
            }
        } catch (DMLException e) {
            LOGGER.error("Error getting array list cot kpis " + e.getMessage());
        }
    }

    private KpiChart2BCCSI getTotal(List<KpiChart2BCCSI> list) {
        KpiChart2BCCSI result = new KpiChart2BCCSI();
        result.setArea("TOTALE");
        result.setTimeref(new Timestamp(DateTime.now().getMillis()));
        int count = 0;
        for (KpiChart2BCCSI elem : list) {
            result.setRisposte(result.getRisposte() + elem.getRisposte());
            result.setAbbandonate(result.getAbbandonate() + elem.getAbbandonate());
            if (elem.getPercrispmin40() > 0) {
                result.setPercrispmin40(result.getPercrispmin40() + elem.getPercrispmin40());
                count++;
            }
        }
        result.setOfferte(result.getRisposte() + result.getAbbandonate());
        if (result.getOfferte() > 0) {
            double res = (double) result.getRisposte() / (double) result.getOfferte() * 100;
            result.setPercrisposte(res);
        }
        if (count > 0) {
            double res = result.getPercrispmin40() / (double) count;
            result.setPercrispmin40(res);
        }
        return result;
    }

    public String thresholdEval(KpiChart2BCCSI row, String kpiid) throws DMLException {
        KpiChart2BCCSI old = null;
        for (KpiChart2BCCSI tmp : valuesOld) {
            if (row.getArea().equals(tmp.getArea())) {
                old = tmp;
            }
        }
        ThresholdEvaluationKpi eval = ThresholdEvaluationJPA.getAllByKpi(kpiid);
        double newValue = 0;
        double oldValue = 0;
        if (old != null) {
            switch (kpiid) {
                case "01": // Offerte
                    newValue = row.getOfferte();
                    oldValue = old.getOfferte();
                    break;
                case "02": // Risposte
                    newValue = row.getRisposte();
                    oldValue = old.getRisposte();
                    break;
                case "03": // Abbandonate
                    newValue = row.getAbbandonate();
                    oldValue = old.getAbbandonate();
                    break;
                case "04": // % Risposte su Totale
                    newValue = row.getPercrisposte();
                    oldValue = old.getPercrisposte();
                    break;
                case "05": // % Risposte < 40"
                    newValue = row.getPercrispmin40();
                    oldValue = old.getPercrispmin40();
                    break;
            }
        }
        return Utils.evaluationKpiWithValues(eval, newValue, oldValue);
    }

    private List<KpiChart2BCCSI> orderList(List<KpiChart2BCCSI> origin) {
        Map<String, Integer> order = new HashMap<>();
//        order.put("IsiBox", 1);
        order.put("SEGNALAZIONI E BILANCIO", 1);
        order.put("OVERFLOW SEGNALAZIONI E BILANCIO", 2);
        order.put("SISTEMI DISPOSITIVI", 3);
        order.put("OVERFLOW SISTEMI DISPOSITIVI", 4);
        order.put("SPORTELLO E PRODOTTI DI BASE", 5);
        order.put("OVERFLOW SPORTELLO E PRODOTTI DI BASE", 6);
//        order.put("Reperibile", 8);
        order.put("CDG_RISCHIO DI CREDITO_PRODUZIONE FLUSSI", 7);
        order.put("OVERFLOW CDG_RDC_PRODUZIONE FLUSSI", 8);
        order.put("CREDITO", 9);
        order.put("OVERFLOW CREDITO", 10);
        order.put("DIREZIONALE E CRM", 11);
        order.put("OVERFLOW DIREZIONALE E CRM", 12);
        order.put("FINANZA", 13);
        order.put("OVERFLOW FINANZA", 14);
        order.put("HELP DESK TECNOLOGICO", 15);
        order.put("OVERFLOW HELP DESK TECNOLOGICO", 16);
        order.put("SERVIZI INTEGRATIVI", 17);
        KpiChart2BCCSI[] result = new KpiChart2BCCSI[origin.size()];
        for (KpiChart2BCCSI temp : origin) {
            result[order.get(temp.getArea()) - 1] = temp;
        }
        return Arrays.asList(result);
    }

    private void createChart3() {
        valuesChart3 = groupList(BccsiJPA.getLastForAllTicket());
        valuesChart3old = groupList(BccsiJPA.getYesterdayAllTicket());
        valuesChart3.add(0, getTotalTk(valuesChart3));
        valuesChart3old.add(0, getTotalTk(valuesChart3old));
    }

    private List<Chart3bccsi> groupList(List<Chart3bccsi> list) {
        List<Chart3bccsi> result = new ArrayList<>();
        for (Chart3bccsi elem : list) {
            int i = 0;
            Chart3bccsi tmp = elem;
            boolean find = false;
            for (Chart3bccsi elem2 : result) {
                if (elem.getArea().equals(elem2.getArea())) {
                    tmp.setTkPublisher(tmp.getTkPublisher() + elem2.getTkPublisher());
                    tmp.setTkClosed(tmp.getTkClosed() + elem2.getTkClosed());
                    tmp.setTkNotAccepted(tmp.getTkNotAccepted() + elem2.getTkNotAccepted());
                    tmp.setTkAccepted(tmp.getTkAccepted() + elem2.getTkAccepted());
                    tmp.setTkTecnicalIntervention(tmp.getTkTecnicalIntervention() + elem2.getTkTecnicalIntervention());
                    tmp.setBacklogAssistance(tmp.getBacklogAssistance() + elem2.getBacklogAssistance());
                    tmp.setBacklogTerzeParti(tmp.getBacklogTerzeParti() + elem2.getBacklogTerzeParti());
                    find = true;
                    break;
                }
                i++;
            }
            if (find) {
                result.remove(i);
                if (tmp.getTkPublisher() > 0) {
                    tmp.setPercTkNotAccepted((double) tmp.getTkNotAccepted() / (double) tmp.getTkPublisher() * 100);
                }
                result.add(tmp);
            } else {
                result.add(elem);
            }
        }
        return result;
    }

    private Chart3bccsi getTotalTk(List<Chart3bccsi> list) {
        Chart3bccsi result = new Chart3bccsi();
        result.setArea("Totale");
        for (Chart3bccsi elem : list) {
            result.setTkPublisher(result.getTkPublisher() + elem.getTkPublisher());
            result.setTkClosed(result.getTkClosed() + elem.getTkClosed());
            result.setTkNotAccepted(result.getTkNotAccepted() + elem.getTkNotAccepted());
            if (result.getTkPublisher() > 0) {
                result.setPercTkNotAccepted((double) result.getTkNotAccepted() / (double) result.getTkPublisher() * 100);
            } else {
                result.setPercTkNotAccepted(0);
            }
            result.setTkAccepted(result.getTkAccepted() + elem.getTkAccepted());
            result.setTkTecnicalIntervention(result.getTkTecnicalIntervention() + elem.getTkTecnicalIntervention());
            result.setBacklogAssistance(result.getBacklogAssistance() + elem.getBacklogAssistance());
            result.setBacklogTerzeParti(result.getBacklogTerzeParti() + elem.getBacklogTerzeParti());
        }
        return result;
    }

    public String thresholdEval(Chart3bccsi val, String kpiid) throws DMLException {
        Chart3bccsi old = null;
        for (Chart3bccsi tmp : valuesChart3old) {
            if (tmp.getArea().equals(val.getArea())) {
                old = tmp;
            }
        }
        ThresholdEvaluationKpi eval = ThresholdEvaluationJPA.getAllByKpi(kpiid);
        double newValue = 0;
        double oldValue = 0;
        if (old != null) {
            switch (kpiid) {
                case "50": // Tk Pubblicati
                    newValue = val.getTkPublisher();
                    oldValue = old.getTkPublisher();
                    break;
                case "51": // Tk Non Accettati
                    newValue = val.getTkNotAccepted();
                    oldValue = old.getTkNotAccepted();
                    break;
                case "52": // % Tk Non Accettati
                    newValue = val.getPercTkNotAccepted();
                    oldValue = old.getPercTkNotAccepted();
                    break;
                case "53": // Tk Chiusi
                    newValue = val.getTkClosed();
                    oldValue = old.getTkClosed();
                    break;
                case "54": // Tk In Intervento Tecnico
                    newValue = val.getTkTecnicalIntervention();
                    oldValue = old.getTkTecnicalIntervention();
                    break;
                case "55": // Backlog Assistenza
                    newValue = val.getBacklogAssistance();
                    oldValue = old.getBacklogAssistance();
                    break;
                case "56": // Backlog Terze Parti
                    newValue = val.getBacklogTerzeParti();
                    oldValue = old.getBacklogTerzeParti();
                    break;
                case "60": // Tk Accettati
                    newValue = val.getTkAccepted();
                    oldValue = old.getTkAccepted();
                    break;
            }
        }
        return Utils.evaluationKpiWithValues(eval, newValue, oldValue);
    }

    private void createChart4() {
        valuesChart4 = groupList4(BccsiJPA.getLastForChart4());
        tkChiusiModel = new HorizontalBarChartModel();

        ChartSeries tkPubblicati = new ChartSeries();
        ChartSeries tkChiusi = new ChartSeries();
        ChartSeries tkInEssere = new ChartSeries();

        for (Chart4bccsi tk : valuesChart4) {
            if (tk.getTkInEssere() > maxValue) {
                maxValue = tk.getTkInEssere();
            }
            tkPubblicati.set(tk.getArea(), tk.getTkPubblicati());
            tkChiusi.set(tk.getArea(), tk.getTkChiusi());
            tkInEssere.set(tk.getArea(), tk.getTkInEssere());
        }
        tkChiusiModel.addSeries(tkInEssere);
        tkChiusiModel.addSeries(tkChiusi);
        tkChiusiModel.addSeries(tkPubblicati);
        tkChiusiModel.setShowPointLabels(true);
        tkChiusiModel.setBarWidth(6);
        tkChiusiModel.setSeriesColors("F74A4A,58BA27,DBC300");

        Axis xAxis = tkChiusiModel.getAxis(AxisType.X);
        xAxis.setMin(0);
        xAxis.setMax(maxValue + 20);
        xAxis.setTickFormat("%#d");
        Axis yAxis = tkChiusiModel.getAxis(AxisType.Y);
    }

    private List<Chart4bccsi> groupList4(List<Chart4bccsi> list) {
        List<Chart4bccsi> result = new ArrayList<>();
        for (Chart4bccsi elem : list) {
            int i = 0;
            Chart4bccsi tmp = elem;
            boolean find = false;
            for (Chart4bccsi elem2 : result) {
                if (elem.getArea().equals(elem2.getArea())) {
                    tmp.setTkPubblicati(tmp.getTkPubblicati() + elem2.getTkPubblicati());
                    tmp.setTkChiusi(tmp.getTkChiusi() + elem2.getTkChiusi());
                    tmp.setTkInEssere(tmp.getTkInEssere() + elem2.getTkInEssere());
                    find = true;
                    break;
                }
                i++;
            }
            if (find) {
                result.remove(i);
                result.add(tmp);
            } else {
                result.add(elem);
            }
        }
        return result;
    }

    private void createChart5() {
        valuesChart5a = BccsiJPA.getAllByStepChart5(IntervalsBCCSI.DAY);
        valuesChart5b = BccsiJPA.getAllByStepChart6(IntervalsBCCSI.DAY);
        chart5Model = new PieChartModel();
        float total = 0;
        for (KpiSerieRichieste kpi : valuesChart5a) {
            total += kpi.getValue();
        }
        for (KpiSerieRichieste kpi : valuesChart5a) {
            chart5Model.set(kpi.getCodrich() + " " + String.format("%.2f", ((float) kpi.getValue() / total) * 100) + "%", kpi.getValue());
        }
        chart5Model.setLegendPosition("e");
        chart5Model.setShowDataLabels(true);

        chart6Model = new PieChartModel();
        total = 0;
        for (KpiSerieCatalogazione kpi : valuesChart5b) {
            total += kpi.getValue();
        }
        for (KpiSerieCatalogazione kpi : valuesChart5b) {
            chart6Model.set(kpi.getDescr() + " " + String.format("%.2f", ((float) kpi.getValue() / total) * 100) + "%", kpi.getValue());
        }
        chart6Model.setLegendPosition("e");
        chart6Model.setShowDataLabels(true);
    }

    public String getTimeFormat(BigDecimal value) {
        return Utils.tFormat(value.doubleValue() / 1000); // da millisecondi a secondi
    }

    public LineChartModel getLineModelDay() {
        return lineModelDay;
    }

    public int getTotOfferte() {
        return totOfferte;
    }

    public int getTotRisposte() {
        return totRisposte;
    }

    public int getTotAbbandonate() {
        return totAbbandonate;
    }

    public double getPercRispMin40() {
        return percRispMin40;
    }

    public double getPercTicketSuRisp() {
        return percTicketSuRisp;
    }

    public List<Chart3bccsi> getValuesChart3() {
        return valuesChart3;
    }

    public HorizontalBarChartModel getTkChiusiModel() {
        return tkChiusiModel;
    }

    public PieChartModel getChart5Model() {
        return chart5Model;
    }

    public PieChartModel getChart6Model() {
        return chart6Model;
    }

    public List<KpiChart2BCCSI> getValues() {
        return values;
    }

    public boolean isDay() {
        return day;
    }
}
