package com.tvop.beans;

import com.tvop.persistence.PerformanceChartsJPA;
import com.tvop.persistence.PureCloudJPA;
import com.tvop.persistence.dbentities.PC_IVROrdering;
import com.tvop.persistence.dbentities.PC_RT_WrapUp;
import com.tvop.utils.Utils;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

@ManagedBean(name = "pC_RT_WrapUpView")
@RequestScoped
public class PC_RT_WrapUpView implements Serializable {

    static final transient Logger LOGGER = LogManager.getLogger(PC_RT_WrapUpView.class.getName());
    private List<PC_RT_WrapUp> values;
    private List<PC_RT_WrapUp> valuesOld;

    public PC_RT_WrapUpView() {
        values = new ArrayList<>();
        valuesOld = new ArrayList<>();
    }

    @PostConstruct
    public void init() {
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("schema", "iccrea_sinergia_b2b");
        values = order(PerformanceChartsJPA.getAllbyQueues(PC_RT_WrapUp.class, "name", false));
        values.add(0, getTotal(values));
    }
    
    private List<PC_RT_WrapUp> order(List<PC_RT_WrapUp> list) {
        List<PC_IVROrdering> ivrOrdering = PureCloudJPA.getPC_IvrOrdering(PC_IVROrdering.class, "key.ordering");
        List<PC_RT_WrapUp> result = new ArrayList<>();
        for (PC_IVROrdering ivr : ivrOrdering) {
            for (PC_RT_WrapUp elem : list) {
                if (ivr.getKey().getCoda().equals(elem.getName())) {
                    result.add(elem);
                    break;
                }
            }
        }
        return result;
    }

    private PC_RT_WrapUp getTotal(List<PC_RT_WrapUp> list) {
        PC_RT_WrapUp result = new PC_RT_WrapUp();
        result.getKey().setId("Totale");
        result.setName("TOTALE");
        for (PC_RT_WrapUp elem : list) {
            result.setCmbrischedulate(result.getCmbrischedulate() + elem.getCmbrischedulate());
            result.setDissuase(result.getDissuase() + elem.getDissuase());
            result.setGestite(result.getGestite() + elem.getGestite());
            result.setGestitenonconsegnate(result.getGestitenonconsegnate() + elem.getGestitenonconsegnate());
            result.setInviatomessaggio(result.getInviatomessaggio() + elem.getInviatomessaggio());
            result.setTrasferite(result.getTrasferite() + elem.getTrasferite());
            result.setTrasferitebackoffice(result.getTrasferitebackoffice() + elem.getTrasferitebackoffice());
            result.setTrasferitetesoreria(result.getTrasferitetesoreria() + elem.getTrasferitetesoreria());
        }
        return result;
    }

    public String tFormat(int val) {
        return Utils.tFormat(val);
    }

    public String getPerc(float val) {
        if (val == 0) {
            return "-";
        }
        return Utils.checkDecimal(val) + "%";
    }

    public List<PC_RT_WrapUp> getValues() {
        return values;
    }
}
